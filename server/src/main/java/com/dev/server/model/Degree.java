package com.dev.server.model;

import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "degrees")
public class Degree {
    @Id
    private String id;
    private String name;
}
