import React, { useState } from "react";
import {
  IoArrowForwardCircleOutline,
  IoArrowBackCircleOutline,
  IoArrowForwardOutline,
  IoArrowBackOutline,
} from "react-icons/io5";
import { generateDate } from "../../utils/calendar";
import dayjs from "dayjs";
import { chooseDate } from "../../redux/appointmentSlice";
import { useDispatch } from "react-redux";
import TimeOfDoctor from "./TimeOfDoctor";
function MedicalScheduleCalendar(props) {
  const { dateOfWeek, day } = props;
  const days = ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"];
  const currentDate = dayjs();
  const [today, setToday] = useState(currentDate);
  const [checkIndex, setCheckIndex] = useState(false);
  const [showIndex, setShowIndex] = useState();

  const [rowIndex, setRowIndex] = useState([]);
  const generatedDates = generateDate(today.month(), today.year()); // Tạo mảng các ngày trong tháng hiện tại
  let daysOfDoctor = [];

  let startIndex = dateOfWeek + 1;
  const arrStartIndex = [];
  let ARRdays = [];
  for (let i = 0; i < generatedDates.length; i++) {
    if (i + 1 === startIndex) {
      arrStartIndex.push(startIndex);
      startIndex += 7;
    }
  }
  for (let i = 0; i < arrStartIndex.length; i++) {
    if (generatedDates[arrStartIndex[i] - 1].currentMonth === true) {
      if (generatedDates[arrStartIndex[i] - 1].date.date() >= today.date()) {
        daysOfDoctor.push(generatedDates[arrStartIndex[i] - 1].date.date());
      } else if (currentDate.month() + 1 < today.month() + 1) {
        daysOfDoctor.push(generatedDates[arrStartIndex[i] - 1].date.date());
      }
    }
  }
  let a = 0;
  for (let i = 0; i < 6; i++) {
    ARRdays[i] = [];
    for (let j = 0; j < 7; j++) {
      ARRdays[i][j] = generatedDates[a].date.date();
      a++;
    }
  }
  console.log(day);
  const CheckDay = (index) => {
    let temp = 0;
    for (let i = 0; i < 6; i++) {
      for (let j = 0; j < 7; j++) {
        if (
          ARRdays[i][j] === generatedDates[index].date.date() &&
          generatedDates[temp].currentMonth === true
        ) {
          return i;
        }
        temp++;
      }
    }
  };
  console.log(day);
  const DISPATCH = useDispatch();

  const saveDateOfWeek = async (date) => {
    DISPATCH(chooseDate(date));
  };
  const [showTime, setShowTime] = useState(false);
  return (
    <div className="w-full flex flex-wrap">
      <div className="w-full bg-[#f26a8d] text-white px-5 py-2 rounded-t-md font-semibold">
        <h2>Vui lòng chọn ngày khám</h2>
      </div>
      <div className="w-full">
        <div>
          <div className="flex items-center justify-center text-xl font-bold my-2 gap-5">
            <IoArrowBackOutline
              className="cursor-pointer bg-[#FB6F92] text-white rounded-full"
              onClick={() => {
                if (today.month() === currentDate.month()) {
                  return;
                }
                setToday(today.month(today.month() - 1));
                setCheckIndex(false);
                setShowTime(false);
              }}
            />
            <h1 className="text-[#FB6F92]">{`Tháng ${today.month() + 1 < 10 ? "0" : ""
              }${today.month() + 1}-${today.year()}`}</h1>
            <IoArrowForwardOutline
              className="cursor-pointer bg-[#FB6F92] text-white rounded-full"
              onClick={() => {
                if (today.month() === currentDate.month() + 1) {
                  return;
                }
                setToday(today.month(today.month() + 1));
                setCheckIndex(false);
                setShowTime(false);
              }}
            />
          </div>
          <div className="grid grid-cols-7 bg-slate-100 text-lg font-semibold">
            {days?.map((day, index) => {
              return (
                <h1
                  className="h-14 border grid place-content-center"
                  key={index}
                >
                  {day}
                </h1>
              );
            })}
          </div>
          <div className={`grid grid-cols-7 ${checkIndex ? "hidden" : ""}`}>
            {generateDate(today.month(), today.year()).map(
              ({ date, currentMonth, today }, index) => {
                // Kiểm tra xem ngày hiện tại có tồn tại trong mảng daysOfDoctor hay không
                const isDoctorDay = daysOfDoctor.includes(date.date());
                return (
                  <div
                    className="h-14 border grid place-content-center"
                    key={index}
                  >
                    <h1
                      className={` flex items-center justify-center w-11 h-11 text-center ${isDoctorDay && currentMonth
                          ? "cursor-pointer hover:bg-[#FB6F92] hover:text-white transition-all rounded-md"
                          : "text-gray-400 "
                        }`}
                      onClick={() => {
                        if (isDoctorDay && currentMonth) {
                          const i = CheckDay(index);
                          setRowIndex(ARRdays[i]);
                          setCheckIndex(true);
                          setShowIndex(date.date());
                          const dateChoosed = {
                            DayOfWeek: dateOfWeek,
                            day: date.date(),
                            month: date.month() + 1,
                            year: date.year(),
                          };
                          saveDateOfWeek(dateChoosed);
                          setShowTime(true);
                        }
                      }}
                    >
                      {date.date()}
                    </h1>
                  </div>
                );
              }
            )}
          </div>
          <div className={`grid grid-cols-7 ${checkIndex ? "" : "hidden"}`}>
            {rowIndex?.map((day, index) => {
              return (
                <div
                  className="h-14 border grid place-content-center"
                  key={index}
                >
                  <h1
                    className={`  flex items-center justify-center w-11 h-11 text-center ${day === showIndex
                        ? "cursor-pointer bg-[#FB6F92] text-white rounded-md"
                        : "text-gray-400 "
                      }`}
                  >
                    {day}
                  </h1>
                </div>
              );
            })}
          </div>
          <div className="text-end">
            <p
              className={`cursor-pointer mx-4 my-2 ${checkIndex ? "" : "hidden"
                }`}
              onClick={() => {
                setCheckIndex(false);
                setShowTime(false);
              }}
            >
              Đóng
            </p>
          </div>
        </div>
      </div>
      {showTime && (
        <>
          {day
            ?.filter((daysItem) => daysItem.day === showIndex)
            .map((daysItem) =>
              daysItem.times.map((items) => (
                <TimeOfDoctor
                  key={items._id}
                  isChoose={items?.choose}
                  timeStart={items?.timeStart}
                  timeEnd={items?.timeEnd}
                />
              ))
            )}
        </>
      )}
    </div>
  );
}

export default MedicalScheduleCalendar;
