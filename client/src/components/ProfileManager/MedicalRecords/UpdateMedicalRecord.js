import React, { useEffect, useReducer, useState } from "react";
import styled from "styled-components";
import { IoRefreshCircleOutline, IoPersonAdd } from "react-icons/io5";
import recordApi from "../../../api/recordApi";
import { useParams, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
const Header2 = styled.h2`
  border-bottom: 5px solid rgba(255, 202, 212, 0.4);
  height: 28px;
`;
const Rewrite = styled.div`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  color: #4a4e69;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: rgba(144, 224, 239, 0.2);
    color: #48cae4;
  }
`;
const AddNew = styled.button`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: #fb6f92;
  }
`;
const Input = styled.input`
  transition: ease-in-out all 0.2s;
`;
const Select = styled.select`
  transition: ease-in-out all 0.2s;
  padding: 0.5rem 0.75rem;
  width: 100%;
  height: 2.5rem;
  margin: 0 1.25rem;
  outline: solid 2px transparent;
  outline-offset: 2px;
  color: #495057;
  border-width: 1px;
  border-style: solid;
  border-radius: 0.375rem;
  &:focus {
    border-color: #ffb3c6;
  }
`;
const initialState = {
  inputFullNameValue: "",
  showFullNameError: false,
  inputPhoneNumValue: "",
  showPhoneNumError: false,
  checkFormatPhoneNum: false,
  inputAddressValue: "",
  showAddressError: false,
  inputNNValue: "0",
  showNNError: false,
  inputTTValue: "0",
  showTTError: false,
  inputPXValue: "0",
  showPXError: false,
  inputNSValue: "",
  showNSError: false,
  inputGTValue: "-1",
  showGTError: false,
  inputQHValue: "0",
  showQHError: false,
  inputEmailValue: "",
  inputCMNDValue: "",
  inputDTValue: "",
};
function reducer(state, action) {
  switch (action.type) {
    case "SET_FULLNAME_VALUE":
      return { ...state, inputFullNameValue: action.payload };
    case "SHOW_FULLNAME_ERROR":
      return { ...state, showFullNameError: true };
    case "HIDE_FULLNAME_ERROR":
      return { ...state, showFullNameError: false };
    case "SET_PHONENUM_VALUE":
      return { ...state, inputPhoneNumValue: action.payload };
    case "SHOW_PHONENUM_ERROR":
      return { ...state, showPhoneNumError: true };
    case "HIDE_PHONENUM_ERROR":
      return { ...state, showPhoneNumError: false };
    case "CHECK_FORMAT_PHONENUM_TRUE":
      return { ...state, checkFormatPhoneNum: true };
    case "CHECK_FORMAT_PHONENUM_FALSE":
      return { ...state, checkFormatPhoneNum: false };
    case "SET_ADDRESS_VALUE":
      return { ...state, inputAddressValue: action.payload };
    case "SHOW_ADDRESS_ERROR":
      return { ...state, showAddressError: true };
    case "HIDE_ADDRESS_ERROR":
      return { ...state, showAddressError: false };
    case "SET_NN_VALUE":
      return { ...state, inputNNValue: action.payload };
    case "SHOW_NN_ERROR":
      return { ...state, showNNError: true };
    case "HIDE_NN_ERROR":
      return { ...state, showNNError: false };
    case "SET_TT_VALUE":
      return { ...state, inputTTValue: action.payload };
    case "SHOW_TT_ERROR":
      return { ...state, showTTError: true };
    case "HIDE_TT_ERROR":
      return { ...state, showTTError: false };
    case "SET_PX_VALUE":
      return { ...state, inputPXValue: action.payload };
    case "SHOW_PX_ERROR":
      return { ...state, showPXError: true };
    case "HIDE_PX_ERROR":
      return { ...state, showPXError: false };
    case "SET_NS_VALUE":
      return { ...state, inputNSValue: action.payload };
    case "SHOW_NS_ERROR":
      return { ...state, showNSError: true };
    case "HIDE_NS_ERROR":
      return { ...state, showNSError: false };
    case "SET_GT_VALUE":
      return { ...state, inputGTValue: action.payload };
    case "SHOW_GT_ERROR":
      return { ...state, showGTError: true };
    case "HIDE_GT_ERROR":
      return { ...state, showGTError: false };
    case "SET_QH_VALUE":
      return { ...state, inputQHValue: action.payload };
    case "SHOW_QH_ERROR":
      return { ...state, showQHError: true };
    case "HIDE_QH_ERROR":
      return { ...state, showQHError: false };
    case "SET_EMAIL_VALUE":
      return { ...state, inputEmailValue: action.payload };
    case "SET_PEOPLE_VALUE":
      return { ...state, inputDTValue: action.payload };
    case "SET_IDENTITY_VALUE":
      return { ...state, inputCMNDValue: action.payload };
    case "RESET":
      return initialState;
    default:
      return state;
  }
}
function UpdateMedicalRecord(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const { id } = useParams();
  let navigate = useNavigate();
  const [state, dispatch] = useReducer(reducer, initialState);
  useEffect(() => {
    if (!user) {
      navigate("/login");
    }
  });
  const handleFUllNameChange = (event) => {
    dispatch({ type: "SET_FULLNAME_VALUE", payload: event.target.value });
  };
  const handlePhoneNumChange = (event) => {
    const { value } = event.target;
    if (/^\d*$/.test(value)) {
      dispatch({ type: "SET_PHONENUM_VALUE", payload: event.target.value });
    }
  };
  const handleAddressChange = (event) => {
    dispatch({ type: "SET_ADDRESS_VALUE", payload: event.target.value });
  };
  const handleNNChange = (event) => {
    dispatch({ type: "SET_NN_VALUE", payload: event.target.value });
  };
  const handleGTChange = (event) => {
    dispatch({ type: "SET_GT_VALUE", payload: event.target.value });
  };
  const handleTTChange = (event) => {
    dispatch({ type: "SET_TT_VALUE", payload: event.target.value });
  };
  const handlePXChange = (event) => {
    dispatch({ type: "SET_PX_VALUE", payload: event.target.value });
  };
  const handleQHChange = (event) => {
    dispatch({ type: "SET_QH_VALUE", payload: event.target.value });
  };
  const handleNSChange = (event) => {
    dispatch({ type: "SET_NS_VALUE", payload: event.target.value });
  };
  const handleEmailChange = (event) => {
    dispatch({ type: "SET_EMAIL_VALUE", payload: event.target.value });
  };
  const handlePeopleChange = (event) => {
    dispatch({ type: "SET_PEOPLE_VALUE", payload: event.target.value });
  };
  const handleIdentityChange = (event) => {
    dispatch({ type: "SET_IDENTITY_VALUE", payload: event.target.value });
  };

  const handleFullNameBlur = () => {
    const trimmedValue = state.inputFullNameValue.trim();
    if (trimmedValue === "" || !trimmedValue.trim().includes(" ")) {
      dispatch({ type: "SHOW_FULLNAME_ERROR" });
    } else {
      dispatch({ type: "HIDE_FULLNAME_ERROR" });
    }
  };
  const handlePhoneNumBlur = () => {
    const trimmedValue = state.inputPhoneNumValue.trim();
    if (trimmedValue === "") {
      dispatch({ type: "SHOW_PHONENUM_ERROR" });
    } else {
      dispatch({ type: "HIDE_PHONENUM_ERROR" });
    }
  };
  const handleFormatPhoneNumCheck = () => {
    const trimmedValue = state.inputPhoneNumValue.trim();
    if (!/^0\d{9}$/.test(trimmedValue)) {
      dispatch({ type: "CHECK_FORMAT_PHONENUM_TRUE" });
      dispatch({ type: "HIDE_PHONENUM_ERROR" });
    } else {
      dispatch({ type: "CHECK_FORMAT_PHONENUM_FALSE" });
    }
  };

  const handleAddressBlur = () => {
    const trimmedValue = state.inputAddressValue.trim();
    if (trimmedValue === "") {
      dispatch({ type: "SHOW_ADDRESS_ERROR" });
    } else {
      dispatch({ type: "HIDE_ADDRESS_ERROR" });
    }
  };
  const handleNNBlur = () => {
    const trimmedValue = state.inputNNValue.trim();
    if (trimmedValue === "0") {
      dispatch({ type: "SHOW_NN_ERROR" });
    } else {
      dispatch({ type: "HIDE_NN_ERROR" });
    }
  };
  const handleDTBlur = () => {
    const trimmedValue = state.inputDTValue.trim();
    if (trimmedValue === "0") {
      dispatch({ type: "SHOW_NN_ERROR" });
    } else {
      dispatch({ type: "HIDE_NN_ERROR" });
    }
  };
  const handleGTBlur = () => {
    const trimmedValue = state.inputGTValue.trim();
    if (trimmedValue === "-1") {
      dispatch({ type: "SHOW_GT_ERROR" });
    } else {
      dispatch({ type: "HIDE_GT_ERROR" });
    }
  };
  const handleTTBlur = () => {
    const trimmedValue = state.inputTTValue.trim();
    console.log(trimmedValue);
    if (trimmedValue === "0") {
      dispatch({ type: "SHOW_TT_ERROR" });
    } else {
      dispatch({ type: "HIDE_TT_ERROR" });
    }
  };
  const handlePXBlur = () => {
    const trimmedValue = state.inputPXValue.trim();
    if (trimmedValue === "0") {
      dispatch({ type: "SHOW_PX_ERROR" });
    } else {
      dispatch({ type: "HIDE_PX_ERROR" });
    }
  };
  const handleQHBlur = () => {
    const trimmedValue = state.inputQHValue.trim();
    if (trimmedValue === "0") {
      dispatch({ type: "SHOW_QH_ERROR" });
    } else {
      dispatch({ type: "HIDE_QH_ERROR" });
    }
  };
  const handleNSBlur = () => {
    const trimmedValue = state.inputNSValue.trim();
    if (trimmedValue === "") {
      dispatch({ type: "SHOW_NS_ERROR" });
    } else {
      dispatch({ type: "HIDE_NS_ERROR" });
    }
  };
  const handleSubmitClick = () => {
    if (
      state.inputFullNameValue === "" ||
      !state.inputFullNameValue.trim().includes(" ")
    ) {
      dispatch({ type: "SHOW_FULLNAME_ERROR" });
    }
    if (state.inputPhoneNumValue === "") {
      dispatch({ type: "SHOW_PHONENUM_ERROR" });
    }
    if (state.inputAddressValue === "") {
      dispatch({ type: "SHOW_ADDRESS_ERROR" });
    }
    if (state.inputNNValue === "0") {
      dispatch({ type: "SHOW_NN_ERROR" });
    }
    if (state.inputGTValue === "-1") {
      dispatch({ type: "SHOW_GT_ERROR" });
    }
    if (state.inputTTValue === "0") {
      dispatch({ type: "SHOW_TT_ERROR" });
    }
    if (state.inputPXValue === "0") {
      dispatch({ type: "SHOW_PX_ERROR" });
    }
    if (state.inputQHValue === "0") {
      dispatch({ type: "SHOW_QH_ERROR" });
    }
    if (state.inputNSValue === "") {
      dispatch({ type: "SHOW_NS_ERROR" });
    }
  };
  const HandleRewriteClick = () => {
    dispatch({ type: "RESET" });
  };

  const validateForm = () => {
    let isValid = true;

    // Kiểm tra từng trường input
    if (
      state.inputFullNameValue.trim() === "" ||
      !state.inputFullNameValue.trim().includes(" ")
    ) {
      dispatch({ type: "SHOW_FULLNAME_ERROR" });
      isValid = false;
    }
    if (state.inputPhoneNumValue === "") {
      dispatch({ type: "SHOW_PHONENUM_ERROR" });
      isValid = false;
    }
    if (state.inputAddressValue.trim() === "") {
      dispatch({ type: "SHOW_ADDRESS_ERROR" });
      isValid = false;
    }
    if (state.inputNNValue === "0") {
      dispatch({ type: "SHOW_NN_ERROR" });
      isValid = false;
    }
    if (state.inputGTValue === "-1") {
      dispatch({ type: "SHOW_GT_ERROR" });
      isValid = false;
    }
    if (selectedProvince === "") {
      // Nếu không chọn tỉnh thành
      isValid = false;
    }
    if (selectedDistrict === "") {
      // Nếu không chọn quận huyện
      isValid = false;
    }
    if (selectedWard === "") {
      // Nếu không chọn phường xã
      isValid = false;
    }
    if (state.inputNSValue.trim() === "") {
      dispatch({ type: "SHOW_NS_ERROR" });
      isValid = false;
    }

    return isValid;
  };

  const fetchService = async () => {
    try {
      const data = {
        fullName: state.inputFullNameValue,
        email: state.inputEmailValue,
        date: state.inputNSValue,
        sex: state.inputGTValue,
        career: state.inputNNValue,
        address:
          state.inputAddressValue +
          ", " +
          selectedWard +
          ", " +
          selectedDistrict +
          ", " +
          selectedProvince,
        phoneNumber: state.inputPhoneNumValue,
        identityCardNumber: state.inputCMNDValue,
        peoples: state.inputDTValue,
      };
      console.log(data);
      const res = await recordApi.updateRecord(
        axiosJWT,
        navigated,
        accessToken,
        id,
        data
      );
      return res; // Trả về phản hồi từ API
    } catch (error) {
      console.error("Error fetching news:", error);
      throw error; // Ném ra lỗi để bắt ở nơi gọi fetchService
    }
  };
  const handleMakeMedicalRecord = (e) => {
    e.preventDefault();
    const isValidForm = validateForm();

    if (isValidForm) {
      fetchService()
        .then((res) => {
          if (res !== null) {
            navigate("/Profile", {
              state: { message: "Cập nhật hồ sơ thành công!" },
            });
          } else {
            toast("Đã xảy ra lỗi khi cập nhật hồ sơ.");
          }
        })
        .catch((error) => {
          console.error("Error handling medical record creation:", error);
          toast("Đã xảy ra lỗi khi cập nhật hồ sơ.");
        });
    } else {
      toast("Vui lòng điền đầy đủ thông tin trước cập nhật hồ sơ!");
    }
  };
  const [provinces, setProvinces] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);
  const [selectedProvince, setSelectedProvince] = useState("");
  const [selectedDistrict, setSelectedDistrict] = useState("");
  const [selectedWard, setSelectedWard] = useState("");
  useEffect(() => {
    const fetchData = async () => {
      await fetchProvinces();
      await fetchDistrictsNews();
      await fetchWardsNews();
      await fetchRecord();
    };

    fetchData();
  }, [id]);
  const fetchProvinces = async () => {
    try {
      const response = await fetch("https://esgoo.net/api-tinhthanh/1/0.htm");
      const data = await response.json();
      if (data.error === 0) {
        setProvinces(data.data);
      }
    } catch (error) {
      console.error("Error fetching provinces:", error);
    }
  };
  const handleProvinceChange = (e) => {
    if (typeof e === "object") {
      handleTTChange(e);
      const fullNameProvince = e.target.value;
      const selectedProvinceObj = provinces.find(
        (x) => x.full_name === fullNameProvince
      );
      if (selectedProvinceObj) {
        setSelectedProvince(fullNameProvince);
        setSelectedDistrict("");
        fetchDistricts(selectedProvinceObj.id);
      }
    } else if (typeof e === "string") {
      const selectedProvinceObj = provinces.find(
        (x) => x.full_name === e.trim()
      );
      console.log(selectedProvinceObj);
      if (selectedProvinceObj) {
        fetchDistricts(selectedProvinceObj.id);
        setSelectedProvince(e);
        setSelectedDistrict("");
        console.log(e);
      }
    }
  };
  const fetchDistricts = async (idProvince) => {
    try {
      const response = await fetch(
        `https://esgoo.net/api-tinhthanh/2/${idProvince}.htm`
      );
      const data = await response.json();
      if (data.error === 0) {
        setDistricts(data.data);
        console.log(data.data);
      }
    } catch (error) {
      console.error("Error fetching districts:", error);
    }
  };

  const handleDistrictChange = (e) => {
    if (typeof e === "object") {
      handleQHChange(e);
      const fullNameDistrict = e.target.value;
      const selectedDistrictObj = districts.find(
        (x) => x.full_name === fullNameDistrict
      );
      if (selectedDistrictObj) {
        setSelectedDistrict(fullNameDistrict);
        fetchWards(selectedDistrictObj.id);
      }
    } else {
      const selectedDistrictObj = districts.find((x) => x.full_name === e);
      if (selectedDistrictObj) {
        setSelectedDistrict(e);
        fetchWards(selectedDistrictObj.id);
      }
    }
  };

  const fetchWards = async (idDistrict) => {
    try {
      const response = await fetch(
        `https://esgoo.net/api-tinhthanh/3/${idDistrict}.htm`
      );
      const data = await response.json();
      if (data.error === 0) {
        setWards(data.data);
      }
    } catch (error) {
      console.error("Error fetching wards:", error);
    }
  };
  const handleWardChange = (e) => {
    if (typeof e === "object") {
      console.log(e.target);
      handlePXChange(e);
      const fullNameWard = e.target.value;
      const selectedWardObj = wards.find((x) => x.full_name === fullNameWard);
      if (selectedWardObj) {
        setSelectedWard(fullNameWard);
      }
    } else {
      const selectedWardObj = wards.find((x) => x.full_name === e);
      if (selectedWardObj) {
        setSelectedWard(e);
      }
    }
  };
  const fetchDistrictsNews = async () => {
    try {
      const dataRecordById = await recordApi.getRecordById(
        axiosJWT,
        navigated,
        accessToken,
        id
      );
      const response = await fetch(
        `https://esgoo.net/api-tinhthanh/2/${dataRecordById.data.province}.htm`
      );
      const data = await response.json();
      if (data.error === 0) {
        setDistricts(data.data);
        console.log(data.data);
      }
    } catch (error) {
      console.error("Error fetching districts:", error);
    }
  };
  const fetchWardsNews = async () => {
    try {
      const dataRecordById = await recordApi.getRecordById(
        axiosJWT,
        navigated,
        accessToken,
        id
      );
      const response = await fetch(
        `https://esgoo.net/api-tinhthanh/3/${dataRecordById.data.district}.htm`
      );
      const data = await response.json();
      if (data.error === 0) {
        setWards(data.data);
      }
    } catch (error) {
      console.error("Error fetching wards:", error);
    }
  };
  const fetchRecord = async () => {
    try {
      const dataRecord = await recordApi.getRecordById(
        axiosJWT,
        navigated,
        accessToken,
        id
      );
      const dataRecordById = dataRecord.data;
      const address = dataRecordById.address.split(",");
      dispatch({
        type: "SET_FULLNAME_VALUE",
        payload: dataRecordById.fullName,
      });
      dispatch({
        type: "SET_PHONENUM_VALUE",
        payload: dataRecordById.phoneNumber,
      });
      dispatch({ type: "SET_ADDRESS_VALUE", payload: address[0].trim() });
      dispatch({ type: "SET_NN_VALUE", payload: dataRecordById.career });
      dispatch({
        type: "SET_NS_VALUE",
        payload: new Date(dataRecordById.date).toISOString().split("T")[0],
      });
      dispatch({ type: "SET_GT_VALUE", payload: dataRecordById.sex });
      dispatch({ type: "SET_EMAIL_VALUE", payload: dataRecordById.email });
      dispatch({ type: "SET_PEOPLE_VALUE", payload: dataRecordById.peoples });
      dispatch({
        type: "SET_IDENTITY_VALUE",
        payload: dataRecordById.identityCardNumber,
      });
      dispatch({ type: "SET_TT_VALUE", payload: address[3].trim() });
      dispatch({ type: "SET_QH_VALUE", payload: address[2].trim() });
      dispatch({ type: "SET_PX_VALUE", payload: address[1].trim() });

      // Đặt giá trị tỉnh thành, quận huyện và phường xã từ địa chỉ
      const provinceName = address[3].trim();
      const districtName = address[2].trim();
      const wardName = address[1].trim();
      setSelectedProvince(provinceName);
      setSelectedDistrict(districtName);
      setSelectedWard(wardName);
    } catch (error) {
      console.error("Error fetching record:", error);
    }
  };

  return (
    <div className="w-full flex items-center justify-center bg-white">
      <div className="w-7/12 my-5">
        <div className="w-full flex flex-wrap justify-center items-center">
          <div className="w-full flex justify-center items-center text-center font-semibold text-xl uppercase my-5">
            <Header2>Tạo hồ sơ mới</Header2>
          </div>
          <div className="w-full">
            <form onSubmit={handleMakeMedicalRecord}>
              <div className="uppercase font-bold text-[#6c757d] w-full flex justify-center items-center my-5">
                <h3>Nhập thông tin bệnh nhân</h3>
              </div>
              <div className="w-full bg-[#ffc2d1] px-4 py-3 rounded-md text-[#a4133c]">
                Vui lòng cung cấp thông tin chính xác để được phục vụ tốt nhất.
                Trong trường hợp cung cấp sai thông tin bệnh nhân & điện thoại,
                việc xác nhận cuộc hẹn sẽ không hiệu lực trước khi đặt khám.
              </div>
              <div className="text-red-500 mx-5 text-xs font-semibold">
                (*) Thông tin bắt buộc nhập
              </div>
              <div className="w-full flex mb-4 flex-wrap">
                <div className="lg:w-1/2 flex flex-wrap justify-center">
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="fullName"
                    >
                      Họ và tên (có dấu)<sup className="text-red-500">*</sup>
                    </label>
                    <Input
                      className=" px-3 py-2 rounded-md border w-full mx-5 border-solid outline-none h-10 text-[#495057] focus:border-[#ffb3c6]"
                      type="text"
                      // name="fullName"
                      id="fullName"
                      value={state.inputFullNameValue}
                      onChange={handleFUllNameChange}
                      onBlur={handleFullNameBlur}
                      placeholder="Ví dụ: Trương Lê Minh Nhật"
                    />
                    {(state.showFullNameError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng nhập cả họ và tên!!
                      </p>
                    )) ||
                      (state.showNSError && (
                        <p className="text-red-500 mx-5 text-xs font-semibold w-full h-4">
                          {" "}
                        </p>
                      ))}
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="SDT"
                    >
                      Số điện thoại<sup className="text-red-500">*</sup>
                    </label>
                    <Input
                      className=" px-3 py-2 rounded-md border w-full mx-5 border-solid outline-none h-10 text-[#495057] focus:border-[#ffb3c6]"
                      type="text"
                      // name="phoneNumber"
                      id="SDT"
                      value={state.inputPhoneNumValue}
                      onChange={handlePhoneNumChange}
                      onKeyUp={handleFormatPhoneNumCheck}
                      onBlur={handlePhoneNumBlur}
                      placeholder="Nhập số điện thoại"
                    />
                    {(state.showPhoneNumError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng nhập số điện thoại!!
                      </p>
                    )) ||
                      (state.checkFormatPhoneNum && (
                        <p className="text-red-500 mx-5 text-xs font-semibold">
                          Vui lòng nhập đúng định dạng số điện thoại!!
                        </p>
                      )) ||
                      (state.showGTError && (
                        <p className="text-red-500 mx-5 text-xs font-semibold w-full h-4">
                          {" "}
                        </p>
                      ))}
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="NN"
                    >
                      Nghề nghiệp<sup className="text-red-500">*</sup>
                    </label>
                    <Select
                      id="NN"
                      // name="career"
                      value={state.inputNNValue}
                      onBlur={handleNNBlur}
                      onChange={handleNNChange}
                    >
                      <option value="0">Chọn nghề nghiệp</option>
                      <option value="Bác sĩ">Bác sĩ</option>
                      <option value="Điều dưỡng">Điều dưỡng</option>
                      <option value="Y sĩ">Y sĩ</option>
                      <option value="Dược sĩ">Dược sĩ</option>
                      <option value="Nha sĩ">Nha sĩ</option>
                      <option value="Nha công">Nha công</option>
                      <option value="Dược sĩ trung cấp">
                        Dược sĩ trung cấp
                      </option>
                      <option value="Cử nhân Điều dưỡng">
                        Cử nhân Điều dưỡng
                      </option>
                      <option value="Hộ lý">Hộ lý</option>
                      <option value="Kỹ sư điện">Kỹ sư điện</option>
                      <option value="Kỹ sư xây dựng">Kỹ sư xây dựng</option>
                      <option value="Kỹ sư hóa">Kỹ sư hóa</option>
                      <option value="Kỹ sư địa chất">Kỹ sư địa chất</option>
                      <option value="Kỹ sư nông nghiệp">
                        Kỹ sư nông nghiệp
                      </option>
                      <option value="Kỹ sư lâm nghiệp">Kỹ sư lâm nghiệp</option>
                      <option value="Bác sĩ thú y">Bác sĩ thú y</option>
                      <option value="Lương y">Lương y</option>
                      <option value="Bác sĩ y học cổ truyền">
                        Bác sĩ y học cổ truyền
                      </option>
                      <option value="Kiến trúc sư">Kiến trúc sư</option>
                      <option value="Làm nông">Làm nông</option>
                      <option value="Nhà báo">Nhà báo</option>
                      <option value="Luật sư">Luật sư</option>
                      <option value="Kỹ sư công nghệ thông tin">
                        Kỹ sư công nghệ thông tin
                      </option>
                      <option value="Làm ruộng">Làm ruộng</option>
                      <option value="Thợ mộc">Thợ mộc</option>
                      <option value="Thợ làm bánh">Thợ làm bánh</option>
                      <option value="Thợ may">Thợ may</option>
                      <option value="Công nhân may">Công nhân may</option>
                      <option value="Thiết kế thời trang">
                        Thiết kế thời trang
                      </option>
                      <option value="Thợ uốn tóc">Thợ uốn tóc</option>
                      <option value="Tài xế">Tài xế</option>
                      <option value="Buôn bán">Buôn bán</option>
                      <option value="Kinh doanh">Kinh doanh</option>
                      <option value="Giám đốc">Giám đốc</option>
                      <option value="Quản lý">Quản lý</option>
                      <option value="Quản đốc">Quản đốc</option>
                      <option value="Thư ký hành chính">
                        Thư ký hành chính
                      </option>
                      <option value="Thư ký y khoa">Thư ký y khoa</option>
                      <option value="Thợ làm tóc">Thợ làm tóc</option>
                      <option value="Thợ sửa xe">Thợ sửa xe</option>
                      <option value="Đầu bếp">Đầu bếp</option>
                      <option value="Nhân viên bán hàng">
                        Nhân viên bán hàng
                      </option>
                      <option value="Sinh viên">Sinh viên</option>
                      <option value="Học sinh">Học sinh</option>
                      <option value="Hưu trí">Hưu trí</option>
                      <option value="Giáo viên">Giáo viên</option>
                      <option value="Giảng viên">Giảng viên</option>
                      <option value="Thợ hồ">Thợ hồ</option>
                      <option value="Nail">Nail</option>
                      <option value="Kỹ thuật viên vi tính">
                        Kỹ thuật viên vi tính
                      </option>
                      <option value="Quản trị nhân sự">Quản trị nhân sự</option>
                      <option value="Kế toán">Kế toán</option>
                      <option value="Bác sĩ răng hàm mặt">
                        Bác sĩ răng hàm mặt
                      </option>
                      <option value="Marketing">Marketing</option>
                      <option value="Ca sĩ">Ca sĩ</option>
                      <option value="Nhạc sĩ">Nhạc sĩ</option>
                      <option value="Nhạc công">Nhạc công</option>
                      <option value="Diễn viên điện ảnh">
                        Diễn viên điện ảnh
                      </option>
                      <option value="Diễn viên sân khấu">
                        Diễn viên sân khấu
                      </option>
                      <option value="Biên kịch">Biên kịch</option>
                      <option value="Biên tập viên">Biên tập viên</option>
                      <option value="Phát thanh viên">Phát thanh viên</option>
                      <option value="Trình dược viên">Trình dược viên</option>
                      <option value="Nội trợ">Nội trợ</option>
                      <option value="Tự do">Tự do</option>
                      <option value="Làm thuê">Làm thuê</option>
                      <option value="Họa sĩ">Họa sĩ</option>
                      <option value="PR">PR</option>
                      <option value="Thợ làm cửa sắt">Thợ làm cửa sắt</option>
                      <option value="Thu ngân">Thu ngân</option>
                      <option value="Bộ đội">Bộ đội</option>
                      <option value="Công an">Công an</option>
                      <option value="Thủy thủ">Thủy thủ</option>
                      <option value="Tiếp viên hàng không">
                        Tiếp viên hàng không
                      </option>
                      <option value="Tiếp viên nhà hàng">
                        Tiếp viên nhà hàng
                      </option>
                      <option value="Hướng dẫn viên du lịch">
                        Hướng dẫn viên du lịch
                      </option>
                      <option value="Dược tá">Dược tá</option>
                      <option value="Làm vuông">Làm vuông</option>
                      <option value="Chăn nuôi">Chăn nuôi</option>
                      <option value="Thợ dệt">Thợ dệt</option>
                      <option value="Bảo vệ">Bảo vệ</option>
                      <option value="Nhiếp ảnh gia">Nhiếp ảnh gia</option>
                      <option value="Vệ sĩ">Vệ sĩ</option>
                      <option value="Kế toán trưởng">Kế toán trưởng</option>
                      <option value="IT">IT</option>
                      <option value="Phi công">Phi công</option>
                      <option value="Thuyền trưởng">Thuyền trưởng</option>
                      <option value="Tài công">Tài công</option>
                      <option value="Phụ bếp">Phụ bếp</option>
                      <option value="Đánh cá">Đánh cá</option>
                      <option value="Công nhân">Công nhân</option>
                      <option value="Nhân viên văn phòng">
                        Nhân viên văn phòng
                      </option>
                      <option value="làm công">làm công</option>
                      <option value="Làm rẫy">Làm rẫy</option>
                      <option value="Cán bộ">Cán bộ</option>
                      <option value="Viên chức">Viên chức</option>
                      <option value="Công chức">Công chức</option>
                      <option value="Kỹ Sư">Kỹ Sư</option>
                      <option value="Tu sĩ">Tu sĩ</option>
                      <option value="Công nhân viên">Công nhân viên</option>
                      <option value="Không">Không</option>
                      <option value="Kỹ sư cơ khí">Kỹ sư cơ khí</option>
                      <option value="Nhân viên ngân hàng">
                        Nhân viên ngân hàng
                      </option>
                      <option value="Hải quan">Hải quan</option>
                      <option value="Linh mục">Linh mục</option>
                      <option value="Quân nhân">Quân nhân</option>
                      <option value="Kỹ sư viễn thông">Kỹ sư viễn thông</option>
                      <option value="Đi biển">Đi biển</option>
                      <option value="Nhân viên hành chính">
                        Nhân viên hành chính
                      </option>
                      <option value="Làm vườn">Làm vườn</option>
                      <option value="Nhân viên phục vụ">
                        Nhân viên phục vụ
                      </option>
                      <option value="Lái tàu biển">Lái tàu biển</option>
                      <option value="Kỹ thuận viên điện tử">
                        Kỹ thuận viên điện tử
                      </option>
                      <option value="Kỹ sư môi trường">Kỹ sư môi trường</option>
                      <option value="Thợ Máy">Thợ Máy</option>
                      <option value="Thủ Kho">Thủ Kho</option>
                      <option value="Làm gốm">Làm gốm</option>
                      <option value="Vận động viên">Vận động viên</option>
                      <option value="Thợ tiện">Thợ tiện</option>
                      <option value="Thợ hàn">Thợ hàn</option>
                      <option value="Phiên dịch viên">Phiên dịch viên</option>
                      <option value="Thiết kế đồ họa">Thiết kế đồ họa</option>
                      <option value="Công nhân in ấn">Công nhân in ấn</option>
                      <option value="Bảo trì">Bảo trì</option>
                      <option value="Phóng viên">Phóng viên</option>
                      <option value="Tạp vụ">Tạp vụ</option>
                      <option value="Thợ bạc">Thợ bạc</option>
                      <option value="Nhân viên Lễ tân">Nhân viên Lễ tân</option>
                      <option value="Thợ điện">Thợ điện</option>
                      <option value="Nhân viên bảo hiểm">
                        Nhân viên bảo hiểm
                      </option>
                      <option value="Nữ hộ sinh">Nữ hộ sinh</option>
                      <option value="Thợ nhuộm">Thợ nhuộm</option>
                      <option value="Thợ rèn">Thợ rèn</option>
                      <option value="Nhân viên thiết kế">
                        Nhân viên thiết kế
                      </option>
                      <option value="Công nhân cơ khí">Công nhân cơ khí</option>
                      <option value="Nhân viên kinh doanh">
                        Nhân viên kinh doanh
                      </option>
                      <option value="Kỹ thuật viên xét nghiệm">
                        Kỹ thuật viên xét nghiệm
                      </option>
                      <option value="Bảo mẫu">Bảo mẫu</option>
                      <option value="Giáo viên mầm non">
                        Giáo viên mầm non
                      </option>
                      <option value="Thợ sơn">Thợ sơn</option>
                      <option value="Kỹ Thuật viên">Kỹ Thuật viên</option>
                      <option value="Kỹ thuật viên thẩm mỹ">
                        Kỹ thuật viên thẩm mỹ
                      </option>
                      <option value="Viễn thông">Viễn thông</option>
                      <option value="Kỹ sư tin học">Kỹ sư tin học</option>
                      <option value="Đan len">Đan len</option>
                      <option value="Kiểm toán">Kiểm toán</option>
                      <option value="Thẩm phán">Thẩm phán</option>
                      <option value="Quay phim">Quay phim</option>
                      <option value="Thợ xây">Thợ xây</option>
                      <option value="Trang trí nội thất">
                        Trang trí nội thất
                      </option>
                      <option value="Nghiên cứu sinh">Nghiên cứu sinh</option>
                      <option value="Thợ sửa điện tử">Thợ sửa điện tử</option>
                      <option value="Cấp dưỡng">Cấp dưỡng</option>
                      <option value="Trang điểm">Trang điểm</option>
                      <option value="Cầu thủ">Cầu thủ</option>
                      <option value="Thợ cơ khí">Thợ cơ khí</option>
                      <option value="Nhà văn">Nhà văn</option>
                      <option value="Nhà thơ">Nhà thơ</option>
                      <option value="Thủy sản">Thủy sản</option>
                      <option value="Huyến luyện viên">Huyến luyện viên</option>
                      <option value="Truyền thông">Truyền thông</option>
                      <option value="Huấn luyện viên">Huấn luyện viên</option>
                      <option value="Thợ lặn">Thợ lặn</option>
                      <option value="Giám định">Giám định</option>
                      <option value="KCS">KCS</option>
                      <option value="Nhân viên tư vấn">Nhân viên tư vấn</option>
                      <option value="Kỹ thuật điện">Kỹ thuật điện</option>
                      <option value="HR">HR</option>
                      <option value="Tổng biên tập">Tổng biên tập</option>
                      <option value="Kiểm lâm">Kiểm lâm</option>
                      <option value="Thư ký Giám đốc">Thư ký Giám đốc</option>
                      <option value="Chuyên viên">Chuyên viên</option>
                      <option value="Đạo diễn">Đạo diễn</option>
                      <option value="Thông dịch viên">Thông dịch viên</option>
                      <option value="Mục sư">Mục sư</option>
                      <option value="Lao công">Lao công</option>
                      <option value="Giữ xe">Giữ xe</option>
                      <option value="Lập trình viên">Lập trình viên</option>
                      <option value="Kiểm ngư">Kiểm ngư</option>
                      <option value="Thương binh">Thương binh</option>
                      <option value="Văn thư">Văn thư</option>
                      <option value="Nhân viên pháp lý">
                        Nhân viên pháp lý
                      </option>
                      <option value="Tiến sĩ Y khoa">Tiến sĩ Y khoa</option>
                      <option value="Khuyết tật">Khuyết tật</option>
                      <option value="Mất sức lao động">Mất sức lao động</option>
                      <option value="Thanh tra giao thông">
                        Thanh tra giao thông
                      </option>
                      <option value="Bartender">Bartender</option>
                      <option value="Giúp việc">Giúp việc</option>
                      <option value="Bốc xếp">Bốc xếp</option>
                      <option value="Thợ hớt tóc">Thợ hớt tóc</option>
                      <option value="Lao động phổ thông">
                        Lao động phổ thông
                      </option>
                      <option value="Nhân viên hóa trang">
                        Nhân viên hóa trang
                      </option>
                      <option value="Nhân viên phục trang">
                        Nhân viên phục trang
                      </option>
                      <option value="Dân quân tự vệ">Dân quân tự vệ</option>
                      <option value="Vũ công">Vũ công</option>
                      <option value="Điêu khắc">Điêu khắc</option>
                      <option value="Nhân viên chăm sóc khách hàng">
                        Nhân viên chăm sóc khách hàng
                      </option>
                      <option value="Nhân viên massage">
                        Nhân viên massage
                      </option>
                      <option value="Nhân viên Hàng hải">
                        Nhân viên Hàng hải
                      </option>
                      <option value="Kỹ thuật cơ khí">Kỹ thuật cơ khí</option>
                      <option value="Kỹ sư điện tử viễn thông">
                        Kỹ sư điện tử viễn thông
                      </option>
                      <option value="phụ xe">phụ xe</option>
                      <option value="trợ lý giám đốc">trợ lý giám đốc</option>
                      <option value="Lái cần cẩu">Lái cần cẩu</option>
                      <option value="Giáo sư">Giáo sư</option>
                      <option value="Cử nhân xét nghiệm">
                        Cử nhân xét nghiệm
                      </option>
                      <option value="Kiểm sát viên">Kiểm sát viên</option>
                      <option value="Khác">Khác</option>
                      <option value="Thuyền viên">Thuyền viên</option>
                      <option value="Giám Đốc">Giám Đốc</option>
                      <option value="Phó Giám Đốc">Phó Giám Đốc</option>
                      <option value="Nhân viên dịch vụ khách hàng">
                        Nhân viên dịch vụ khách hàng
                      </option>
                      <option value="Nhân viên Tiếp thị">
                        Nhân viên Tiếp thị
                      </option>
                      <option value="Điều phối viên">Điều phối viên</option>
                      <option value="Tiến sĩ công nghệ thông tin">
                        Tiến sĩ công nghệ thông tin
                      </option>
                      <option value="Biên dịch">Biên dịch</option>
                      <option value="Nghệ nhân">Nghệ nhân</option>
                      <option value="Thiết kế đồ họa">Thiết kế đồ họa</option>
                      <option value="Thủ quỹ">Thủ quỹ</option>
                      <option value="Biên đạo múa">Biên đạo múa</option>
                      <option value="Giao dịch viên">Giao dịch viên</option>
                      <option value="Võ sư">Võ sư</option>
                      <option value="Quảng cáo">Quảng cáo</option>
                      <option value="Cử nhân Công nghệ sinh học">
                        Cử nhân Công nghệ sinh học
                      </option>
                      <option value="Còn nhỏ">Còn nhỏ</option>
                      <option value="Thợ nhôm">Thợ nhôm</option>
                    </Select>
                    {state.showNNError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng chọn nghề nghiệp!!
                      </p>
                    )}
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="Email"
                    >
                      Địa chỉ Email
                    </label>
                    <Input
                      className=" px-3 py-2 rounded-md border w-full mx-5 border-solid outline-none h-10 text-[#495057] focus:border-[#ffb3c6]"
                      type="text"
                      // name="email"
                      id="Email"
                      value={state.inputEmailValue}
                      onChange={handleEmailChange}
                      placeholder="Nhập địa chỉ email để nhận phiếu khám"
                    />
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="TT"
                    >
                      Tỉnh / Thành<sup className="text-red-500">*</sup>
                    </label>
                    <Select
                      id="TT"
                      value={selectedProvince}
                      onBlur={handleTTBlur}
                      onChange={handleProvinceChange}
                    >
                      <option value="0">Tỉnh Thành</option>
                      {provinces.map((province) => (
                        <option key={province.id} value={province.full_name}>
                          {province.full_name}
                        </option>
                      ))}
                    </Select>
                    {state.showTTError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng chọn tỉnh thành!!
                      </p>
                    )}
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="PX"
                    >
                      Phường / Xã<sup className="text-red-500">*</sup>
                    </label>
                    <Select
                      id="PX"
                      value={selectedWard}
                      onBlur={handlePXBlur}
                      onChange={handleWardChange}
                    >
                      <option value="0">Phường Xã</option>
                      {wards.map((ward) => (
                        <option key={ward.id} value={ward.full_name}>
                          {ward.full_name}
                        </option>
                      ))}
                    </Select>
                    {state.showPXError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng chọn phường xã!!
                      </p>
                    )}
                  </div>
                </div>
                <div className="lg:w-1/2 flex flex-wrap justify-start">
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="NS"
                    >
                      Ngày tháng năm sinh<sup className="text-red-500">*</sup>
                    </label>
                    <Input
                      className=" px-3 py-2 rounded-md border w-full mx-5 border-solid outline-none h-10 text-[#495057] focus:border-[#ffb3c6]"
                      type="date"
                      // name="date"
                      id="NS"
                      value={state.inputNSValue}
                      onChange={handleNSChange}
                      onBlur={handleNSBlur}
                    />
                    {(state.showNSError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng chọn ngày tháng năm sinh!!
                      </p>
                    )) ||
                      (state.showFullNameError && (
                        <p className="text-red-500 mx-5 text-xs font-semibold w-full h-4">
                          {" "}
                        </p>
                      ))}
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="GT"
                    >
                      Giới tính<sup className="text-red-500">*</sup>
                    </label>
                    <Select
                      id="GT"
                      // name="sex"
                      value={state.inputGTValue}
                      onBlur={handleGTBlur}
                      onChange={handleGTChange}
                    >
                      <option value="-1">Chọn giới tính</option>
                      <option value="Nam">Nam</option>
                      <option value="Nữ">Nữ</option>
                    </Select>
                    {(state.showGTError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng chọn giới tính!!
                      </p>
                    )) ||
                      (state.showPhoneNumError && (
                        <p className="text-red-500 mx-5 text-xs font-semibold w-full h-4">
                          {" "}
                        </p>
                      )) ||
                      (state.checkFormatPhoneNum && (
                        <p className="text-red-500 mx-5 text-xs font-semibold w-full h-4">
                          {" "}
                        </p>
                      ))}
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="CMND"
                    >
                      Số CMND/Passport
                    </label>
                    <Input
                      className=" px-3 py-2 rounded-md border w-full mx-5 border-solid outline-none h-10 text-[#495057] focus:border-[#ffb3c6]"
                      type="text"
                      // name="IdentityCardNumber"
                      id="CMND"
                      value={state.inputCMNDValue}
                      onChange={handleIdentityChange}
                      placeholder="Nhập số CMND"
                    />
                    {state.showNNError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold w-full h-4">
                        {" "}
                      </p>
                    )}
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="DT"
                    >
                      Dân tộc
                    </label>
                    <Select
                      id="DT"
                      // name="Peoples"
                      value={state.inputDTValue}
                      onBlur={handleDTBlur}
                      onChange={handlePeopleChange}
                    >
                      <option value="Chọn dân tộc">Chọn dân tộc</option>
                      <option value="Kinh">Kinh</option>
                      <option value="Hoa">Hoa</option>
                      <option value="Khơ-me">Khơ-me</option>
                      <option value="Chăm">Chăm</option>
                      <option value="Thái">Thái</option>
                      <option value="Mường">Mường</option>
                      <option value="Tày">Tày</option>
                      <option value="Ba-na">Ba-na</option>
                      <option value="Ê-đê">Ê-đê</option>
                      <option value="Gia-rai">Gia-rai</option>
                      <option value="Nùng">Nùng</option>
                      <option value="Chơ-ro">Chơ-ro</option>
                      <option value="Xtiêng">Xtiêng</option>
                      <option value="Dao">Dao</option>
                      <option value="Hmông">Hmông</option>
                      <option value="Ngái">Ngái</option>
                      <option value="Xơ-đăng">Xơ-đăng</option>
                      <option value="Sán Chay">Sán Chay</option>
                      <option value="Kơ-ho">Kơ-ho</option>
                      <option value="Sán Dìu">Sán Dìu</option>
                      <option value="Hrê">Hrê</option>
                      <option value="Mnông">Mnông</option>
                      <option value="Ra-glai">Ra-glai</option>
                      <option value="Bru-Vân Kiều">Bru-Vân Kiều</option>
                      <option value="Thổ">Thổ</option>
                      <option value="Giáy">Giáy</option>
                      <option value="Cơ-tu">Cơ-tu</option>
                      <option value="Gié-Triêng">Gié-Triêng</option>
                      <option value="Mạ">Mạ</option>
                      <option value="Khơ-mú">Khơ-mú</option>
                      <option value="Co">Co</option>
                      <option value="Ta-ôi">Ta-ôi</option>
                      <option value="Kháng">Kháng</option>
                      <option value="Xinh-mun">Xinh-mun</option>
                      <option value="Hà Nhì">Hà Nhì</option>
                      <option value="Chu-ru">Chu-ru</option>
                      <option value="Lào">Lào</option>
                      <option value="La Chi">La Chi</option>
                      <option value="La Ha">La Ha</option>
                      <option value="Phù Lá">Phù Lá</option>
                      <option value="La Hủ">La Hủ</option>
                      <option value="Lự">Lự</option>
                      <option value="Lô Lô">Lô Lô</option>
                      <option value="Chứt">Chứt</option>
                      <option value="Mảng">Mảng</option>
                      <option value="Pà Thẻn">Pà Thẻn</option>
                      <option value="Cơ Lao">Cơ Lao</option>
                      <option value="Cống">Cống</option>
                      <option value="Bố Y">Bố Y</option>
                      <option value="Si La">Si La</option>
                      <option value="Pu Péo">Pu Péo</option>
                      <option value="Brâu">Brâu</option>
                      <option value="Ơ Đu">Ơ Đu</option>
                      <option value="Rơ-măm">Rơ-măm</option>
                      <option value="Khác">Khác</option>
                      <option value="Nước ngoài">Nước ngoài</option>
                      <option value="Hán">Hán</option>
                      <option value="Thanh">Thanh</option>
                      <option value="Cil">Cil</option>
                      <option value="Lạch">Lạch</option>
                      <option value="Châu Mạ">Châu Mạ</option>
                      <option value="Stieng">Stieng</option>
                      <option value="Sán chỉ">Sán chỉ</option>
                      <option value="Hà Roi">Hà Roi</option>
                      <option value="Nộp">Nộp</option>
                      <option value="Cao Lan">Cao Lan</option>
                      <option value="Ấn">Ấn</option>
                      <option value="Sách">Sách</option>
                    </Select>
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="QH"
                    >
                      Quận / Huyện<sup className="text-red-500">*</sup>
                    </label>
                    <Select
                      id="QH"
                      value={selectedDistrict}
                      onBlur={handleQHBlur}
                      onChange={handleDistrictChange}
                    >
                      <option value="0">Quận Huyện</option>
                      {districts.map((district) => (
                        <option key={district.id} value={district.full_name}>
                          {district.full_name}
                        </option>
                      ))}
                    </Select>
                    {state.showQHError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng chọn quận huyện!!
                      </p>
                    )}
                  </div>
                  <div className="w-full flex flex-wrap items-start justify-start">
                    <label
                      className="w-full mb-3 font-medium mx-5 text-[#495057] mt-4"
                      htmlFor="DC"
                    >
                      Địa chỉ<sup className="text-red-500">*</sup>
                    </label>
                    <Input
                      className=" px-3 py-2 rounded-md border w-full mx-5 border-solid outline-none h-10 text-[#495057] focus:border-[#ffb3c6]"
                      type="text"
                      // name="address"
                      id="DC"
                      value={state.inputAddressValue}
                      onChange={handleAddressChange}
                      onBlur={handleAddressBlur}
                      placeholder="Nhập địa chỉ"
                    />
                    {(state.showAddressError && (
                      <p className="text-red-500 mx-5 text-xs font-semibold">
                        Vui lòng nhập số nhà, đường, thôn (ấp), xóm!!
                      </p>
                    )) ||
                      (state.showPXError && (
                        <p className="text-red-500 mx-5 text-xs font-semibold w-full h-4">
                          {" "}
                        </p>
                      ))}
                  </div>
                </div>
              </div>
              <div className="w-full flex justify-end border-t border-solid py-5 px-1 my-12">
                <Rewrite
                  className="w-28 h-9 flex items-center justify-center mx-2 pr-2"
                  onClick={HandleRewriteClick}
                >
                  <div className="flex justify-center items-center">
                    <div className="mx-2 ">
                      <IoRefreshCircleOutline></IoRefreshCircleOutline>
                    </div>
                    Nhập lại
                  </div>
                </Rewrite>
                <AddNew
                  className="w-28 h-9 flex items-center justify-center mx-2 bg-[#ff8fa3] text-white pr-2"
                  onClick={handleSubmitClick}
                  type="submit"
                  href="/Profile"
                >
                  <div className="flex justify-center items-center">
                    <div className="mx-2 ">
                      <IoPersonAdd></IoPersonAdd>
                    </div>
                    Cập nhật
                  </div>
                </AddNew>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UpdateMedicalRecord;
