import React, { useEffect, useState } from "react";
import Title from "../Title/Title";
import { Link } from "react-router-dom";
import newsApi from "../../api/newsApi";
import Loading from "../Loading/Loading";
import LoadingEdit from "../Loading/LoadingEdit";

const HomeNews = (props) => {
  const [newsList, setNewsList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    const fetchNews = async () => {
      try {
        const data = await newsApi.getAllNews();
        setNewsList(data);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
      finally {
        setIsLoading(false);
      }
    };

    fetchNews();
  }, []);
  const encode = (id) => {
    return btoa(id);
  };
  return (
    <section className={`p-5 bg-[aliceblue] ${props.className || ""}`}>
      <div className="container mx-auto">
        <Title>CÁC TIN TỨC CHÍNH</Title>
        {
          isLoading ? (
            <LoadingEdit />
          ) : (
            <div className="grid lg:grid-cols-5 grid-cols-1 gap-6 mt-6">
              {newsList.slice(0, 5).map((newsItem) => (
                <div className="flex flex-col items-center">
                  <Link
                    to={`/news/${encode(newsItem._id)}`}
                    className="<=md:h-auto h-52 w-full lg:w-auto rounded-lg overflow-hidden"
                  >
                    <img
                      className="h-full w-full rounded-lg transition-transform duration-300 transform hover:scale-110"
                      src={`data:image/png;base64,${newsItem?.mainImage}`}
                      alt={newsItem.title}
                    />
                  </Link>
                  <Link
                    to={`/news/${encode(newsItem._id)}`}
                    className="text-xl text-[#9d4edd] font-semibold my-2"
                  >
                    {newsItem.title.toUpperCase()}
                  </Link>
                </div>
              ))}
            </div>
          )
        }
      </div>
    </section>
  );
};

export default HomeNews;
