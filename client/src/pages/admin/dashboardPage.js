import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import doctorApi from "../../api/doctorApi";
import specialistApi from "../../api/specialistApi";
import appointmentApi from "../../api/appointmentApi";
import accountApi from "../../api/accountApi";
import recordApi from "../../api/recordApi";
import reportApi from "../../api/reportApi";
import servicesApi from "../../api/servicesApi";
import CountUp from "react-countup";
import { FaArrowUp, FaArrowDown } from 'react-icons/fa';

const DashboardPage = () => {
  const [isDeleted, setIsDeleted] = useState(false);
  const navigate = useNavigate();
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [docterList, setDocterList] = useState([]);
  const [specialists, setSpecialists] = useState([]);
  const [id, setId] = useState();
  const [appointments, setAppointments] = useState([]);
  const [account, setAccount] = useState([]);
  const [records, setRecords] = useState([]);
  const [reports, setReports] = useState([]);
  const [specialist, setSpecialist] = useState([]);
  const [doctor, setDoctor] = useState([]);
  const [serviceList, setServiceList] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [selectedMonth, setSelectedMonth] = useState(-1);
  const [filteredReports, setFilteredReports] = useState([]);
  const [totalRevenue, setTotalRevenue] = useState(0);
  const [previousMonthRevenue, setPreviousMonthRevenue] = useState(0);
  const [percentageChange, setPercentageChange] = useState(0.0);

  useEffect(() => {
    const filtered = reports.filter(report => {
      const reportDate = new Date(report.createdAt);
      return selectedMonth === -1 || reportDate.getMonth() + 1 === selectedMonth;
    });
    setFilteredReports(filtered);
    calculateTotalRevenue(filtered);
  }, [selectedMonth, reports]);

  useEffect(() => {
    if (filteredReports.length > 0) {
      calculatePreviousMonthRevenue();
    } else {
      setPreviousMonthRevenue(0);
      setPercentageChange(0);
    }
  }, [filteredReports]);

  const handleMonthChange = (e) => {
    setSelectedMonth(parseInt(e.target.value));
  };

  const calculateTotalRevenue = (filteredReports) => {
    const total = filteredReports.reduce((sum, report) => sum + report.amount, 0);
    setTotalRevenue(total);
  };

  const calculatePreviousMonthRevenue = () => {
    const previousMonth = selectedMonth === 1 ? 12 : selectedMonth - 1;
    const previousMonthYear = selectedMonth === 1 ? new Date().getFullYear() - 1 : new Date().getFullYear();
    const previousMonthReports = reports.filter(report => {
      const reportDate = new Date(report.createdAt);
      return reportDate.getMonth() + 1 === previousMonth && reportDate.getFullYear() === previousMonthYear;
    });
    const totalPrevious = previousMonthReports.reduce((sum, report) => sum + report.amount, 0);
    setPreviousMonthRevenue(totalPrevious);
    calculatePercentageChange(totalPrevious, totalRevenue);
  };

  const calculatePercentageChange = (previous, current) => {
    if (previous === 0) {
      setPercentageChange(current > 0 ? 100 : 0);
    } else {
      const change = ((current - previous) / previous) * 100;
      setPercentageChange(change);
    }
  };

  const formatDateTime = (dateTimeString) => {
    const date = new Date(dateTimeString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
    return `${day}-${month}-${year} ${hours}:${minutes}:${seconds}`;
  };

  useEffect(() => {
    setIsLoading(true);
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {

          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchService = async () => {
            try {
              const data = await specialistApi.getAllSpecialist();
              setSpecialists(data);
            } catch (error) {

            }
            finally {
              setIsLoading(false);
            }
          };
          fetchService();
        }
      }
    } catch (error) { }
  }, [isDeleted]);

  useEffect(() => {
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {
          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchService = async () => {
            const data = await servicesApi.getAllService();
            setServiceList(data);
          };
          fetchService();
        }
      }
    } catch (error) { }
  }, [isDeleted]);

  useEffect(() => {
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {
          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchAppointment = async () => {
            const data = await appointmentApi.getAllAppointment(
              axiosJWT,
              navigated,
              accessToken
            );
            setAppointments(data.data);
          };
          fetchAppointment();
          const fetchAccount = async () => {
            const data = await accountApi.getAllAccount(
              axiosJWT,
              navigated,
              accessToken
            );
            setAccount(data.data);
          };
          fetchAccount();
          const fetchRecord = async () => {
            const data = await recordApi.getAllRecord(
              axiosJWT,
              navigated,
              accessToken
            );
            setRecords(data.data);
          };
          fetchRecord();
          const fetchReport = async () => {
            const data = await reportApi.getAllReport(
              axiosJWT,
              navigated,
              accessToken
            );
            setReports(data.data);
          };
          fetchReport();
          const fetchSpec = async () => {
            const data = await specialistApi.getAllSpecialist();
            setSpecialist(data);
          };
          fetchSpec();
          const fetchDoctor = async () => {
            const data = await doctorApi.getAllDoctor(
              axiosJWT,
              navigated,
              accessToken
            );
            setDoctor(data.data);
          };
          fetchDoctor();
        }
      }
    } catch (error) { }
  }, []);

  useEffect(() => {
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {

          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchAppointment = async () => {
            const data = await appointmentApi.getAllAppointment(
              axiosJWT,
              navigated,
              accessToken
            );
            setAppointments(data.data);
          };
          fetchAppointment();
          const fetchAccount = async () => {
            const data = await accountApi.getAllAccount(
              axiosJWT,
              navigated,
              accessToken
            );
            setAccount(data.data);
          };
          fetchAccount();
          const fetchRecord = async () => {
            const data = await recordApi.getAllRecord(
              axiosJWT,
              navigated,
              accessToken
            );
            setRecords(data.data);
          }
          fetchRecord();
          const fetchSpec = async () => {
            const data = await specialistApi.getAllSpecialist();
            setSpecialist(data);
          }
          fetchSpec();
          const fetchDoctor = async () => {
            const data = await doctorApi.getAllDoctor(
              axiosJWT,
              navigated,
              accessToken
            );
            setDoctor(data.data);
          }
          fetchDoctor();
        }
      }
    } catch (error) { }
  }, []);

  useEffect(() => {
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {

          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchDocter = async () => {
            try {
              const data = await doctorApi.getAllDoctor(
                axiosJWT,
                navigated,
                accessToken
              );
              setDocterList(data.data);
            } catch (error) {

            }
          };
          fetchDocter();
          const fetchSpec = async () => {
            try {
              const data = await specialistApi.getAllSpecialist();
              setSpecialists(data);
            } catch (error) {

            }
          }
          fetchSpec();
        }
      }
    } catch (error) { }
  }, [isDeleted]);
  return (
    <main class="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
      <div className="p-6">
        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 mb-6">
          <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
            <div class="flex justify-between mb-6">
              <div>
                <div class="text-2xl font-semibold mb-1"><span className="text-2xl font-semibold"><CountUp end={docterList?.length} duration={3} className="text-2xl font-semibold" /></span></div>
                <div class="text-sm font-medium text-gray-400">Số Lượng Bác Sĩ</div>
              </div>
              {/* <div class="dropdown">8
                                <button type="button" class="dropdown-toggle text-gray-400 hover:text-gray-600">dsda<i class="ri-more-fill"></i></button>
                                <ul class="dropdown-menu shadow-md shadow-black/5 z-30 hidden py-1.5 rounded-md bg-white border border-gray-100 w-full max-w-[140px]">
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Profile</a>
                                    </li>
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Settings</a>
                                    </li>
                                    <li>
                                        <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Logout</a>
                                    </li>
                                </ul>
                            </div> */}
            </div>
            <Link
              to="/admin/doctor"
              className="text-pink-400 font-medium text-sm hover:text-pink-500"
            >
              Xem Chi Tiết
            </Link>
          </div>
          <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
            <div class="flex justify-between mb-6">
              <div>
                <div class="flex items-center mb-1">
                  <div class="text-2xl font-semibold">
                    <span className="text-2xl font-semibold">
                      <CountUp
                        end={serviceList?.length}
                        duration={3}
                        className="text-2xl font-semibold"
                      />
                    </span>
                  </div>
                  {/* <div class="p-1 rounded bg-emerald-500/10 text-emerald-500 text-[12px] font-semibold leading-none ml-2">+30%</div> */}
                </div>
                <div class="text-sm font-medium text-gray-400">
                  Số Lượng Dịch Vụ
                </div>
              </div>
              {/* <div class="dropdown">
                <button type="button" class="dropdown-toggle text-gray-400 hover:text-gray-600"><i class="ri-more-fill"></i></button>
                <ul class="dropdown-menu shadow-md shadow-black/5 z-30 hidden py-1.5 rounded-md bg-white border border-gray-100 w-full max-w-[140px]">
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Profile</a>
                  </li>
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Settings</a>
                  </li>
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Logout</a>
                  </li>
                </ul>
              </div> */}
            </div>
            <Link
              to="/admin/service"
              className="text-pink-400 font-medium text-sm hover:text-pink-500"
            >
              Xem Chi Tiết
            </Link>
          </div>
          <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
            <div class="flex justify-between mb-6">
              <div>
                <div class="text-2xl font-semibold mb-1">
                  <span class="text-base font-normal text-gray-400 align-top"></span>
                  <span className="text-2xl font-semibold">
                    <CountUp
                      end={appointments?.length}
                      duration={3}
                      className="text-2xl font-semibold"
                    />
                  </span>
                </div>
                <div class="text-sm font-medium text-gray-400">
                  Số Lịch Đặt Khám
                </div>
              </div>
              {/* <div class="dropdown">
                <button type="button" class="dropdown-toggle text-gray-400 hover:text-gray-600">dsd<i class="ri-more-fill"></i></button>
                <ul class="dropdown-menu shadow-md shadow-black/5 z-30 hidden py-1.5 rounded-md bg-white border border-gray-100 w-full max-w-[140px]">
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Profile</a>
                  </li>
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Settings</a>
                  </li>
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Logout</a>
                  </li>
                </ul>
              </div> */}
            </div>
            <Link
              to="/admin/appointment"
              className="text-pink-400 font-medium text-sm hover:text-pink-500"
            >
              Xem Chi Tiết
            </Link>
          </div>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-1 gap-6 mb-6">
          <div className="bg-white rounded-md border border-gray-100 p-5 shadow-md shadow-black/5">
            <div className="flex justify-between mb-6">
              <div>
                <div className="text-2xl font-semibold mb-1 flex items-center space-x-3">
                  <span className="text-base font-medium text-gray-500">Tổng Doanh Thu:</span>
                  <span className={`text-base text-[17px] font-semibold ${totalRevenue >= 0 ? 'text-blue-400' : 'text-pink-500'}`}><CountUp end={totalRevenue} duration={2} /></span>
                  <span className={`flex items-center space-x-1 text-base font-semibold text-[14.5px] ${percentageChange >= 0 ? 'text-green-500' : 'text-red-500'}`}>
                    {percentageChange === 0 ? "" : percentageChange >= 0 ? <FaArrowUp /> : <FaArrowDown />}
                    <span>{Math.abs(percentageChange).toFixed(2)}%</span>
                  </span>
                </div>
              </div>
              {/* <div class="dropdown">
                <button type="button" class="dropdown-toggle text-gray-400 hover:text-gray-600">dsd<i class="ri-more-fill"></i></button>
                <ul class="dropdown-menu shadow-md shadow-black/5 z-30 hidden py-1.5 rounded-md bg-white border border-gray-100 w-full max-w-[140px]">
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Profile</a>
                  </li>
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Settings</a>
                  </li>
                  <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-blue-500 hover:bg-gray-50">Logout</a>
                  </li>
                </ul>
              </div> */}
              <div className="text-right w-full max-w-xs dark:bg-zinc-900">
                <select
                  name="month"
                  className="min-w-80px px-5 py-1 border border-gray-300 rounded-md focus:outline-none focus:border-pink-400"
                  onChange={handleMonthChange}
                  value={selectedMonth}
                >
                  <option value="-1">Lọc theo tháng</option>
                  <option value="1">Tháng 1</option>
                  <option value="2">Tháng 2</option>
                  <option value="3">Tháng 3</option>
                  <option value="4">Tháng 4</option>
                  <option value="5">Tháng 5</option>
                  <option value="6">Tháng 6</option>
                  <option value="7">Tháng 7</option>
                  <option value="8">Tháng 8</option>
                  <option value="9">Tháng 9</option>
                  <option value="10">Tháng 10</option>
                  <option value="11">Tháng 11</option>
                  <option value="12">Tháng 12</option>
                </select>
              </div>
            </div>
            <div className="overflow-x-auto">
              <div className="overflow-y-auto max-h-[508px]">
                <table className={`w-full min-w-[800px] ${!reports.length ? 'flex flex-col justify-center items-center' : ''}`} data-tab-for="order" data-page="active">
                  <thead>
                    <tr className="text-left">
                      <th className="font-normal text-gray-400 py-2 pl-6">STT</th>
                      <th className="font-normal text-gray-400 py-2">Ngày</th>
                      <th className="font-normal text-gray-400 py-2">Doanh Thu</th>
                    </tr>
                  </thead>
                  <tbody>
                    {filteredReports.map((report, index) => (
                      <tr key={index} className="border-t border-gray-100">
                        <td className="pl-6 py-3">{index + 1}</td>
                        <td className="py-3 text-gray-500"><span className="inline-block p-1 rounded bg-gray-500/10 text-gray-500 font-medium text-[14.5px] leading-none">{formatDateTime(report.createdAt)}</span></td>
                        <td className={`py-3 ${report.amount >= 0 ? 'text-green-500' : 'text-red-500'}`}>
                          <span className={`inline-block p-1 rounded ${report.amount >= 0 ? 'bg-emerald-500/10 text-emerald-500' : 'bg-red-500/10 text-red-500'} font-medium text-[13.5px] leading-none`}>
                            {report.amount >= 0 ? '+' : '-'} {Math.abs(report.amount)} VND
                          </span>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default DashboardPage;
