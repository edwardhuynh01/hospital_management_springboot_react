package com.dev.server.Service;

import com.dev.server.model.Record;
import com.dev.server.repository.RecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class RecordService {
    private final RecordRepository recordRepository;

    public Record createRecord(Record record) {
        try {
            return recordRepository.save(record);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo hồ sơ: " + e.getMessage(), e);
        }
    }

    public List<Record> getAllRecords() {
        try {
            return recordRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy danh sách tất cả hồ sơ: " + e.getMessage(), e);
        }
    }

    public Optional<Record> getRecordById(String id) {
        try {
            return recordRepository.findById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy thông tin hồ sơ với id: " + id, e);
        }
    }

    public Record updateRecord(String id, Record recordDetails) {
        try {
            Optional<Record> optionalRecord = recordRepository.findById(id);

            if (optionalRecord.isPresent()) {
                Record existingRecord = optionalRecord.get();
                existingRecord.setFullName(recordDetails.getFullName());
                existingRecord.setEmail(recordDetails.getEmail());
                existingRecord.setDate(recordDetails.getDate());
                existingRecord.setSex(recordDetails.getSex());
                existingRecord.setCareer(recordDetails.getCareer());
                existingRecord.setIdentityCardNumber(recordDetails.getIdentityCardNumber());
                existingRecord.setAddress(recordDetails.getAddress());
                existingRecord.setProvince(recordDetails.getProvince());
                existingRecord.setDistrict(recordDetails.getDistrict());
                existingRecord.setWard(recordDetails.getWard());
                existingRecord.setPhoneNumber(recordDetails.getPhoneNumber());
                existingRecord.setPeoples(recordDetails.getPeoples());
                existingRecord.setUpdatedAt(recordDetails.getUpdatedAt());
                return recordRepository.save(existingRecord);
            } else {
                throw new RuntimeException("Không tìm thấy hồ sơ với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi cập nhật hồ sơ: " + e.getMessage(), e);
        }
    }

    public boolean deleteRecord(String id) {
        try {
            Optional<Record> optionalRecord = recordRepository.findById(id);

            if (optionalRecord.isPresent()) {
                recordRepository.deleteById(id);
                return true;
            } else {
                throw new RuntimeException("Không tìm thấy hồ sơ với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi xóa hồ sơ với id: " + id, e);
        }
    }
}
