package com.dev.server.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "news")
public class News {
    @Id
    private String _id;
    private String title;
    private String teaser;
    private String mainImage;
    private String content;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId author;
}
