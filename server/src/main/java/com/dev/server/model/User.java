package com.dev.server.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Date;

import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
public class User {
    @Id
    private String id;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId account;
    private String fullName;
    private String email;
    private Date date;
    private String sex;
    private String career;
    private String address;
    private String phoneNumber;
    private String Peoples;
    @CreatedDate
    private Date createdAt;
    @CreatedDate
    private Date updatedAt;
}
