package com.dev.server.Service;

import com.dev.server.DTO.PaymentDataDTO;
import com.dev.server.model.*;
import com.dev.server.model.Appointment;
import com.dev.server.model.Record;
import com.dev.server.repository.AppointmentRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class AppointmentService {

    private final AppointmentRepository appointmentRepository;
    @Autowired
    private JavaMailSender mailSender;
    @Value("${jwt.secret}")
    private String jwtSecret;

    // Tạo mới một cuộc hẹn
    public Appointment createAppointment(Appointment appointment) {
        try {
            return appointmentRepository.save(appointment);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo cuộc hẹn: " + e.getMessage(), e);
        }
    }

    // Cập nhật thông tin cuộc hẹn
    public Appointment updateAppointment(String id, Appointment appointment) {
        try {
            Optional<Appointment> optionalAppointment = appointmentRepository.findById(id);
            if (optionalAppointment.isPresent()) {
                Appointment existingAppointment = optionalAppointment.get();
                existingAppointment.setId(appointment.getId());
                existingAppointment.setRecordId(appointment.getRecordId());
                existingAppointment.setDayOfWeek(appointment.getDayOfWeek());
                existingAppointment.setDay(appointment.getDay());
                existingAppointment.setMonth(appointment.getMonth());
                existingAppointment.setYear(appointment.getYear());
                existingAppointment.setTimeStart(appointment.getTimeStart());
                existingAppointment.setTimeEnd(appointment.getTimeEnd());
                existingAppointment.setSpecialistId(appointment.getSpecialistId());
                existingAppointment.setServices(appointment.getServices());
                existingAppointment.setDoctorId(appointment.getDoctorId());
                return appointmentRepository.save(existingAppointment);
            } else {
                throw new RuntimeException("Appointment not found with id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi cập nhật cuộc hẹn: " + e.getMessage(), e);
        }
    }

    // Lấy thông tin cuộc hẹn bằng ID
    public Appointment getAppointmentById(String id) {
        try {
            Optional<Appointment> optionalAppointment = appointmentRepository.findById(id);
            return optionalAppointment.orElse(null);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy thông tin cuộc hẹn với id: " + id, e);
        }
    }

    // Lấy danh sách tất cả cuộc hẹn
    public List<Appointment> getAllAppointments() {
        try {
            return appointmentRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy danh sách tất cả cuộc hẹn: " + e.getMessage(), e);
        }
    }

    // Xóa cuộc hẹn bằng ID
    public void deleteAppointment(String id) {
        try {
            appointmentRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi xóa cuộc hẹn với id: " + id, e);
        }
    }

    public void sendReportMail(Record record, String reportLink) throws MessagingException {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("nhat.tlm3173@gmail.com");
            helper.setTo(record.getEmail());
            helper.setSubject("[Bệnh viện đa khoa sinh tố dâu] Thông tin phiếu khám");
            helper.setText(
                    "<h2>Chào " + record.getFullName() + ",</h2>" +
                            "<br/>" +
                            "<p>Bạn vừa mới đặt khám. Nhấn vào link bên dưới để xem thông tin phiếu khám.</p>" +
                            "<br/>" +
                            "<a href=\"" + reportLink + "\">Thông tin phiếu khám</a>" +
                            "<br/>" +
                            "<p>Cảm ơn bạn đã tin tưởng và sử dụng dịch vụ của chúng tôi. Vui lòng đến đúng giờ hẹn để được khám và điều trị tốt nhất.</p>" +
                            "<br/>" +
                            "<p>Trân trọng,</p>" +
                            "<p>Sinh tố dâu team</p>",
                    true
            );
            mailSender.send(message);
        } catch (MessagingException e) {
            throw new MessagingException("Lỗi khi gửi email báo cáo: " + e.getMessage(), e);
        }
    }

    public Claims verifyDataPaymentToken(String token) {
        try {
            byte[] secretBytes = Base64.getEncoder().encode(jwtSecret.getBytes());
            SecretKeySpec secretKey = new SecretKeySpec(secretBytes, "HmacSHA512");
            return Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token).getPayload();
        } catch (JwtException e) {
            return null;
        }
    }

    public String generateDataPaymentToken(PaymentDataDTO paymentDataDTO) {
        try {
            byte[] secretBytes = Base64.getEncoder().encode(jwtSecret.getBytes());
            SecretKeySpec secretKey = new SecretKeySpec(secretBytes, "HmacSHA512");
            return Jwts.builder()
                    .claim("listAppointment", paymentDataDTO.getListAppointment())
                    .claim("report", paymentDataDTO.getReport())
                    .claim("paymentLinkId", paymentDataDTO.getPaymentLinkId())
                    .setIssuedAt(new Date())
                    .setExpiration(new Date(System.currentTimeMillis() + (30 * 60 * 1000)))
                    .signWith(secretKey)
                    .compact();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo token thanh toán: " + e.getMessage(), e);
        }
    }
}
