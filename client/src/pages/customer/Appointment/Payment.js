import React, { useEffect, useState } from "react";
import styled, { keyframes } from "styled-components";
import {
  IoArrowBackCircle,
  IoArrowForwardOutline,
  IoNavigateCircleOutline,
  IoPersonCircleOutline,
  IoPhonePortraitOutline,
} from "react-icons/io5";
import InformationPaymentList from "../../../components/Appointment/InformationPaymentList";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
import recordApi from "../../../api/recordApi";
import { PaymentReport, makeReportNonPayment } from "../../../redux/apiRequest";
import { addToken } from "../../../redux/appointmentSlice";
import specialistApi from "../../../api/specialistApi";
import { toast } from "react-toastify";
import LoadingEdit from "../../../components/Loading/LoadingEdit";
const bounce = keyframes`
  /* 0% { transform: translateX(0); opacity: 0; }
  25% { opacity: 1; }
  50% { transform: translateX(10px); }
  75% { opacity: 1; }
  100% { transform: translateX(0); opacity: 0; } */
  0% {
    opacity: 0;
    transform: translateX(-100%);
  }
  50% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    transform: translateX(100%);
  }
`;
const AnimatedArrow = styled(IoArrowForwardOutline)`
  animation: ${bounce} 2s linear infinite;
`;
const Confirm = styled.button`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: #fb6f92;
  }
`;
const Back = styled.a`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  color: #4a4e69;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: rgba(144, 224, 239, 0.2);
    color: #48cae4;
  }
`;
const Bottom = styled.div`
  border-top: 3px solid #dee2e6;
`;
const Side = styled.div`
  border-bottom: 2px solid #ffb3c6;
  border-left: 2px solid #ffb3c6;
  border-right: 2px solid #ffb3c6;
`;
const Container = styled.div`
  border-bottom: 2px solid #ffb3c6;
  border-left: 2px solid #ffb3c6;
  border-right: 2px solid #ffb3c6;
`;

function Payment(props) {
  const [activeIndex, setActiveIndex] = useState(0);
  const [record, setRecord] = useState();
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [totalPrice, setTotalPrice] = useState(0);
  const appointmentsList = useSelector(
    (state) => state.appointment?.appointmentsList
  );
  const handleRecordClick = (index) => {
    setActiveIndex(index); // Mở `Record` tại vị trí index
  };
  const recordId = useSelector(
    (state) => state.appointment?.appointment?.recordId
  );
  const encode = (id) => {
    return btoa(id);
  };
  useEffect(() => {
    if (!user) {
      navigated("/login");
    }
  });
  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchRecord = async () => {
        const data = await recordApi.getRecordById(
          axiosJWT,
          navigated,
          accessToken,
          encode(recordId)
        );
        setRecord(data.data);
        const specialistPromises = appointmentsList.map(async (item) => {
          const response = await specialistApi.getSpecialistById(
            encode(item?.specialistId)
          );
          return response;
        });
        const specialists = await Promise.all(specialistPromises);
        const newTotalPrice = specialists.reduce((acc, specialist) => {
          return acc + Number(specialist.price);
        }, 0);

        setTotalPrice(newTotalPrice);
      };
      fetchRecord();
    } catch (error) {
      alert(error);
    }
  }, []);
  const dataPaymentsList = useSelector(
    (state) => state.appointment?.appointmentsList
  );
  const dataRecordId = useSelector(
    (state) => state.appointment?.appointment?.recordId
  );
  const data = {
    userId: user._id,
    recordId: dataRecordId,
    listAppointments: dataPaymentsList,
    amount: totalPrice + 10000,
    payment: true,
  };
  const dataNonPayment = {
    userId: user._id,
    recordId: dataRecordId,
    listAppointments: dataPaymentsList,
    amount: totalPrice,
    payment: false,
  };
  const [isLoading, setIsLoading] = useState(false);
  const handlePayment = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      if (activeIndex === 0) {
        const checkoutdata = await makeReportNonPayment(
          dataNonPayment,
          DISPATCH
        );
        toast(checkoutdata.data);
        if (checkoutdata.data !== "Lịch khám này đã được đặt") {
          navigated("/profile?component=medicalReports");
        }
      }
      if (activeIndex === 1) {
        const checkoutdata = await PaymentReport(data);
        if (checkoutdata) {
          DISPATCH(addToken(checkoutdata.paymentToken));
          console.log(data);
          console.log(checkoutdata);
          if (checkoutdata.data) {
            toast(checkoutdata.data);
          } else {
            window.location.href = checkoutdata?.checkoutUrl; // Chuyển hướng đến URL thanh toán
          }
        } else {
          console.error("Failed to get checkout URL");
        }
      }
    } catch (error) {
    } finally {
      // navigated("/profile?component=medicalReports");
      setIsLoading(false);
    }
  };

  return (
    <>
      {isLoading ? (
        <LoadingEdit />
      ) : (
        <div className="w-full flex justify-center py-5 bg-white">
          <div className="w-full md:w-4/5 lg:flex justify-between ">
            <div className="w-full h-56 lg:w-1/5 flex flex-wrap lg:mr-4 mb-4">
              <div
                className="w-full bg-[#f26a8d] text-white px-5 py-3 rounded-t-md
            font-semibold"
              >
                <h2>Thông tin bệnh nhân</h2>
              </div>
              <Side className="w-full pr-5 pl-4 pt-3 pb-7">
                <div className="flex justify-start items-start mb-4 mt-2">
                  <div className="mx-2 ">
                    <IoPersonCircleOutline></IoPersonCircleOutline>
                  </div>
                  <p className="font-semibold text-sm">{record?.fullName}</p>
                </div>
                <div className="flex justify-start items-start my-4">
                  <div className="mx-2">
                    <IoPhonePortraitOutline></IoPhonePortraitOutline>
                  </div>
                  <p className="font-normal text-sm">{record?.phoneNumber}</p>
                </div>
                <div className="flex justify-start items-start my-4">
                  <div className="mx-2 mt-1">
                    <IoNavigateCircleOutline></IoNavigateCircleOutline>
                  </div>
                  <p className="font-normal text-sm">{record?.address}</p>
                </div>
              </Side>
            </div>
            <div className="w-full lg:w-4/5 lg:ml-4">
              <div className="w-full bg-[#f26a8d] text-white px-5 py-2 rounded-t-md font-semibold">
                <h2>Chọn phương thức thanh toán</h2>
              </div>
              <Container>
                <div className="flex w-full">
                  <div className="w-1/2 flex flex-wrap py-5">
                    <form action="">
                      <div className="w-full px-5 text-[#495057]">
                        <input
                          id="direct"
                          name="payment"
                          type="radio"
                          onClick={() => handleRecordClick(0)}
                          checked={activeIndex === 0}
                        />
                        <label
                          className={`${
                            activeIndex === 0 ? "text-[#ff8fab]" : ""
                          }`}
                          htmlFor="direct"
                        >
                          Thanh toán trực tiếp
                        </label>
                        <div
                          className={`transform transition-all duration-700 ${
                            activeIndex === 0
                              ? "scale-100 opacity-100"
                              : "scale-75 opacity-0"
                          } origin-top w-1/2 h-auto`}
                        >
                          <img
                            className={` ${
                              activeIndex === 0 ? "flex" : "hidden"
                            } border border-dashed border-[#ff8fab] bg-[#ffe5ec] md:w-8/12 h-auto py-2 px-7 rounded-sm`}
                            src="/images/directPayment.jpg"
                            alt="direct"
                          />
                        </div>
                      </div>
                      <br />
                      <div className="w-full px-5 text-[#495057]">
                        <input
                          id="online"
                          name="payment"
                          type="radio"
                          onClick={() => handleRecordClick(1)}
                          checked={activeIndex === 1}
                        />
                        <label
                          className={`${
                            activeIndex === 1 ? "text-[#ff8fab]" : ""
                          }`}
                          htmlFor="online"
                        >
                          Thanh toán online
                        </label>
                        <div
                          className={`transform transition-all duration-700 ${
                            activeIndex === 1
                              ? "scale-100 opacity-100"
                              : "scale-75 opacity-0"
                          } origin-top w-1/2 h-auto`}
                        >
                          <img
                            className={`${
                              activeIndex === 1 ? "flex" : "hidden"
                            } border border-dashed border-[#ff8fab] bg-[#ffe5ec] md:w-8/12 h-auto py-2 px-7 md:px-12 rounded-sm`}
                            src="/images/vnpay-removebg-preview.png"
                            alt="online"
                          />
                        </div>
                      </div>
                    </form>
                  </div>
                  <InformationPaymentList
                    utilityFee={activeIndex === 0 ? 0 : 10000}
                  />
                </div>
                <Bottom className="flex justify-between w-full py-5">
                  <Back
                    className="w-32 h-11 flex items-center justify-center mx-2 pr-2"
                    //   onClick={HandleRewriteClick}
                    href="/ChooseMedicalSchedule"
                  >
                    <div className="flex justify-center items-center">
                      <div className="mx-2 ">
                        <IoArrowBackCircle></IoArrowBackCircle>
                      </div>
                      Quay lại
                    </div>
                  </Back>
                  <div className="flex 1/2 lg:w-1/3 justify-end">
                    <form
                      // action="http://localhost:5000/api/appointment/create-payment-link"
                      // method="POST"
                      onSubmit={handlePayment}
                    >
                      <Confirm
                        className="w-32 h-11 flex items-center justify-center mx-2 bg-[#ff8fa3] text-white pl-2"
                        //   onClick={handleSubmitClick}
                        type="submit"
                      >
                        <div className="flex justify-center items-center">
                          Xác nhận
                          <div className="mx-2 ">
                            <AnimatedArrow size={16} />
                          </div>
                        </div>
                      </Confirm>
                    </form>
                  </div>
                </Bottom>
              </Container>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default Payment;
