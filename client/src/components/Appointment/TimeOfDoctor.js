import React from "react";
import { useDispatch } from "react-redux";
import { chooseTime } from "../../redux/appointmentSlice";

const TimeOfDoctor = (props) => {
  const DISPATCH = useDispatch();
  const { timeStart, timeEnd, isChoose } = props;
  const saveTime = async (time) => {
    DISPATCH(chooseTime(time));
  };
  console.log(isChoose);
  return (
    <div className="my-4 mx-2 ">
      <a
        className={` w-full h-full flex justify-center items-center  py-2 px-4 
            rounded-lg  transition duration-100 ease-in-out delay-100
            ${
              isChoose
                ? "bg-slate-400 text-white pointer-events-none"
                : "cursor-pointer border border-solid border-[#ff8fab] text-[#ff8fab] hover:bg-[#ff8fab] hover:text-white"
            }`}
        href="/ConfirmInformation"
        onClick={() => {
          const chooseTime = {
            timeStart: timeStart,
            timeEnd: timeEnd,
          };
          saveTime(chooseTime);
        }}
      >
        <p>{`${timeStart}-${timeEnd}`}</p>
      </a>
    </div>
  );
};

export default TimeOfDoctor;
