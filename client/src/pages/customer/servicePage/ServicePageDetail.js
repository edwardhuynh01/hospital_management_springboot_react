import React, { useEffect, useState } from "react";
import Service from "../../../api/servicesApi";
import { useParams } from "react-router-dom";
import Title from "../../../components/Title/Title";
const ServicePageDetail = () => {
  const [service, setService] = useState();
  const { id } = useParams();
  useEffect(() => {
    const fetchService = async () => {
      try {
        const response = await Service.getServiceById(id);
        setService(response);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
    };
    fetchService();
  }, [id]);
  console.log(service);
  return (
    <div className="bg-white">
      <Title>{service?.name.toUpperCase()}</Title>
      <div className="container mx-auto py-6 mt-4">
        <div className="md:mx-20 ">
          <img
            className="w-full h-[500px] <=md:h-auto"
            src={`data:image/png;base64,${service?.mainImage}`}
            alt={service?.name}
          />
          <p className="font-light my-4">{service?.teaser}</p>
          <div
            className="py-4"
            dangerouslySetInnerHTML={{ __html: service?.description }}
          />
        </div>
      </div>
    </div>
  );
};

export default ServicePageDetail;
