import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import {
  IoPersonCircleOutline,
  IoPhonePortraitOutline,
  IoNavigateCircleOutline,
  IoTrash,
  IoCreateOutline,
} from "react-icons/io5";
import recordApi from "../../../api/recordApi";
const Detail = styled.div`
  font-size: 12px;
  display: flex;
  align-items: center;
  transition: ease-in-out all 0.2s;
  width: 8em;
  justify-content: center;
  &:hover {
    background-color: #ffe5ec;
  }
`;
const Delete = styled.a`
  font-size: 12px;
  transition: ease-in-out all 0.2s;
  color: #fb6f92;
  border-radius: 3px;
  &:hover {
    background-color: #fb6f92;
    /* border-color: rgba(255, 104, 159, 0.75); */
    color: white;
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 104, 159);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
const Update = styled.a`
  font-size: 12px;
  transition: ease-in-out all 0.2s;
  color: #00a6fb;
  border-radius: 3px;
  &:hover {
    background-color: #00a6fb;
    color: white;
    box-shadow: 0 0.5em 0.5em -0.4em rgb(181, 226, 250);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
function MedicalRecords(props) {
  const {
    id,
    fullName,
    email,
    date,
    sex,
    career,
    IdentityCardNumber,
    address,
    Peoples,
    phoneNumber,
  } = props;

  const handleClick = () => {
    props.onClick(id);
  };
  const encode = (id) => {
    return btoa(id);
  };
  return (
    <div className="flex flex-wrap justify-center w-full shadow-md border border-solid my-3">
      <div className="w-11/12 flex justify-between my-3">
        <div className="flex justify-start items-center text-[#fb6f92] font-medium">
          <div className="mx-2 ">
            <IoPersonCircleOutline></IoPersonCircleOutline>
          </div>
          <h3>{fullName}</h3>
        </div>
        <Detail className="text-[#ffafcc] rounded-sm cursor-pointer">
          <Link to={`/Profile/PATIENT-RECORD/${encode(id)}`}>Xem thêm</Link>
        </Detail>
      </div>
      <div className="flex flex-wrap w-11/12">
        <div className="flex w-full justify-start items-center">
          <div className="w-2/4 lg:w-1/4">
            <div className="flex justify-start items-center">
              <div className="mx-2 ">
                <IoPhonePortraitOutline></IoPhonePortraitOutline>
              </div>
              <h3 className="font-normal">Điện thoại</h3>
            </div>
          </div>
          <div className="font-bold text-sm w-2/4 lg:w-3/4">{phoneNumber}</div>
        </div>

        <div className="flex w-full justify-start items-center">
          <div className="w-2/4 lg:w-1/4">
            <div className="flex justify-start items-center">
              <div className="mx-2 ">
                <IoNavigateCircleOutline></IoNavigateCircleOutline>
              </div>
              <h3 className="font-normal">Địa chỉ</h3>
            </div>
          </div>
          <div className="w-2/4 lg:w-3/4 text-sm">{address}</div>
        </div>
      </div>
      <div className="border-t border-solid w-11/12 my-4"></div>
      <div className="w-full flex justify-end my-3 pr-5">
        <Delete
          className="w-28 h-6 flex items-center justify-center mx-1"
          onClick={handleClick}
        >
          <div className="flex justify-center items-center">
            <div className="mx-2 ">
              <IoTrash></IoTrash>
            </div>
            Xóa hồ sơ
          </div>
        </Delete>
        <Update className="w-28 h-6 flex items-center justify-center mx-1">
          <Link
            to={`/Profile/UpdateMedicalRC/${encode(id)}`}
            className="flex justify-center items-center"
          >
            <div className="mx-2 ">
              <IoCreateOutline></IoCreateOutline>
            </div>
            Sửa hồ sơ
          </Link>
        </Update>
      </div>
    </div>
  );
}

export default MedicalRecords;
