import React from "react";
import { confirmAlert } from "react-confirm-alert";
import {
  IoCalendarClearOutline,
  IoMaleFemaleOutline,
  IoNavigateCircleOutline,
  IoPeopleOutline,
  IoPersonCircleOutline,
  IoPhonePortraitOutline,
  IoArrowForwardOutline,
  IoTrash,
  IoCreateOutline,
} from "react-icons/io5";
import { Link, useNavigate } from "react-router-dom";
import styled, { keyframes } from "styled-components";
import recordApi from "../../api/recordApi";
import { useDispatch, useSelector } from "react-redux";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import { toast } from "react-toastify";
const Container = styled.div`
  transition: all ease-in 0.2s;
  border: 2px solid #ffe5ec;
  &:hover {
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 229, 236);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
const bounce = keyframes`
  /* 0% { transform: translateX(0); opacity: 0; }
  25% { opacity: 1; }
  50% { transform: translateX(10px); }
  75% { opacity: 1; }
  100% { transform: translateX(0); opacity: 0; } */
  0% {
    opacity: 0;
    transform: translateX(-100%);
  }
  50% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    transform: translateX(100%);
  }
`;
const AnimatedArrow = styled(IoArrowForwardOutline)`
  animation: ${bounce} 2s linear infinite;
`;
const Continue = styled.a`
  transition: ease-in-out all 0.2s;
  font-weight: 500;
  font-size: 0.95rem;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: #fb6f92;
  }
`;
const Delete = styled.a`
  transition: ease-in-out all 0.2s;
  color: #fb6f92;
  border-radius: 3px;
  cursor: pointer;
  background-color: rgba(251, 111, 146, 0.2);
  &:hover {
    background-color: #f26a8d;
    color: white;
  }
`;
const Update = styled.a`
  transition: ease-in-out all 0.2s;
  color: #48cae4;
  border-radius: 3px;
  cursor: pointer;
  background-color: rgba(144, 224, 239, 0.2);
  &:hover {
    background-color: #00a6fb;
    color: white;
  }
`;
function Record(props) {
  const {
    id,
    fullName,
    email,
    date,
    sex,
    career,
    IdentityCardNumber,
    address,
    Peoples,
    phoneNumber,
  } = props;
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const encode = (id) => {
    return btoa(id);
  };
  let navigate = useNavigate();
  const handleOnClick = () => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className="bg-slate-100 p-6 rounded-lg shadow-xl">
          <h1 className="text-2xl text-[#fb6f92] font-semibold mb-4 text-start h-full w-full">
            Xác nhận xóa
          </h1>
          <p className="mb-4">Bạn có chắc chắn muốn xóa hồ sơ này?</p>
          <div className="flex justify-center gap-4">
            <button
              onClick={async () => {
                try {
                  const deleteData = await recordApi.deleteRecord(
                    axiosJWT,
                    navigated,
                    accessToken,
                    id
                  );
                  if (deleteData) {
                    props.delete();
                    toast("Xóa hồ sơ thành công!");
                  } else {
                    toast.error("Đã xảy ra lỗi khi xóa hồ sơ.");
                  }
                } catch (error) {
                  toast.error("Đã xảy ra lỗi khi xóa hồ sơ.");
                  console.error("Error deleting record:", error);
                }
                onClose();
              }}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-red-600 duration-300 bg-red-400 text-white px-8 py-2 rounded-lg mr-2"
            >
              Có
            </button>
            <button
              onClick={onClose}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-gray-400 duration-300 bg-gray-300 text-black px-4 py-2 rounded-lg"
            >
              Không
            </button>
          </div>
        </div>
      ),
    });
  };
  return (
    <Container
      className="w-full h-auto shadow-sm rounded-md flex flex-wrap justify-center my-4"
      onClick={props.onClick}
    >
      <div className="w-11/12 flex justify-between my-3 flex-wrap">
        <div className="flex justify-start items-center text-[#fb6f92] font-medium w-full">
          <div className="mx-2 ">
            <IoPersonCircleOutline></IoPersonCircleOutline>
          </div>
          <h3>{fullName}</h3>
        </div>
        <div className="flex flex-wrap w-full mt-2">
          <div className="flex w-full justify-start items-start my-1">
            <div className="w-1/2">
              <div className="flex justify-start items-center text-[#adb5bd]">
                <div className="mx-2 ">
                  <IoCalendarClearOutline></IoCalendarClearOutline>
                </div>
                <h3 className="font-normal ">Ngày sinh</h3>
              </div>
            </div>
            <div className="w-1/2">{date}</div>
          </div>
          <div className="flex w-full justify-start items-start my-1">
            <div className="w-1/2">
              <div className="flex justify-start items-center text-[#adb5bd]">
                <div className="mx-2 ">
                  <IoPhonePortraitOutline></IoPhonePortraitOutline>
                </div>
                <h3 className="font-normal ">Điện thoại</h3>
              </div>
            </div>
            <div className="w-1/2">{phoneNumber}</div>
          </div>
          <div
            className={`${props.isActive ? "flex" : "hidden"
              } w-full justify-start items-start my-1`}
          >
            <div className="w-1/2">
              <div className="flex justify-start items-center text-[#adb5bd]">
                <div className="mx-2 ">
                  <IoMaleFemaleOutline></IoMaleFemaleOutline>
                </div>
                <h3 className="font-normal ">Giới tính</h3>
              </div>
            </div>
            <div className="w-1/2">{sex}</div>
          </div>
          <div
            className={`${props.isActive ? "flex" : "hidden"
              } w-full justify-start items-start my-1`}
          >
            <div className="w-1/2">
              <div className="flex justify-start items-center text-[#adb5bd]">
                <div className="mx-2 ">
                  <IoPeopleOutline></IoPeopleOutline>
                </div>
                <h3 className="font-normal ">Dân tộc</h3>
              </div>
            </div>
            <div className="w-1/2">{Peoples}</div>
          </div>
          <div
            className={`${props.isActive ? "flex" : "hidden"
              } w-full justify-start items-start my-1`}
          >
            <div className="w-1/2">
              <div className="flex justify-start items-center text-[#adb5bd]">
                <div className="mx-2 ">
                  <IoNavigateCircleOutline></IoNavigateCircleOutline>
                </div>
                <h3 className="font-normal ">Địa chỉ</h3>
              </div>
            </div>
            <div className="w-1/2">{address}</div>
          </div>
          <div
            className={`${props.isActive ? "flex" : "hidden"
              } w-full justify-between items-center mt-4 mb-2 pt-7 border-t border-solid `}
          >
            <div className="w-2/3 flex justify-between">
              <Delete
                className="w-2/5 h-8 flex items-center justify-center mx-2 pr-2 "
                //   onClick={HandleRewriteClick}
                onClick={handleOnClick}
              >
                <div className="flex justify-center items-center text-sm font-semibold">
                  <div className="mx-2 ">
                    <IoTrash></IoTrash>
                  </div>
                  Xóa
                </div>
              </Delete>
              <Link
                to={`/Profile/UpdateMedicalRC/${encode(id)}`}
                className="w-2/5 h-8 mx-2"
              >
                <Update
                  className="w-full h-full flex items-center justify-center  pr-2"
                  //   onClick={HandleRewriteClick}
                  href="/Profile/UpdateMedicalRC"
                >
                  <div className="flex justify-center items-center text-sm font-semibold">
                    <div className="mx-2 ">
                      <IoCreateOutline></IoCreateOutline>
                    </div>
                    Sửa
                  </div>
                </Update>
              </Link>
            </div>
            <Continue
              className="w-36 h-11 flex items-center justify-center mx-2 bg-[#ff8fa3] text-white pl-2"
              //   onClick={handleSubmitClick}
              href="/ChooseSpecialist"
              onClick={props.chooseRecord}
            >
              <div className="flex justify-center items-center">
                Tiếp tục
                <div className="mx-2 ">
                  <AnimatedArrow size={16} />
                </div>
              </div>
            </Continue>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default Record;
