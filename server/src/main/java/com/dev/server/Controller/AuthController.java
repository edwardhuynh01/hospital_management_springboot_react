package com.dev.server.Controller;

import com.dev.server.DTO.*;
import com.dev.server.Service.AccountService;
import com.dev.server.model.Account;
import com.dev.server.repository.AccountRepository;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/auth")
//@CrossOrigin(maxAge = 3600,, allowedHeaders = "Requestor-Type", exposedHeaders = "X-Get-Header")
public class AuthController {
    @Autowired
    AccountService accountService;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private HttpServletResponse response;
    Map<String, String> RFtokens = new HashMap<>();
    private List<String> refreshTokens = new ArrayList<>();

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody UserDTO userDto) {
        try {
            accountService.registerUser(userDto);
            return ResponseEntity.ok("Gửi mail thành công");
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Gửi mail không thành công!");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/confirmEmail")
    public ResponseEntity<?> confirmEmail(@RequestParam String token, @RequestParam String email) {
        try {
            if (token == null || token.isEmpty()) {
                return ResponseEntity.badRequest().body("Yêu cầu không hợp lệ hoặc hết hạn");
            }

            Claims claims = accountService.verifyToken(token);
            if (claims == null) {
                return ResponseEntity.badRequest().body("Yêu cầu không hợp lệ hoặc hết hạn");
            }

            Optional<Account> account = accountRepository.findByEmail(claims.getSubject());
            if (account == null) {
                return ResponseEntity.badRequest().body("Email không tồn tại!");
            }

            account.get().setEmailConfirmed(true);
            accountRepository.save(account.get());
            return ResponseEntity.ok("Xác nhận tài khoản thành công");
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PostMapping("/loginGG")
    public ResponseEntity<?> loginGG(@RequestBody GoogleUserDTO googleUserDTO, HttpServletResponse response) {
        String email = googleUserDTO.getEmail();
        String name = googleUserDTO.getName();
        boolean email_confirm = googleUserDTO.isEmail_verified();
        Account account = accountService.loginGG(email, name, email_confirm);
        if (account != null) {
            final String accessToken = accountService.generateAccessToken(account);
            final String refreshToken = accountService.generateRefreshToken(account);
            // Logic for storing refresh token if needed
            Cookie cookie = new Cookie("refreshToken", refreshToken);
            cookie.setHttpOnly(true);
            cookie.setSecure(false); // Set to true if using HTTPS
            cookie.setPath("/");
            cookie.setMaxAge(365 * 24 * 60 * 60); // One year in seconds
            response.addCookie(cookie);
            refreshTokens.add(refreshToken);
            LoginResponseDTO loginResponse = new LoginResponseDTO();
            loginResponse.set_id(account.get_id());
            loginResponse.setUsername(account.getUsername());
            loginResponse.setEmail(account.getEmail());
            loginResponse.setPhoneNumber(account.getPhoneNumber());
            loginResponse.setRole(account.getRole());
            loginResponse.setEmailConfirmed(account.getEmailConfirmed());
            loginResponse.setAccessToken(accessToken);
            return ResponseEntity.ok(loginResponse);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Sai tên đăng nhập hoặc mật khẩu");
        }
    }
    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody LoginDTO loginDTO, HttpServletResponse response) {
        try {
            Account account = accountService.loginUser(loginDTO.getEmail(), loginDTO.getPassword());
            if (account != null) {
                final String accessToken = accountService.generateAccessToken(account);
                final String refreshToken = accountService.generateRefreshToken(account);
                // Logic for storing refresh token if needed
                Cookie cookie = new Cookie("refreshToken", refreshToken);
                cookie.setHttpOnly(true);
                cookie.setSecure(false); // Set to true if using HTTPS
                cookie.setPath("/");
                cookie.setMaxAge(365 * 24 * 60 * 60); // One year in seconds
                response.addCookie(cookie);
                refreshTokens.add(refreshToken);
                LoginResponseDTO loginResponse = new LoginResponseDTO();
                loginResponse.set_id(account.get_id());
                loginResponse.setUsername(account.getUsername());
                loginResponse.setEmail(account.getEmail());
                loginResponse.setPhoneNumber(account.getPhoneNumber());
                loginResponse.setRole(account.getRole());
                loginResponse.setEmailConfirmed(account.getEmailConfirmed());
                loginResponse.setAccessToken(accessToken);
                return ResponseEntity.ok(loginResponse);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Sai tên đăng nhập hoặc mật khẩu");
            }
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Error: " + e.getMessage());
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PostMapping("/refresh")
    public ResponseEntity<?> refreshToken() {
        // Lấy refresh token từ người dùng
        Cookie[] cookies = request.getCookies();
        String refreshToken = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("refreshToken".equals(cookie.getName())) {
                    refreshToken = cookie.getValue();
                    break;
                }
            }
        }

        if (refreshToken == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Bạn chưa được xác thực");
        }

        try {
            // Xác thực và kiểm tra refresh token
            Claims claims = accountService.verifyToken(refreshToken);
            String email = claims.getSubject();
            Optional<Account> account = accountRepository.findByEmail(email);
            // Tạo token mới
            String newAccessToken = accountService.generateAccessToken(account.get());
            String newRefreshToken = accountService.generateRefreshToken(account.get());

            // Thiết lập refresh token mới vào cookie
            Cookie newCookie = new Cookie("refreshToken", newRefreshToken);
            newCookie.setHttpOnly(true);
            newCookie.setSecure(false);
            newCookie.setPath("/");
            newCookie.setMaxAge(365 * 24 * 60 * 60); // 1 năm
            response.addCookie(newCookie);
            refreshTokens.add(newRefreshToken);
            RFtokens.put("accessToken", newAccessToken);
            return ResponseEntity.ok(RFtokens);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
        }
    }
    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PostMapping("/logout")
    public ResponseEntity<?> userLogout() {
        // Xóa refresh token từ cookie
        String token = request.getHeader("token");
        if (token == null || token.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
        } else {
            String accessToken = token.split(" ")[1];
            Claims claims = accountService.verifyToken(accessToken);
            if (claims == null) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
            } else {
                request.setAttribute("user", claims);
                Cookie[] cookies = request.getCookies();
                if (cookies != null) {
                    for (Cookie cookie : cookies) {
                        if ("refreshToken".equals(cookie.getName())) {
                            refreshTokens = refreshTokens.stream()
                                    .filter(Token -> !Token.equals(cookie.getValue()))
                                    .collect(Collectors.toList());
                            cookie.setValue(null);
                            cookie.setMaxAge(0);
                            cookie.setPath("/");
                            response.addCookie(cookie);
                            break;
                        }
                    }
                }
                return ResponseEntity.ok("Đăng xuất thành công!");
            }
        }
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/forgot")
    public ResponseEntity<?> forgotPassword(@RequestBody ForgetPasswordDTO email) {
        if (email.getEmail() == null || email.getEmail().isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Vui lòng nhập email!");
        }
        try {
            accountService.forgotPassword(email.getEmail(), request);
            return ResponseEntity.status(HttpStatus.OK).body("Gửi mail thành công!");
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email không tồn tại!");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage()+" Gửi mail không thành công!");
        }
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/reset")
    public ResponseEntity<?> resetPasswordShow(@RequestParam String token, @RequestParam String email) {
        if (token == null || token.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Yêu cầu không hợp lệ hoặc hết hạn");
        }
        Claims claims = accountService.verifyToken(token);
        if (claims == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Yêu cầu không hợp lệ hoặc hết hạn");
        }
        return ResponseEntity.status(HttpStatus.OK).body("Mời bạn cập nhật mật khẩu");
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/reset")
    public ResponseEntity<?> resetPassword(@RequestParam String token, @RequestParam String email, @RequestBody ResetPasswordDTO resetPasswordRequest) {
        try {
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Yêu cầu không hợp lệ hoặc hết hạn");
            }
            Claims claims = accountService.verifyToken(token);
            if (claims == null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Yêu cầu không hợp lệ hoặc hết hạn");
            }
            Optional<Account> account = accountRepository.findByEmail(claims.getSubject());
            if (account.isEmpty()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email không tồn tại!");
            }
            if (!resetPasswordRequest.getPassword().equals(resetPasswordRequest.getConfirmPassword())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Passwords do not match");
            }
            String hashedPassword = passwordEncoder.encode(resetPasswordRequest.getPassword());
            account.get().setPassword(hashedPassword);
            accountRepository.save(account.get());
            return ResponseEntity.status(HttpStatus.OK).body("Cập nhật mật khẩu thành công");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Có lỗi xảy ra");
        }
    }
}
