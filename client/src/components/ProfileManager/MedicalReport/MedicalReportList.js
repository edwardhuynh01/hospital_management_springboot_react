import React, { useEffect, useState } from "react";
import styled from "styled-components";
import MedicalReport from "./MedicalReport";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
import reportApi from "../../../api/reportApi";
import MedicalReports from "./MedicalReports";
const Header6 = styled.h6`
  font-family: Averta, serif;
  font-size: 0.625rem;
  letter-spacing: 0.08rem;
`;
function MedicalReportList(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [reportList, setReportList] = useState([]);
  const formatDate = (date) => {
    if (!date) return "Invalid Date"; // Trả về chuỗi thông báo nếu không có giá trị ngày
    const dateObj = new Date(date);
    if (isNaN(dateObj.getTime())) return "Invalid Date"; // Kiểm tra xem dateObj có hợp lệ không

    const day = String(dateObj.getDate()).padStart(2, "0");
    const month = String(dateObj.getMonth() + 1).padStart(2, "0"); // Tháng trong JavaScript bắt đầu từ 0
    const year = dateObj.getFullYear();

    return `${day} - ${month} - ${year}`;
  };
  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchRecord = async () => {
        const data = await reportApi.getAllReport(
          axiosJWT,
          navigated,
          accessToken
        );
        const databyUserId = data.data.filter((x) => x.account === user._id);
        setReportList(databyUserId.reverse());
      };
      fetchRecord();
    } catch (error) {}
  }, []);
  return (
    <div className="ml-5">
      <div className="flex flex-wrap justify-start items-start">
        <div className="w-full">
          <Header6 className="text-[#627792] font-light">MEDICAL BILLS</Header6>
        </div>
        <div className="w-full">
          <h1 className="mb-0 text-2xl font-medium font-sans">
            Danh sách phiếu khám bệnh
          </h1>
        </div>
        <div className="w-full mr-2 mt-4">
          {reportList?.map((reportItem, index) => (
            <MedicalReports
              key={index}
              isPay={reportItem?.isPayment}
              reportId={reportItem?._id}
              recordId={reportItem?.record}
              createAt={formatDate(reportItem?.createdAt)}
              appointment={reportItem?.appointment}
            ></MedicalReports>
          ))}
        </div>
      </div>
    </div>
  );
}

export default MedicalReportList;
