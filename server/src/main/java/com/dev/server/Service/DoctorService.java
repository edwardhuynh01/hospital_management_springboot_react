package com.dev.server.Service;

import com.dev.server.model.Doctor;
import com.dev.server.repository.DoctorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class DoctorService {
    private final DoctorRepository doctorRepository;

    public Doctor createDoctor(Doctor doctor) {
        try {
            return doctorRepository.save(doctor);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo bác sĩ: " + e.getMessage(), e);
        }
    }

    public List<Doctor> getAllDoctors() {
        try {
            return doctorRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy danh sách tất cả bác sĩ: " + e.getMessage(), e);
        }
    }

    public Optional<Doctor> getDoctorById(String id) {
        try {
            return doctorRepository.findById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy thông tin bác sĩ với id: " + id, e);
        }
    }

    public Doctor updateDoctor(String id, Doctor doctorDetails) {
        try {
            Optional<Doctor> optionalDoctor = doctorRepository.findById(id);

            if (optionalDoctor.isPresent()) {
                Doctor existingDoctor = optionalDoctor.get();
                existingDoctor.setName(doctorDetails.getName());
                existingDoctor.setPhoneNumber(doctorDetails.getPhoneNumber());
                existingDoctor.setEmail(doctorDetails.getEmail());
                existingDoctor.setAvatarImage(doctorDetails.getAvatarImage());
                existingDoctor.setExperience(doctorDetails.getExperience());
                existingDoctor.setDateOfWeek(doctorDetails.getDateOfWeek());
                existingDoctor.setSex(doctorDetails.getSex());
                existingDoctor.setDays(doctorDetails.getDays());
                existingDoctor.setSpecialist(doctorDetails.getSpecialist());
                existingDoctor.setDegrees(doctorDetails.getDegrees());
                return doctorRepository.save(existingDoctor);
            } else {
                throw new RuntimeException("Không tìm thấy bác sĩ với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi cập nhật bác sĩ: " + e.getMessage(), e);
        }
    }

    public boolean deleteDoctor(String id) {
        try {
            Optional<Doctor> optionalDoctor = doctorRepository.findById(id);

            if (optionalDoctor.isPresent()) {
                doctorRepository.deleteById(id);
                return true;
            } else {
                throw new RuntimeException("Không tìm thấy bác sĩ với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi xóa bác sĩ với id: " + id, e);
        }
    }
}

