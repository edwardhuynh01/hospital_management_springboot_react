package com.dev.server.Controller;

import com.dev.server.Service.AccountService;
import com.dev.server.model.Report;
import com.dev.server.Service.ReportService;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/Report")
public class ReportApiController {
    @Autowired
    AccountService accountService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private ReportService reportService;

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping
    public ResponseEntity<?> getAllReports() {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    List<Report> reports = reportService.getAllReports();
                    return ResponseEntity.ok(reports);
                }
            }
        }catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }

    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PostMapping
    public ResponseEntity<?> createReport(@RequestBody Report report) {
        try{
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        return ResponseEntity.status(HttpStatus.CREATED).body(reportService.createReport(report));
                    }
                }
            }
        }catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping("/{_id}")
    public ResponseEntity<?> getReportById(@PathVariable String _id) {
        try{
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    Report report = reportService.getReportById(_id)
                            .orElseThrow(() -> new RuntimeException("Report not found on :: " + _id));
                    return ResponseEntity.ok().body(report);
                }
            }
        }catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PutMapping("/{_id}")
    public ResponseEntity<?> updateReport(@PathVariable String _id, @RequestBody Report report) {
        try{
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        Report reportUpdated = reportService.updateReport(_id, report);
                        return ResponseEntity.ok().body(reportUpdated);
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @DeleteMapping("/{_id}")
    public ResponseEntity<?> deleteReport(@PathVariable String _id) {
        try{
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        reportService.deleteReport(_id);
                        return ResponseEntity.noContent().build();
                    }
                }
            }
        }catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }
}
