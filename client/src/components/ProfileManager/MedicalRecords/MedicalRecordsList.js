import React, { useEffect, useState } from "react";
import MedicalRecords from "./MedicalRecords";
import styled from "styled-components";
import recordApi from "../../../api/recordApi";
import { toast } from "react-toastify";
import { confirmAlert } from "react-confirm-alert";
import { useNavigate } from "react-router-dom";
import { createAxios } from "../../../createInstance";
import { useDispatch, useSelector } from "react-redux";
import { loginSuccess } from "../../../redux/authSlice";
const Header6 = styled.h6`
  font-family: Averta, serif;
  font-size: 0.625rem;
  letter-spacing: 0.08rem;
`;
function MedicalRecordsList() {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [recordList, setRecordList] = useState([]);
  const [isDeleted, setIsDeleted] = useState(false);
  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchRecord = async () => {
        const data = await recordApi.getAllRecord(
          axiosJWT,
          navigated,
          accessToken
        );
        const databyUserId = data.data.filter((x) => x.account === user._id);
        setRecordList(databyUserId.reverse());
      };
      fetchRecord();
    } catch (error) {}
  }, [isDeleted]);
  const handleDelete = () => {
    setIsDeleted(!isDeleted);
  };
  let navigate = useNavigate();
  const handleOnClick = (id) => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className="bg-slate-100 p-6 rounded-lg shadow-xl">
          <h1 className="text-2xl text-[#fb6f92] font-semibold mb-4 text-start h-full w-full">
            Xác nhận xóa
          </h1>
          <p className="mb-4">Bạn có chắc chắn muốn xóa hồ sơ này?</p>
          <div className="flex justify-center gap-4">
            <button
              onClick={async () => {
                try {
                  const deleteData = await recordApi.deleteRecord(
                    axiosJWT,
                    navigated,
                    accessToken,
                    id
                  );
                  if (deleteData) {
                    navigate("/profile", {
                      state: { message: "Xóa hồ sơ thành công!" },
                    });
                    handleDelete();
                  } else {
                    toast.error("Đã xảy ra lỗi khi xóa hồ sơ.");
                  }
                } catch (error) {
                  toast.error("Đã xảy ra lỗi khi xóa hồ sơ.");
                  console.error("Error deleting record:", error);
                }
                onClose();
              }}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-red-600 duration-300 bg-red-400 text-white px-8 py-2 rounded-lg mr-2"
            >
              Có
            </button>
            <button
              onClick={onClose}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-gray-400 duration-300 bg-gray-300 text-black px-4 py-2 rounded-lg"
            >
              Không
            </button>
          </div>
        </div>
      ),
    });
  };
  return (
    <div className="ml-5">
      <div className="flex flex-wrap justify-start items-start">
        <div className="w-full">
          <Header6 className="text-[#627792] font-light">
            PATIENT'S RECORDS
          </Header6>
        </div>
        <div className="w-full">
          <h1 className="mb-0 text-2xl font-medium font-sans">
            Danh sách hồ sơ bệnh nhân
          </h1>
        </div>
        <div className="w-full">
          {recordList?.map((recordItem) => (
            <MedicalRecords
              onClick={handleOnClick}
              key={recordItem._id}
              id={recordItem._id}
              fullName={recordItem.fullName}
              email={recordItem.email}
              date={recordItem.date}
              sex={recordItem.sex}
              IdentityCardNumber={recordItem.IdentityCardNumber}
              address={recordItem.address}
              Peoples={recordItem.Peoples}
              phoneNumber={recordItem.phoneNumber}
            ></MedicalRecords>
          ))}
        </div>
      </div>
    </div>
  );
}

export default MedicalRecordsList;
