package com.dev.server.Service;

import com.dev.server.model.News;
import com.dev.server.repository.NewsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class NewsService {
    private final NewsRepository newsRepository;

    public News createNews(News news) {
        try {
            return newsRepository.save(news);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo tin tức: " + e.getMessage(), e);
        }
    }

    public List<News> getAllNews() {
        try {
            return newsRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy danh sách tất cả tin tức: " + e.getMessage(), e);
        }
    }

    public Optional<News> getNewsById(String id) {
        try {
            return newsRepository.findById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy thông tin tin tức với id: " + id, e);
        }
    }

    public News updateNews(String id, News newsDetails) {
        try {
            Optional<News> optionalNews = newsRepository.findById(id);

            if (optionalNews.isPresent()) {
                News existingNews = optionalNews.get();
                existingNews.setTitle(newsDetails.getTitle());
                existingNews.setTeaser(newsDetails.getTeaser());
                existingNews.setMainImage(newsDetails.getMainImage());
                existingNews.setContent(newsDetails.getContent());
                existingNews.setAuthor(newsDetails.getAuthor());
                return newsRepository.save(existingNews);
            } else {
                throw new RuntimeException("Không tìm thấy tin tức với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi cập nhật tin tức: " + e.getMessage(), e);
        }
    }

    public boolean deleteNews(String id) {
        try {
            Optional<News> optionalNews = newsRepository.findById(id);

            if (optionalNews.isPresent()) {
                newsRepository.deleteById(id);
                return true;
            } else {
                throw new RuntimeException("Không tìm thấy tin tức với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi xóa tin tức với id: " + id, e);
        }
    }
}
