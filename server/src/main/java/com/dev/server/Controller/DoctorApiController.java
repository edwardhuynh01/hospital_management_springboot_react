package com.dev.server.Controller;

import com.dev.server.Service.AccountService;
import com.dev.server.Service.DoctorService;
import com.dev.server.model.Doctor;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/Doctor")
public class DoctorApiController {

    @Autowired
    private DoctorService doctorService;
    @Autowired
    AccountService accountService;
    @Autowired
    private HttpServletRequest request;

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping
    public ResponseEntity<?> getAllDoctor() {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    List<Doctor> doctors = doctorService.getAllDoctors();
                    return ResponseEntity.ok(doctors);
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PostMapping
    public ResponseEntity<?> createDoctor(@RequestBody Doctor doctor) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        return ResponseEntity.status(HttpStatus.CREATED).body(doctorService.createDoctor(doctor));
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping("/{_id}")
    public ResponseEntity<?> getDoctorById(@PathVariable String _id) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    Doctor doctor = doctorService.getDoctorById(_id)
                            .orElseThrow(() -> new RuntimeException("Doctor not found on :: " + _id));
                    return ResponseEntity.ok().body(doctor);
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PutMapping("/{_id}")
    public ResponseEntity<?> updateDoctor(@PathVariable String _id, @RequestBody Doctor doctor) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        Doctor doctorUpdated = doctorService.updateDoctor(_id, doctor);
                        return ResponseEntity.ok().body(doctorUpdated);
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @DeleteMapping("/{_id}")
    public ResponseEntity<?> deleteDoctor(@PathVariable String _id) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        doctorService.deleteDoctor(_id);
                        return ResponseEntity.noContent().build();
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }
}

