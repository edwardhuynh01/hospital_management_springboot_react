import React from "react";
import { IoFitnessOutline } from "react-icons/io5";

function InformationPayment(props) {
  return (
    <div className="w-full flex flex-wrap text-[#495057]">
      <div className="w-full flex justify-between items-end py-3 border-b border-dashed border-[#fb6f92]">
        <div className="flex justify-start">
          <div className="mx-2">
            <IoFitnessOutline></IoFitnessOutline>
          </div>
          <p className="text-sm">Chuyên khoa</p>
        </div>
        <p className="text-base">{props?.name}</p>
      </div>
      <div className="w-full flex flex-wrap items-end py-3 border-b border-solid border-[#ced4da]">
        <div className="flex justify-between w-full">
          <div className="flex justify-start">
            <div className="mx-2">
              <IoFitnessOutline></IoFitnessOutline>
            </div>
            <p className="text-sm">Dịch vụ</p>
          </div>
          <p className="text-base">KHÁM DỊCH VỤ</p>
        </div>
        <div className="w-full text-end flex items-end text-[#495057] pb-3 justify-between mt-3">
          <p className="text-sm px-2">Tiền khám</p>
          <p className="font-semibold text-sm w-40 text-[#495057]">
            {props?.price.toLocaleString("vi-VN")} VNĐ
          </p>
        </div>
      </div>
    </div>
  );
}

export default InformationPayment;
