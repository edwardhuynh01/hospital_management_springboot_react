package com.dev.server.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponseDTO {
    private String _id;
    private String username;
    private String email;
    private String phoneNumber;
    private String role="customer";
    private Boolean EmailConfirmed = false;
    private String accessToken;
}
