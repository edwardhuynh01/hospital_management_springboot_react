
const decode = (id) => {
    return atob(id);
};
const reportApi = {
    getAllReport: async (axiosJWT, navigate, accessToken) => {
        try {
            const url = "http://localhost:5000/api/Report";
            return axiosJWT.get(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    getReportById: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `http://localhost:5000/api/Report/${decode(id)}`;
            return axiosJWT.get(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    createReport: async (axiosJWT, navigate, accessToken, data) => {
        try {
            const url = "http://localhost:5000/api/Report";
            return axiosJWT.post(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    updateReport: async (axiosJWT, navigate, accessToken, id, data) => {
        try {
            const url = `http://localhost:5000/api/Report/${decode(id)}`;
            return axiosJWT.put(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    deleteReport: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `http://localhost:5000/api/Report/${id}`;
            return axiosJWT.delete(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
};
export default reportApi;
