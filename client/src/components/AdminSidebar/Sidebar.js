import React, { useState, useContext, useEffect } from 'react';
import { Link, useLocation, useHistory } from 'react-router-dom';
import 'boxicons'
import './sidebar.css'
import '../../assets/shared/admin.css'
import useStore from '../../utils/useStore';

const Sidebar = () => {
    const selectedItem = useStore((state) => state.selectedItem);
    const setSelectedItem = useStore((state) => state.setSelectedItem);
    const location = useLocation();

    useEffect(() => {
        const path = location.pathname;
        if (path.includes('/admin/dashboard')) {
            setSelectedItem('Thống Kê');
        } else if (path.includes('/admin/doctor')) {
            setSelectedItem('Bác Sĩ');
        } else if (path.includes('/admin/appointment')) {
            setSelectedItem('Lịch Khám');
        } else if (path.includes('/admin/news')) {
            setSelectedItem('Tin Tức');
        } else if (path.includes('/admin/service')) {
            setSelectedItem('Dịch Vụ');
        } else if (path.includes('/admin/specialists')) {
            setSelectedItem('Chuyên Ngành');
        }
    }, [location.pathname, setSelectedItem]);

    const handleItemClick = (item) => {
        setSelectedItem(item);
    };

    return (
        // <div class="sidebar">
        //     <img src="https://play.tailwindcss.com/img/beams.jpg" alt="" class="absolute top-1/2 left-1/2 max-w-none -translate-x-1/2 -translate-y-1/2" width="1308" />
        //     <div className="logo-section h-24">
        //         <a href="#" className="logo">
        //             <i className="bx bx-code-alt"></i>
        //             <img src="/sinhto.png" width="100" height="100" objectFit="cover" objectPosition="bottom" />
        //         </a>
        //         <span className="logo-text">Sinh Tố Dâu Hospital</span>
        //     </div>
        //     <ul class="side-menu" style={{ marginTop: '55px' }}>
        //         <li><a className='sidebar-link' href="#" style={{ fontSize: '14.5px', fontWeight: 'bold' }}><span className="sidebar-item-indicator"></span><box-icon type='solid' name='dashboard' style={{ marginRight: '1.5em', marginLeft: '0.75em' }}></box-icon>Thống Kê</a></li>
        //         <li><a className='sidebar-link' href="#" style={{ fontSize: '14.5px', fontWeight: 'bold' }}><span className="sidebar-item-indicator"></span><box-icon type='solid' name='user' style={{ marginRight: '1.5em', marginLeft: '0.75em' }}></box-icon>Bác sĩ</a></li>
        //         <li><a className='sidebar-link' href="#" style={{ fontSize: '14.5px', fontWeight: 'bold' }}><span className="sidebar-item-indicator"></span><box-icon name='plus-medical' style={{ marginRight: '1.5em', marginLeft: '0.75em' }}></box-icon>Lịch Khám</a></li>
        //         <li><a className='sidebar-link' href="#" style={{ fontSize: '14.5px', fontWeight: 'bold' }}><span className="sidebar-item-indicator"></span><box-icon type='solid' name='news' style={{ marginRight: '1.5em', marginLeft: '0.75em' }}></box-icon>Tin Tức</a></li>
        //         <li><a className='sidebar-link' href="#" style={{ fontSize: '14.5px', fontWeight: 'bold' }}><span className="sidebar-item-indicator"></span><box-icon type='solid' name='window-alt' style={{ marginRight: '1.5em', marginLeft: '0.75em' }}></box-icon>Dịch vụ</a></li>
        //         <li><a className='sidebar-link' href="#" style={{ fontSize: '14.5px', fontWeight: 'bold' }}><span className="sidebar-item-indicator"></span><box-icon type='solid' name='medal' style={{ marginRight: '1.5em', marginLeft: '0.75em' }}></box-icon>Chuyên Nghành</a></li>
        //         <li><a className='sidebar-link' href="#" style={{ fontSize: '14.5px', fontWeight: 'bold' }}><span className="sidebar-item-indicator"></span><box-icon type='solid' name='user-account' style={{ marginRight: '1.5em', marginLeft: '0.75em' }}></box-icon>Tài khoản</a></li>
        //     </ul>
        //     {/* <ul class="side-menu">
        //         <li>
        //             <a href="#" class="logout">
        //                 <i class='bx bx-log-out-circle'></i>
        //                 Logout
        //             </a>
        //         </li>
        //     </ul> */}
        // </div>
        <div class="sidebar">
            <div class="fixed left-0 top-0 w-64 h-full bg-white p-4 z-50 sidebar-menu transition-transform">

                <div class="relative z-10">
                    <div className="logo-section h-24">
                        <a href="#" className="logo">
                            <i className="bx bx-code-alt"></i>
                            <img src="/sinhto.png" width="100" height="100" objectFit="cover" objectPosition="bottom" />
                        </a>
                        <span className="logo-text">Sinh Tố Dâu Hospital</span>
                    </div>
                    <ul class="mt-4" style={{ marginTop: '30px' }}>
                        <li className="mb-1 group" style={{ marginBot: '12px' }}>
                            <Link to="/admin/dashboard" className={`flex items-center py-2 px-4 text-black-300 hover:text-black transition-transform duration-500 transform hover:translate-x-2.5 rounded-md group-[.active]:translate-x-2.5 group-[.active]:opacity-70 group-[.active]:text-black ${selectedItem === 'Thống Kê' ? 'translate-x-2.5 text-black bg-gray-100' : ''} group-[.selected]:translate-x-2.5 group-[.selected]:bg-white group-[.selected]:text-black`} onClick={() => handleItemClick('Thống Kê')} style={{ fontSize: "15.5px", fontWeight: "bold", fontFamily: 'Tahoma', width: 215 }}>
                                <span className={`absolute left-0 w-1 h-full bg-sky-400 opacity-0 group-hover:opacity-100 group-[.active]:opacity-50 group-[.selected]:opacity-100 ${selectedItem === 'Thống Kê' ? 'opacity-70 bg-blue-400' : ''}`}></span>
                                <box-icon type="solid" name="dashboard" style={{ marginRight: '1.5em', marginLeft: '0.2em' }}></box-icon>
                                Thống Kê
                            </Link>
                        </li>

                        <li class="mb-1 group" style={{ marginTop: '12px' }}>
                            <Link to="/admin/doctor" className={`flex items-center py-2 px-4 text-black-300 hover:text-black transition-transform duration-500 transform hover:translate-x-2.5 rounded-md group-[.active]:translate-x-2.5 group-[.active]:opacity-70 group-[.active]:text-black ${selectedItem === 'Bác Sĩ' ? 'translate-x-2.5 text-black bg-gray-100' : ''} group-[.selected]:translate-x-2.5 group-[.selected]:bg-white group-[.selected]:text-black`} onClick={() => handleItemClick('Bác Sĩ')} style={{ fontSize: "15.5px", fontWeight: "bold", fontFamily: 'Tahoma', width: 215 }}>
                                <span className={`absolute left-0 w-1 h-full bg-sky-400 opacity-0 group-hover:opacity-100 group-[.active]:opacity-50 group-[.selected]:opacity-100 ${selectedItem === 'Bác Sĩ' ? 'opacity-70 bg-blue-400' : ''}`}></span>
                                <box-icon type='solid' name='user' style={{ marginRight: "1.5em", marginLeft: "0.2em" }}></box-icon>
                                Bác Sĩ
                            </Link>
                            {/* <ul class="pl-7 mt-2 hidden group-[.selected]:block">
                                <li class="mb-4">
                                    <a href="#" class="text-black-300 text-sm flex items-center hover:text-gray-100 before:contents-[''] before:w-1 before:h-1 before:rounded-full before:bg-gray-300 before:mr-3">Active order</a>
                                </li>
                                <li class="mb-4">
                                    <a href="#" class="text-black-300 text-sm flex items-center hover:text-gray-100 before:contents-[''] before:w-1 before:h-1 before:rounded-full before:bg-gray-300 before:mr-3">Completed order</a>
                                </li>
                                <li class="mb-4">
                                    <a href="#" class="text-black-300 text-sm flex items-center hover:text-gray-100 before:contents-[''] before:w-1 before:h-1 before:rounded-full before:bg-gray-300 before:mr-3">Canceled order</a>
                                </li>
                            </ul> */}
                        </li>
                        <li class="mb-1 group" style={{ marginTop: '12px' }}>
                            <Link to="/admin/appointment" className={`flex items-center py-2 px-4 text-black-300 hover:text-black transition-transform duration-500 transform hover:translate-x-2.5 rounded-md group-[.active]:translate-x-2.5 group-[.active]:opacity-70 group-[.active]:text-black ${selectedItem === 'Lịch Khám' ? 'translate-x-2.5 text-black bg-gray-100' : ''} group-[.selected]:translate-x-2.5 group-[.selected]:bg-white group-[.selected]:text-black`} onClick={() => handleItemClick('Lịch Khám')} style={{ fontSize: "15.5px", fontWeight: "bold", fontFamily: 'Tahoma', width: 215 }}>
                                <span className={`absolute left-0 w-1 h-full bg-sky-400 opacity-0 group-hover:opacity-100 group-[.active]:opacity-50 group-[.selected]:opacity-100 ${selectedItem === 'Lịch Khám' ? 'opacity-70 bg-blue-400' : ''}`}></span>
                                <box-icon name='plus-medical' style={{ marginRight: "1.5em", marginLeft: "0.2em" }}></box-icon>
                                Lịch Khám
                            </Link>
                            {/* <ul class="pl-7 mt-2 hidden group-[.selected]:block">
                                <li class="mb-4">
                                    <a href="#" class="text-black-300 text-sm flex items-center hover:text-gray-100 before:contents-[''] before:w-1 before:h-1 before:rounded-full before:bg-gray-300 before:mr-3">Manage services</a>
                                </li>
                            </ul> */}
                        </li>
                        <li class="mb-1 group" style={{ marginTop: '12px' }}>
                            <Link to="/admin/news" className={`flex items-center py-2 px-4 text-black-300 hover:text-black transition-transform duration-500 transform hover:translate-x-2.5 rounded-md group-[.active]:translate-x-2.5 group-[.active]:opacity-70 group-[.active]:text-black ${selectedItem === 'Tin Tức' ? 'translate-x-2.5 text-black bg-gray-100' : ''} group-[.selected]:translate-x-2.5 group-[.selected]:bg-white group-[.selected]:text-black`} onClick={() => handleItemClick('Tin Tức')} style={{ fontSize: "15.5px", fontWeight: "bold", fontFamily: 'Tahoma', width: 215 }}>
                                <span className={`absolute left-0 w-1 h-full bg-sky-400 opacity-0 group-hover:opacity-100 group-[.active]:opacity-50 group-[.selected]:opacity-100 ${selectedItem === 'Tin Tức' ? 'opacity-70 bg-blue-400' : ''}`}></span>
                                <box-icon type='solid' name='news' style={{ marginRight: "1.5em", marginLeft: "0.2em" }}></box-icon>
                                Tin Tức
                            </Link>
                        </li>
                        <li class="mb-1 group" style={{ marginTop: '12px' }}>
                            <Link to="/admin/service" className={`flex items-center py-2 px-4 text-black-300 hover:text-black transition-transform duration-500 transform hover:translate-x-2.5 rounded-md group-[.active]:translate-x-2.5 group-[.active]:opacity-70 group-[.active]:text-black ${selectedItem === 'Dịch Vụ' ? 'translate-x-2.5 text-black bg-gray-100' : ''} group-[.selected]:translate-x-2.5 group-[.selected]:bg-white group-[.selected]:text-black`} onClick={() => handleItemClick('Dịch Vụ')} style={{ fontSize: "15.5px", fontWeight: "bold", fontFamily: 'Tahoma', width: 215 }}>
                                <span className={`absolute left-0 w-1 h-full bg-sky-400 opacity-0 group-hover:opacity-100 group-[.active]:opacity-50 group-[.selected]:opacity-100 ${selectedItem === 'Dịch Vụ' ? 'opacity-70 bg-blue-400' : ''}`}></span>
                                <box-icon type="solid" name="window-alt" style={{ marginRight: "1.5em", marginLeft: "0.2em" }}></box-icon>
                                Dịch Vụ
                            </Link>
                        </li>
                        <li class="mb-1 group" style={{ marginTop: '12px' }}>
                            <Link to="/admin/specialists" className={`flex items-center py-2 px-4 text-black-300 hover:text-black transition-transform duration-500 transform hover:translate-x-2.5 rounded-md group-[.active]:translate-x-2.5 group-[.active]:opacity-70 group-[.active]:text-black ${selectedItem === 'Chuyên Ngành' ? 'translate-x-2.5 text-black bg-gray-100' : ''} group-[.selected]:translate-x-2.5 group-[.selected]:bg-white group-[.selected]:text-black`} onClick={() => handleItemClick('Chuyên Ngành')} style={{ fontSize: "15.5px", fontWeight: "bold", fontFamily: 'Tahoma', width: 215 }}>
                                <span className={`absolute left-0 w-1 h-full bg-sky-400 opacity-0 group-hover:opacity-100 group-[.active]:opacity-50 group-[.selected]:opacity-100 ${selectedItem === 'Chuyên Ngành' ? 'opacity-70 bg-blue-400' : ''}`}></span>
                                <box-icon type="solid" name="medal" style={{ marginRight: "1.5em", marginLeft: "0.2em" }}></box-icon>
                                Chuyên Khoa
                            </Link>
                        </li>
                        <li class="mb-1 group " style={{ marginTop: '12px' }}>
                            <Link to="/admin/account" className={`flex items-center py-2 px-4 text-black-300 hover:text-black transition-transform duration-500 transform hover:translate-x-2.5 rounded-md group-[.active]:translate-x-2.5 group-[.active]:opacity-70 group-[.active]:text-black ${selectedItem === 'Tài Khoản' ? 'translate-x-2.5 text-black bg-gray-100' : ''} group-[.selected]:translate-x-2.5 group-[.selected]:bg-white group-[.selected]:text-black`} onClick={() => handleItemClick('Tài Khoản')} style={{ fontSize: "15.5px", fontWeight: "bold", fontFamily: 'Tahoma', width: 215 }}>
                                <span className={`absolute left-0 w-1 h-full bg-sky-400 opacity-0 group-hover:opacity-100 group-[.active]:opacity-50 group-[.selected]:opacity-100 ${selectedItem === 'Tài Khoản' ? 'opacity-70 bg-blue-400' : ''}`}></span>
                                <box-icon type="solid" name="user-account" style={{ marginRight: "1.5em", marginLeft: "0.2em" }}></box-icon>
                                Tài Khoản
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Sidebar;
