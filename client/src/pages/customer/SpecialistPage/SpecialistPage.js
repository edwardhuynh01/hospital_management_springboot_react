import React, { useEffect, useState } from "react";
import Title from "../../../components/Title/Title";
import "./SpecialistStyle.css";
import Specialist from "../../../api/specialistApi";
import { Link } from "react-router-dom";
import Loading from "../../../components/Loading/Loading";
const SpecialistPage = () => {
  const [specialists, setSpecialists] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    const fetchSpecialists = async () => {
      try {
        const data = await Specialist.getAllSpecialist();
        setSpecialists(data);
      } catch (error) {

      }
      finally {
        setIsLoading(false);
      }
    };
    fetchSpecialists();
  }, []);
  const encode = (id) => {
    return btoa(id);
  };
  return (
    <div className="container mx-auto bg-slate-100">
      <Title>DANH SÁCH CHUYÊN KHOA</Title>
      {
        isLoading ? (
          <Loading />
        ) : (
          <div className="flex justify-center">
            <div
              className={`grid grid-cols-1 lg:grid-cols-2 my-6 align-middle w-[80%] gap-6`}
            >
              {specialists.map((specialistsItem) => (
                <div
                  className={`flex rounded-2xl w-full h-[80px] overflow-hidden shadow-trb bg-white`}
                  id="col"
                  key={specialistsItem?._id}
                >
                  <div className="bg-second-gradient-to-left w-[80px] h-[80px]">
                    <Link
                      to={`/specialist/${encode(specialistsItem?._id)}`}
                      className=""
                    >
                      <img
                        className="h-full w-full"
                        src={`data:image/png;base64,${specialistsItem?.specialistImg}`}
                        alt={specialistsItem?.specialistName}
                      />
                    </Link>
                  </div>
                  <div className="pl-[30px] pr-[15px] py-[15px]">
                    <div className="">
                      <Link
                        to={`/specialist/${encode(specialistsItem?._id)}`}
                        className="text-xl text-[#9d4edd] font-bold pb-2 cursor-pointer"
                      >
                        {specialistsItem?.specialistName}
                      </Link>
                    </div>
                    <div>
                      <Link
                        to={`/specialist/${encode(specialistsItem?._id)}`}
                        className={`transition-colors cursor-pointer`}
                        id="views-detail"
                      >
                        Xem chi tiết
                      </Link>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        )
      }
    </div>
  );
};

export default SpecialistPage;
