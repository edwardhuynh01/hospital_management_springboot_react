package com.dev.server.repository;

import com.dev.server.model.Report;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends MongoRepository<Report, String> {
    public Report findReportByPaymentId(String paymentId);
}
