import { ROUTERS } from "./utils/router";
import HomePage from "./pages/customer/homePage/homePage";
import { Route, Routes } from "react-router-dom";
import MasterLayout from "./pages/customer/theme/masterLayout/masterLayout";
import ServicePage from "./pages/customer/servicePage/servicePage";
import ContactPage from "./pages/customer/ContactPage/ContactPage";
import SearchPage from "./pages/customer/SearchPage/SearchPage";
import CustomerGuidePage from "./pages/customer/CustomerGuidePage/CustomerGuidePage";
import Login from "./pages/login/login";
import Register from "./pages/Register/Register";
import ProfileManager from "./pages/customer/ProfileManager/ProfileManager";
import DetailMedicalRecord from "./components/ProfileManager/MedicalRecords/DetailMedicalRecord";
import MakeMedicalRecord from "./components/ProfileManager/MedicalRecords/MakeMedicalRecord";
import UpdateMedicalRecord from "./components/ProfileManager/MedicalRecords/UpdateMedicalRecord";
import SpecialistPage from "./pages/customer/SpecialistPage/SpecialistPage";
import NewPages from "./pages/customer/NewsPage/NewPages";
import ChooseRecord from "./pages/customer/Appointment/ChooseRecord";
import ChooseSpecialist from "./pages/customer/Appointment/ChooseSpecialist";
import ChooseDoctor from "./pages/customer/Appointment/ChooseDoctor";
import ChooseMedicalSchedule from "./pages/customer/Appointment/ChooseMedicalSchedule";
import ConfirmInformation from "./pages/customer/Appointment/ConfirmInformation";
import NewsPagesDetail from "./pages/customer/NewsPage/NewsPagesDetail";
import ServicePageDetail from "./pages/customer/servicePage/ServicePageDetail";
import SpecialistPageDetail from "./pages/customer/SpecialistPage/SpecialistPageDetail";
import ResetPassword from "./pages/fogotPassword/ResetPassword";
import ForgotPassword from "./pages/fogotPassword/ForgotPassword";
import ConfirmEmail from "./pages/Register/ConfirmEmail";
import DetailNotification from "./pages/customer/ProfileManager/DetailNotification";
import Payment from "./pages/customer/Appointment/Payment";
import PaymentSuccess from "./pages/customer/Appointment/PaymentSuccess";
import PaymentCancel from "./pages/customer/Appointment/PaymentCancel";
import { Navigate } from 'react-router-dom';
import AdminLayout from "./components/AdminLayout/AdminLayout";
import DashboardPage from "./pages/admin/dashboardPage";
import DoctorsPage from "./pages/admin/doctorsPage";
import NewsPage from "./pages/admin/newsPage";
import SpecialistsPage from "./pages/admin/specialistsPage";
import AppointmentsPage from "./pages/admin/appointmentsPage";
import ServicesesPage from "./pages/admin/servicesesPage";
import AccountsPage from "./pages/admin/accountsPage";


const renderCustomerRouter = () => {
  const customerRouter = [
    {
      path: ROUTERS.CUSTOMER.HOME,
      component: HomePage,
    },
    {
      path: ROUTERS.CUSTOMER.SERVICE,
      component: ServicePage,
    },
    {
      path: ROUTERS.CUSTOMER.SERVICEDETAIL,
      component: ServicePageDetail,
    },
    {
      path: ROUTERS.CUSTOMER.NEWS,
      component: NewPages,
    },
    {
      path: ROUTERS.CUSTOMER.SEARCH,
      component: SearchPage,
    },
    {
      path: ROUTERS.CUSTOMER.NEWSDETAIL,
      component: NewsPagesDetail,
    },
    {
      path: ROUTERS.CUSTOMER.DETAILNITIFICATION,
      component: DetailNotification,
    },
    {
      path: ROUTERS.CUSTOMER.PROFILE,
      component: ProfileManager,
    },
    {
      path: ROUTERS.CUSTOMER.DETAIL,
      component: DetailMedicalRecord,
    },
    {
      path: ROUTERS.CUSTOMER.MAKEMEDICALRC,
      component: MakeMedicalRecord,
    },
    {
      path: ROUTERS.CUSTOMER.UPDATEMEDICALRC,
      component: UpdateMedicalRecord,
    },
    {
      path: ROUTERS.CUSTOMER.CHOOSERECORD,
      component: ChooseRecord,
    },
    {
      path: ROUTERS.CUSTOMER.CHOOSESPECIALIST,
      component: ChooseSpecialist,
    },
    {
      path: ROUTERS.CUSTOMER.CHOOSEDOCTOR,
      component: ChooseDoctor,
    },
    {
      path: ROUTERS.CUSTOMER.CHOOSEMEDICALSCHEDULE,
      component: ChooseMedicalSchedule,
    },
    {
      path: ROUTERS.CUSTOMER.CONFIRMINFORMATION,
      component: ConfirmInformation,
    },
    {
      path: ROUTERS.CUSTOMER.PAYMENT,
      component: Payment,
    },
    {
      path: ROUTERS.CUSTOMER.SUCCESSPAYMENT,
      component: PaymentSuccess,
    },
    {
      path: ROUTERS.CUSTOMER.CANCELPAYMENT,
      component: PaymentCancel,
    },
    {
      path: ROUTERS.CUSTOMER.CUSTOMERGUDE,
      component: CustomerGuidePage,
    },
    {
      path: ROUTERS.CUSTOMER.SPECIALIST,
      component: SpecialistPage,
    },
    {
      path: ROUTERS.CUSTOMER.SPECIALISTDETAIL,
      component: SpecialistPageDetail,
    },
    {
      path: ROUTERS.CUSTOMER.CONTACT,
      component: ContactPage,
    },
    {
      path: ROUTERS.ACCOUNT.LOGIN,
      component: Login,
    },
    {
      path: ROUTERS.ACCOUNT.FORGOTPASSWORD,
      component: ForgotPassword,
    },
    {
      path: ROUTERS.ACCOUNT.RESETPASSWORD,
      component: ResetPassword,
    },
    {
      path: ROUTERS.ACCOUNT.REGISTER,
      component: Register,
    },
    {
      path: ROUTERS.ACCOUNT.CONFIRMEMAIL,
      component: ConfirmEmail,
    },
    {
      path: ROUTERS.ADMIN.ADMINPAGE,
      element: AdminLayout,
    },
  ];
  const withLayout = (Component, path) => {
    return props => {
      if (path.startsWith(ROUTERS.ADMIN.ADMINPAGE)) {
        return (
          <AdminLayout>
            <Component {...props} />
          </AdminLayout>
        );
      } else {
        return (
          <MasterLayout>
            <Component {...props} />
          </MasterLayout>
        );
      }
    };
  };
  // return (
  //   <MasterLayout>
  //     <Routes>
  //       {customerRouter.map((item, key) => (
  //         <Route key={key} path={item.path} element={item.component} />
  //       ))}
  //     </Routes>
  //   </MasterLayout>
  // );

  return (<Routes>
    {customerRouter.map(({ path, component }, index) => {
      const ComponentWithLayout = withLayout(component, path);
      return <Route key={index} path={path} element={<ComponentWithLayout />} />;
    })}
    <Route path="/admin/*" element={<AdminLayout />}>
      <Route index element={<Navigate replace to="dashboard" />} />
      <Route path="dashboard" element={<DashboardPage />} />
      <Route path="doctor" element={<DoctorsPage />} />
      <Route path="appointment" element={<AppointmentsPage />} />
      <Route path="news" element={<NewsPage />} />
      <Route path="service" element={<ServicesesPage />} />
      <Route path="specialists" element={<SpecialistsPage />} />
      <Route path="account" element={<AccountsPage />} />
    </Route>
  </Routes>
  );
};
const RouterCustom = () => {
  return renderCustomerRouter();
};
export default RouterCustom;
