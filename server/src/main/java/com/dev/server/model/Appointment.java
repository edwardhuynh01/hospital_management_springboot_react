package com.dev.server.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "appointments")
public class Appointment {
    @Id
    private String _id;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId id;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId recordId;
    private String dayOfWeek;
    private String day;
    private String month;
    private String year;
    private String timeStart;
    private String timeEnd;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId specialistId;
    private String services;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId doctorId;
}
