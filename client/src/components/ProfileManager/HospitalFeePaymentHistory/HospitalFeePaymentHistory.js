import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
import appointmentApi from "../../../api/appointmentApi";
import specialistApi from "../../../api/specialistApi";

function HospitalFeePaymentHistory(props) {
  const { reportId, createdAt, appId, isPayment } = props;
  const user = useSelector((state) => state.auth.login?.currentUser);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const accessToken = user?.accessToken;
  const axiosJWT = createAxios(user, dispatch, loginSuccess);
  const [appointment, setAppointment] = useState();
  const [specialist, setSpecialist] = useState(null);
  const encode = (id) => {
    return btoa(id);
  };
  //   console.log(appointmentId);
  const [priceTotal, setPriceTotal] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        if (!accessToken || typeof accessToken !== "string") {
          throw new Error("Token không hợp lệ");
        }

        let total = 0;
        const appointmentPromises = appId.map(async (id) => {
          const appointmentData = await appointmentApi.getAppointmentById(
            axiosJWT,
            navigate,
            accessToken,
            encode(id)
          );

          if (appointmentData.data?.specialistId) {
            const specialistData = await specialistApi.getSpecialistById(
              encode(appointmentData.data.specialistId)
            );

            total += specialistData.price;
            return { appointment: appointmentData.data, specialist: specialistData };
          }

          return { appointment: appointmentData.data, specialist: null };
        });

        const results = await Promise.all(appointmentPromises);

        setAppointment(results.map(result => result.appointment));
        setSpecialist(results.map(result => result.specialist));
        setPriceTotal(total);
      } catch (e) {
        console.error(e.message);
      }
    };

    fetchData();
  }, [appId, accessToken, axiosJWT, navigate]);
  const timeDifference = (dateString) => {
    const date = new Date(dateString);
    const now = new Date();
    const diffInSeconds = Math.floor((now - date) / 1000);

    if (diffInSeconds < 60) {
      return `${diffInSeconds} giây trước`;
    } else if (diffInSeconds < 3600) {
      const minutes = Math.floor(diffInSeconds / 60);
      return `${minutes} phút trước`;
    } else if (diffInSeconds < 86400) {
      const hours = Math.floor(diffInSeconds / 3600);
      return `${hours} tiếng trước`;
    } else {
      const days = Math.floor(diffInSeconds / 86400);
      return `${days} ngày trước`;
    }
  };
  const formatNumber = (number) => {
    if (typeof number !== "number" || isNaN(number)) {
      return "Invalid number"; // hoặc giá trị mặc định khác
    }
    return number.toLocaleString("de-DE"); // Định dạng theo chuẩn của Đức
  };
  return (
    <div className="flex gap-2 mt-4">
      <div className="flex flex-col items-center ">
        <div className="bg-[#ff8fab] h-4 rounded-full w-4 border-2 border-solid border-[#ff8fab] hover:bg-white"></div>
        <div className=" w-[2px] h-24 bg-slate-200"></div>
      </div>
      <div className="w-full">
        <p className="text-sm font-light text-slate-400">{timeDifference(createdAt)}</p>
        <p className="my-2 text-lg ">
          {
            isPayment ? (<p>Bạn đã thanh toán thành công!</p>) : (<p>Bạn chưa thanh toán!</p>)
          }
          <p>Mã phiếu khám của bạn là: {reportId}. </p>
          <p>Tổng số tiền: {formatNumber(priceTotal)}đ</p>
        </p>
        <a className="text-end cursor-pointer hover:text-pink-300 text-slate-400" href={`/DetailNotification/${encode(reportId)}`}><p className="mr-4">Xem chi tiết...</p></a>
      </div>
    </div>
  );;
}

export default HospitalFeePaymentHistory;
