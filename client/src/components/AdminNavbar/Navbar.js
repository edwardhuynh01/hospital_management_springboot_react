import React, { useContext } from "react";
import 'boxicons'
import './navbar.css'
import '../../assets/shared/admin.css'
import { Link, useLocation } from "react-router-dom";
import useStore from '../../utils/useStore';
import { IoLogOutOutline } from "react-icons/io5";

const Navbar = () => {

    const selectedItem = useStore((state) => state.selectedItem);

    return (
        <main class="w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
            <div class=" py-2 px-4 bg-white flex items-center shadow-md shadow-black/5 sticky top-0 left-0 z-30">
                {/* <button type="button" class="text-lg text-gray-600  flex justify-left items-left">
                    <box-icon name='menu' class="w-8 h-8"></box-icon>
                </button> */}

                <ul class="flex items-center justify-center text-sm ml-2">
                    <li class="mr-2">
                        <Link to="/admin/dashboard" className="text-gray-400 hover:text-gray-600" style={{ fontSize: "15px", fontWeight: "medium-bold" }}>Quản Trị</Link>
                    </li>
                    <li class="text-gray-600 mr-2" style={{ fontSize: "15px", fontWeight: "medium-bold" }}>/</li>
                    <li class="text-gray-600 mr-2" style={{ fontSize: "15px", fontWeight: "medium-bold" }}>{selectedItem}</li>
                </ul>
                <ul class="ml-auto flex items-center">
                    <li class="mr-1 dropdown">
                        <div className="flex items-center">
                            <button type="button" class="flex items-center w-9 h-9 dark:shadow-highlight/4 group ml-4 rounded-md shadow-sm ring-1 ring-gray-900/5 hover:bg-gray-50 focus:outline-none focus-visible:ring-2 focus-visible:ring-sky-500 sm:ml-0 dark:bg-gray-800 dark:ring-0 dark:hover:bg-gray-700 dark:focus-visible:ring-2 dark:focus-visible:ring-gray-400">
                                <span class="sr-only"><span class="dark:hidden">Switch to dark theme</span><span class="hidden dark:inline">Switch to light theme</span></span
                                ><svg width="36" height="36" viewBox="-6 -6 36 36" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="fill-sky-100 stroke-sky-400 group-hover:stroke-sky-600 dark:fill-gray-400/20 dark:stroke-gray-400 dark:group-hover:stroke-gray-300">
                                    <g class="dark:opacity-0">
                                        <path d="M15 12a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"></path>
                                        <path d="M12 4v.01M17.66 6.345l-.007.007M20.005 12.005h-.01M17.66 17.665l-.007-.007M12 20.01V20M6.34 17.665l.007-.007M3.995 12.005h.01M6.34 6.344l.007.007" fill="none"></path>
                                    </g>
                                    <g class="opacity-0 dark:opacity-100">
                                        <path d="M16 12a4 4 0 1 1-8 0 4 4 0 0 1 8 0Z"></path>
                                        <path d="M12 3v1M18.66 5.345l-.828.828M21.005 12.005h-1M18.66 18.665l-.828-.828M12 21.01V20M5.34 18.666l.835-.836M2.995 12.005h1.01M5.34 5.344l.835.836" fill="none"></path>
                                    </g>
                                </svg>
                            </button>
                            <div class="hidden sm:block mx-6 lg:mx-4 w-px h-6 bg-gray-200 dark:bg-gray-800"></div>
                            <li class="mr-1">
                                <button style={{ "alignItems": 'center' }} aria-label="Open user account menu" data-action="click:deferred-side-panel#loadPanel click:deferred-side-panel#panelOpened" data-show-dialog-id="dialog-382834fe-cd9d-45e8-984f-64d0cf355a4c" id="dialog-show-dialog-382834fe-cd9d-45e8-984f-64d0cf355a4c" type="button" class="bg-transparent p-0">
                                    <span class="flex alignItems-center justify-center" >
                                        <div class="mb-2">
                                            <a href="/"><IoLogOutOutline class="w-10 h-10 mt-2.5 hover:text-[#f6889e] rounded-md transition duration-100 ease-in delay-50 hover:scale-125"></IoLogOutOutline></a>
                                            {/* <img src="https://avatars.githubusercontent.com/u/122549079?v=4" alt="" class="w-9 h-9 rounded-full mt-2 "></img> */}
                                        </div>
                                    </span>
                                </button>
                            </li>
                        </div>
                        <div class="dropdown-menu shadow-md shadow-black/5 z-30 hidden max-w-xs w-full bg-white rounded-md border border-gray-100">
                            <form action="" class="p-4 border-b border-b-gray-100">
                                <div class="relative w-full">
                                    <input type="text" class="py-2 pr-4 pl-10 bg-gray-50 w-full outline-none border border-gray-100 rounded-md text-sm focus:border-blue-500" placeholder="Search..."></input>
                                    <i class="ri-search-line absolute top-1/2 left-4 -translate-y-1/2 text-gray-400"></i>
                                </div>
                            </form>
                        </div>
                    </li>

                    <li>
                        <button type="button">
                            <img></img>
                        </button>
                    </li>
                </ul>
            </div>
        </main>
    )
}
export default Navbar