import React from "react";
import styled from "styled-components";
import MedicalExaminationInformation from "./MedicalExaminationInformation";
import { useSelector } from "react-redux";
const ContainerList = styled.div`
  max-height: 300px;
  overflow-y: auto;
`;
function MedicalExaminationInformationsList(props) {
  const appointment = useSelector(
    (state) => state.appointment?.appointmentsList
  );
  return (
    <div className="w-full flex flex-wrap">
      {/* <div className="w-full flex">
        <div className="w-1/12">#</div>
        <div className="w-2/12">Chuyên khoa</div>
        <div className="w-1/12">Dịch vụ</div>
        <div className="w-1/12">Dịch vụ</div>
        <div className="w-2/12">Dịch vụ</div>
        <div className="w-3/12">Dịch vụ</div>
        <div className="w-3/12">Dịch vụ</div>
      </div>
      <ContainerList>
        <StyledList>
          <ListItem>
            <MedicalExaminationInformation></MedicalExaminationInformation>
          </ListItem>
        </StyledList>
      </ContainerList> */}
      <ContainerList className="w-full">
        <table className="w-full text-xs lg:text-base mb-6">
          <thead>
            <tr>
              <th className="text-start px-3 font-semibold py-4">#</th>
              <th className="text-start px-3 font-semibold py-4">
                Chuyên khoa
              </th>
              <th className="text-start px-3 font-semibold py-4">Dịch vụ</th>
              <th className="text-start px-3 font-semibold py-4">Bác sĩ</th>
              <th className="text-start px-3 font-semibold py-4">Giờ khám</th>
              <th className="text-start px-3 font-semibold py-4">Tiền khám</th>
            </tr>
          </thead>
          <tbody>
            {appointment?.map((appItem, index) => (
              <MedicalExaminationInformation
                key={index}
                stt={index + 1}
                item={appItem}
              ></MedicalExaminationInformation>
            ))}
          </tbody>
        </table>
      </ContainerList>
    </div>
  );
}

export default MedicalExaminationInformationsList;
