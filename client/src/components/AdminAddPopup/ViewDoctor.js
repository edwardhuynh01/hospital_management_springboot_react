import React, { useState } from 'react';
import ModelDoctorDetails from "./ModelDoctorDetails"; // Import component
const ViewDoctor = () => {
    const [showModal, setShowModal] = useState(false);
  const [selectedAccount, setSelectedAccount] = useState(null);

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  return (
    <>
      <button className="" onClick={toggleModal}>
        View Appointment
      </button>
      <ModelDoctorDetails
        showModal={showModal}
        setShowModal={setShowModal}
        selectedAccount={selectedAccount}
      />
    </>
  );
};

export default ViewDoctor;