import React from "react";
import Title from "../Title/Title";
const About1 = (props) => {
  return (
    <section className={` bg-white ${props.className || ""}`}>
      <div className="lg:block">
        <Title className="">VỀ CHÚNG TÔI</Title>
        <div className="text-[#010101] text-[1.1rem] font-medium text-center my-4">
          <p>
            Với tâm huyết nâng cao chất lượng khám chữa bệnh và tạo điều kiện
            thuận lợi cho người dân ngày càng tiếp cận dễ dàng hơn các dịch vụ y
            tế chất lượng cao và hiện đại.
          </p>
        </div>
      </div>
      <div className="lg:flex">
        <div className="lg:w-1/2">
          <img
            className="w-full h-full"
            src="/Images/vechungtoi.jpg"
            alt="Về chúng tôi"
          />
        </div>
        <div className="lg:w-1/2 bg-[aliceblue]">
          <p className="ml-10 pt-[80px] pr-10 pb-10">
            Bệnh Viện Đa Khoa Sinh Tố Dâu là Bệnh viện đầu tiên được thành lập
            theo mô hình hoàn toàn tự quản, tận dụng các nguồn lực nội bộ và cam
            kết mang đến dịch vụ y tế chất lượng nhất cho cộng đồng. Bệnh viện
            chính thức khai trương và đi vào hoạt động từ ngày 03 tháng 03 năm
            2018. Cùng với tinh thần tiên phong, thấu hiểu, chuẩn mực và an
            toàn, Bệnh Viện Đa Khoa Sinh Tố Dâu cam kết áp dụng mô hình quản lý
            dịch vụ y tế chuyên nghiệp theo chuẩn quốc tế, nhằm nâng cao chất
            lượng chăm sóc sức khỏe cho cộng đồng và cung cấp thêm nhiều lựa
            chọn trong dịch vụ khám chữa bệnh cho người dân.
          </p>
        </div>
      </div>
    </section>
  );
};

export default About1;
