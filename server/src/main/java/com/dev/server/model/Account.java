package com.dev.server.model;

import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "accounts")
public class Account {
    @Id
    private String _id;
    private String username;
    private String password;
    private String email;
    private String phoneNumber;
    private String role="customer";
    private Boolean EmailConfirmed = false;
}
