import React, { useEffect, useReducer, useState } from "react";
import styled from "styled-components";
import { forgotPassword } from "../../redux/apiRequest";
import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import LoadingEdit from "../../components/Loading/LoadingEdit.js";
const initialState = {
  email: "",
};
const reducer = (state, action) => {
  switch (action.type) {
    case "FOCUS_EMAIL":
      return { ...state, isEmailFocused: true };
    case "BLUR_EMAIL":
      return { ...state, isEmailFocused: false };
    case "CHANGE_EMAIL":
      return { ...state, email: action.payload };
    default:
      return state;
  }
};
const BtnSubmit = styled.button`
  width: 90%;
  height: 2.5em;
  border-radius: 8px;
  border: 1px solid #d1d1d1;
  background: rgba(255, 104, 159, 0.75);
  box-shadow: 0px 15px 30px -10px rgba(255, 189, 213, 0.3);
  color: #fff;
  text-align: center;
  font-size: 1em;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  transition: color 0.25s, border-color 0.25s, box-shadow 0.25s, transform 0.25s;
  cursor: pointer;
  padding: 0.5rem 1rem;
  padding-bottom: 2rem;
  align-items: center;
  margin-top: 30px;
  &:hover {
    border-color: rgba(255, 104, 159, 0.75);
    color: white;
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 104, 159);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
function ForgotPassword(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [msg, setMsg] = useState();
  const [success, setSuccess] = useState(false);
  const user = useSelector((state) => state.auth.login?.currentUser);
  useEffect(() => {
    if (user) {
      Navigate("/");
    }
  });
  const handleEmailChange = (e) => {
    dispatch({ type: "CHANGE_EMAIL", payload: e.target.value });
  };
  const handleEmailFocus = () => {
    dispatch({ type: "FOCUS_EMAIL" });
  };

  const handleEmailBlur = () => {
    dispatch({ type: "BLUR_EMAIL" });
  };
  const [isLoading, setIsLoading] = useState(false);
  const handleForgotPassword = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    try {
      if (!state.email) {
        // Hiển thị thông báo lỗi cho người dùng
        setMsg("Vui lòng cung cấp email và password");
        return;
      }
      const obj = {
        email: state.email,
      };
      const errorMessage = await forgotPassword(obj);
      if (errorMessage) {
        // alert(errorMessage);
        setMsg(errorMessage);
      } else {
        setMsg("");
        setSuccess(true);
      }
    } catch (error) {
      setMsg(error.message);
    } finally {
      setIsLoading(false);
    }
  };
  const formLoginDiv =
    "flex flex-wrap text-[#818181] font-bold w-1/2 h-auto justify-center items-center rounded-3xl border-solid border-[#ffcbd94d] border bg-[#ffffffd9] shadow-md";
  return (
    <>
      {isLoading ? (
        <LoadingEdit />
      ) : (
        <div className="flex justify-center items-center w-full h-full my-8">
          {success ? (
            <div className="my-5 bg-[#b7e4c7] w-11/12 h-16 px-5 pt-5 py-4 text-center font-semibold rounded-md text-[#22577a]">
              Đã gửi email lấy lại mật khẩu thành công bạn vui lòng mở mail,
              kiểm tra và thực hiện theo hướng dẫn để đặt lại mật khẩu
            </div>
          ) : (
            <div className={formLoginDiv}>
              <h1 className="text-center text-[#FF689F] font-[600] py-0 px-[1em] pt-[1em] flex-grow text-4xl w-full ">
                Quên mật khẩu
              </h1>
              <section className="mx-12 flex-grow p-12 ">
                <form action="" onSubmit={handleForgotPassword}>
                  <div className="mb-4 relative">
                    <input
                      id="Input_Email"
                      type="email"
                      className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                      onFocus={handleEmailFocus}
                      onBlur={handleEmailBlur}
                      onChange={handleEmailChange}
                      value={state.email}
                    />
                    <label
                      htmlFor="Input_Email"
                      className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${
                        state.isEmailFocused || state.email
                          ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                          : ""
                      }`}
                    >
                      Email
                    </label>
                  </div>
                  <div className="text-center">
                    <BtnSubmit type="submit">Xác nhận</BtnSubmit>
                  </div>
                </form>
                <div className="text-start text-red-600 font-semibold mt-6 px-3">
                  {msg}
                </div>
              </section>
            </div>
          )}
        </div>
      )}
    </>
  );
}

export default ForgotPassword;
