package com.dev.server.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import lombok.*;

import java.util.Date;


@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "records")
public class Record {
    @Id
    private String _id;
    private String fullName;
    private String email;
    private Date date;
    private String sex;
    private String career;
    private String identityCardNumber;
    private String address;
    private String province;
    private String district;
    private String ward;
    private String phoneNumber;
    private String peoples;
    @CreatedDate
    private Date createdAt;
    @CreatedDate
    private Date updatedAt;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId account;

}
