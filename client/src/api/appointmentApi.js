
const decode = (id) => {
    return atob(id);
};
const appointmentApi = {
    getAllAppointment: async (axiosJWT, navigate, accessToken) => {
        try {
            const url = "http://localhost:5000/api/Appointment";
            return axiosJWT.get(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    getAppointmentById: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `http://localhost:5000/api/Report/Appointment/${decode(id)}`;
            return axiosJWT.get(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    createAppointment: async (axiosJWT, navigate, accessToken, data) => {
        try {
            const url = "http://localhost:5000/api/Report/Appointment";
            return axiosJWT.post(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    updateAppointment: async (axiosJWT, navigate, accessToken, id, data) => {
        try {
            const url = `http://localhost:5000/api/Report/Appointment/${decode(id)}`;
            return axiosJWT.put(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    deleteAppointment: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `http://localhost:5000/api/Report/Appointment/${id}`;
            return axiosJWT.delete(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
};
export default appointmentApi;
