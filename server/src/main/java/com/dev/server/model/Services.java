package com.dev.server.model;

import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "services")
public class Services {
    @Id
    private String _id;
    private String name;
    private String teaser;
    private String description;
    private String mainImage;
}
