package com.dev.server.model;

import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "specialists")
public class Specialist {
    @Id
    private String _id;
    private String specialistName;
    private String specialistDescription;
    private String specialistImg;
    private Number price;

    // getters and setters
}
