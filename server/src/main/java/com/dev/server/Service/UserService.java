package com.dev.server.Service;

import com.dev.server.model.User;
import com.dev.server.repository.AppointmentRepository;
import com.dev.server.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserService {

    private final UserRepository userRepository;

    // Tạo mới một người dùng
    public User createUser(User user) {
        try {
            user.setCreatedAt(new Date()); // Gán ngày tạo mới
            user.setUpdatedAt(new Date()); // Gán ngày cập nhật
            return userRepository.save(user);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo người dùng: " + e.getMessage(), e);
        }
    }

    // Cập nhật thông tin người dùng
    public User updateUser(String id, User user) {
        try {
            Optional<User> optionalUser = userRepository.findById(id);
            if (optionalUser.isPresent()) {
                User existingUser = optionalUser.get();
                existingUser.setFullName(user.getFullName());
                existingUser.setEmail(user.getEmail());
                existingUser.setDate(user.getDate());
                existingUser.setSex(user.getSex());
                existingUser.setCareer(user.getCareer());
                existingUser.setAddress(user.getAddress());
                existingUser.setPhoneNumber(user.getPhoneNumber());
                existingUser.setPeoples(user.getPeoples());
                existingUser.setUpdatedAt(new Date()); // Cập nhật ngày cập nhật
                return userRepository.save(existingUser);
            } else {
                throw new RuntimeException("Không tìm thấy người dùng với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi cập nhật người dùng: " + e.getMessage(), e);
        }
    }

    // Lấy thông tin người dùng bằng ID
    public User getUserById(String id) {
        try {
            Optional<User> optionalUser = userRepository.findById(id);
            return optionalUser.orElseThrow(() -> new RuntimeException("Không tìm thấy người dùng với id: " + id));
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy thông tin người dùng: " + e.getMessage(), e);
        }
    }

    // Lấy danh sách tất cả người dùng
    public List<User> getAllUsers() {
        try {
            return userRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy danh sách tất cả người dùng: " + e.getMessage(), e);
        }
    }

    // Xóa người dùng bằng ID
    public void deleteUser(String id) {
        try {
            userRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi xóa người dùng với id: " + id, e);
        }
    }
}
