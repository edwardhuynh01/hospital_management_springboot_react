import React, { useEffect, useState } from "react";
import './ServicePageStyle.css';
import Title from '../../../components/Title/Title';
import Service from '../../../api/servicesApi';
import { Link } from "react-router-dom";
import Loading from "../../../components/Loading/Loading";
const ServicePage = () => {
    const [serviceList, setServiceList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        setIsLoading(true);
        const fetchService = async () => {
            try {
                const data = await Service.getAllService();
                setServiceList(data);
            } catch (error) {
                console.error('Error fetching news:', error);
            }
            finally {
                setIsLoading(false);
            }
        }
        fetchService();
    }, []);
    const encode = (id) => {
        return btoa(id);
    }
    return (
        <div className="bg-slate-100">

            <div className="container mx-auto">
                <Title>DANH SÁCH DỊCH VỤ</Title>
                {isLoading ? (
                    <Loading />
                ) : (
                    <>
                        {
                            serviceList.length >= 1 && (
                                <div className="flex w-auto flex-col lg:flex-row m-10 rounded-xl justify-center overflow-hidden shadow-lg" id="col">
                                    <Link to={`/service/${encode(serviceList[0]?._id)}`} className="lg:w-1/2 w-full" id="img-main">
                                        <img className="mx-auto w-full h-full" src={`data:image/png;base64,${serviceList[0]?.mainImage}`} alt={serviceList[0]?.name} />
                                    </Link>
                                    <div className="lg:w-1/2 w-full bg-[#f8f9fa]">
                                        <div className=" text-start pt-[60px] pr-[30px] pb-[30px] pl-[45px] mb-0 lg:mb-10">
                                            <Link to={`/service/${encode(serviceList[0]?._id)}`} className="text-2xl font-bold leading-tight mb-6">{serviceList[0]?.name}</Link>
                                            <p className="text-md text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{serviceList[0]?.teaser}</p>
                                        </div>
                                        <div className="mb-10">
                                            <Link to={`/service/${encode(serviceList[0]?._id)}`} className="cursor-pointer mx-[45px] bg-[#FB6F92] py-2 px-6 rounded-md text-white
                             hover:text-[#FB6F92] hover:bg-white border border-solid hover:border-[#FB6F92] ">
                                                Xem chi tiết
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        <div className="grid md:grid-cols-3 grid-cols-1 gap-6 mx-10 pb-10">
                            {
                                serviceList.slice(1).map((serviceItem) => (
                                    <div div className="mx-auto bg-white shadow-lg rounded-lg overflow-hidden" id="col" key={serviceItem?._id}>
                                        <div className="flex flex-col items-center">
                                            <Link to={`/service/${encode(serviceItem?._id)}`} id="img-main">
                                                <img className="mx-auto w-full h-auto" src={`data:image/png;base64,${serviceItem?.mainImage}`} alt={serviceItem?.name} />
                                            </Link>
                                            <div className="text-start sm:text-left p-4" id="text-content">
                                                <Link to={`/service/${encode(serviceItem?._id)}`} className="text-xl font-bold leading-tight mb-2">{serviceItem?.name}</Link>
                                                <p className="text-sm text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{serviceItem?.teaser}</p>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}
export default ServicePage;