package com.dev.server.Controller;

import com.dev.server.Service.AccountService;
import com.dev.server.model.Services;
import com.dev.server.Service.ServicesService;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/Service")
public class ServicesApiController {
    @Autowired
    AccountService accountService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private ServicesService servicesService;

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping
    public ResponseEntity<List<Services>> getAllServices() {
        List<Services> services = servicesService.getAllServices();
        return ResponseEntity.ok(services);
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PostMapping
    public ResponseEntity<?> createService(@RequestBody Services service) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        return ResponseEntity.status(HttpStatus.CREATED).body(servicesService.createService(service));
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }

    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping("/{_id}")
    public ResponseEntity<Services> getServiceById(@PathVariable String _id) {
        Services service = servicesService.getServiceById(_id)
                .orElseThrow(() -> new RuntimeException("Service not found on :: " + _id));
        return ResponseEntity.ok().body(service);
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PutMapping("/{_id}")
    public ResponseEntity<?> updateService(@PathVariable String _id, @RequestBody Services service) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        Services serviceUpdated = servicesService.updateService(_id, service);
                        return ResponseEntity.ok().body(serviceUpdated);
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }

    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @DeleteMapping("/{_id}")
    public ResponseEntity<?> deleteService(@PathVariable String _id) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        servicesService.deleteService(_id);
                        return ResponseEntity.noContent().build();
                    }
                }
            }

        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }
}
