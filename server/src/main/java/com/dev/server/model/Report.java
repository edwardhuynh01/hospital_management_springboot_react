package com.dev.server.model;

import com.dev.server.Serializer.ObjectIdListSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Date;
import java.util.List;

import lombok.*;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "reports")
public class Report {
    @Id
    private String _id;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId account;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId record;
    @JsonSerialize(using = ObjectIdListSerializer.class)
    private List<ObjectId> appointment;
    @CreatedDate
    private Date createdAt;
    @CreatedDate
    private Date updatedAt;
    private double amount;
    private boolean isPayment;
    private String paymentId;
}
