import React, { useState, useReducer, useEffect } from "react";
import styled from "styled-components";
import Doctor from "./Doctor";
import { IoSearchOutline, IoCaretDown } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import doctorApi from "../../api/doctorApi";
import { chooseDoctor } from "../../redux/appointmentSlice";
import specialistApi from "../../api/specialistApi";
import LoadingEdit from "../Loading/LoadingEdit";
const Container = styled.div`
  border-bottom: 2px solid #ffb3c6;
  border-left: 2px solid #ffb3c6;
  border-right: 2px solid #ffb3c6;
`;
const ClearButton = styled.button`
  position: absolute;
  /* top: 50%; */
  right: 12%;
  transform: translateY(-10%);
  background: none;
  border: none;
  cursor: pointer;
`;
const InputSearch = styled.div`
  transition: all ease-in-out 0.3s;
`;
const ContainerList = styled.div`
  max-height: 300px;
  overflow-y: auto;
`;
const StyledList = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
`;
const ListItem = styled.li`
  border-bottom: 1px solid #ccc;
`;
const initialState = {
  HHHVbtnClick: false,
  HHHVvalue: "",
  CKbtnClick: false,
  CKvalue: "",
  GTbtnClick: false,
  GTvalue: "",
};
const DropdownItem = styled.div`
  transition: all ease-in-out 0.3s;
`;
const Dropdown = styled.div`
  transition: all ease-in-out 0.3s;
`;
function reducer(state, action) {
  switch (action.type) {
    case "CLICK_HHHV_BTN":
      return {
        ...state,
        HHHVbtnClick: !state.HHHVbtnClick,
        GTbtnClick: false,
        CKbtnClick: false,
      };
    case "ITEM_HHHV_CLICK":
      return { ...state, HHHVbtnClick: false, HHHVvalue: action.payload };
    case "CLICK_CK_BTN":
      return {
        ...state,
        CKbtnClick: !state.CKbtnClick,
        HHHVbtnClick: false,
        GTbtnClick: false,
      };
    case "ITEM_CK_CLICK":
      return { ...state, CKbtnClick: false, CKvalue: action.payload };
    case "CLICK_GT_BTN":
      return {
        ...state,
        GTbtnClick: !state.GTbtnClick,
        HHHVbtnClick: false,
        CKbtnClick: false,
      };
    case "ITEM_GT_CLICK":
      return { ...state, GTbtnClick: false, GTvalue: action.payload };
    default:
      return state;
  }
}
function DoctorsList(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [value, setValue] = useState("");
  const [onFocus, setOnFocus] = useState(false);
  const [valueCK, setValueCK] = useState("");
  const [onFocusCK, setOnFocusCK] = useState(false);
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  const ClearInput = () => {
    setValue("");
  };
  const handleOnFocus = () => {
    setOnFocus(true);
  };
  const handleOnBlur = () => {
    setOnFocus(false);
  };
  const handleChangeCK = (event) => {
    setValueCK(event.target.value);
  };
  const ClearInputCK = () => {
    setValueCK("");
  };
  const handleOnFocusCK = () => {
    setOnFocusCK(true);
  };
  const handleOnBlurCK = () => {
    setOnFocusCK(false);
  };
  const handleHHHVClick = () => {
    dispatch({ type: "CLICK_HHHV_BTN" });
  };
  const handleItemHHHVClick = (event) => {
    dispatch({ type: "ITEM_HHHV_CLICK", payload: event });
  };
  const handleCKClick = () => {
    dispatch({ type: "CLICK_CK_BTN" });
  };
  const handleItemCKClick = (event) => {
    dispatch({ type: "ITEM_CK_CLICK", payload: event });
  };
  const handleGTClick = () => {
    dispatch({ type: "CLICK_GT_BTN" });
  };
  const handleItemGTClick = (event) => {
    dispatch({ type: "ITEM_GT_CLICK", payload: event });
  };

  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const specialistId = useSelector(
    (state) => state.appointment?.appointment?.specialistId
  );
  const [docterList, setDocterList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchDocter = async () => {
        try {
          const data = await doctorApi.getAllDoctor(
            axiosJWT,
            navigated,
            accessToken
          );
          const dataDocterBySpecId = data.data.filter(
            (x) => x.specialist === specialistId
          );
          setDocterList(dataDocterBySpecId);
        } catch (error) {

        } finally {
          setIsLoading(false);
        }
      };
      fetchDocter();
    } catch (error) { }
  }, []);
  const encode = (id) => {
    return btoa(id);
  };
  const [specialist, setSpecialist] = useState();
  useEffect(() => {
    const fetchSpec = async () => {
      try {
        const response = await specialistApi.getSpecialistById(
          encode(specialistId)
        );
        setSpecialist(response);
      } catch (error) {
        console.error("Error fetching spec:", error);
      }
    };
    fetchSpec();
  }, []);
  const [filteredDoctorList, setFilteredDoctorList] = useState([]);
  useEffect(() => {
    const filteredDoctorList = docterList.filter((x) =>
      x.name.toLowerCase().trim().includes(value.toLowerCase().trim())
    );
    setFilteredDoctorList(filteredDoctorList);
  }, [value, docterList]);
  const checkSearch =
    Array.isArray(filteredDoctorList) && filteredDoctorList.length > 0;
  const saveDoctorId = async (DrId) => {
    DISPATCH(chooseDoctor({ doctorId: DrId }));
  };
  const formatNumber = (number) => {
    if (typeof number !== "number" || isNaN(number)) {
      return "Invalid number"; // hoặc giá trị mặc định khác
    }
    return number.toLocaleString("de-DE"); // Định dạng theo chuẩn của Đức
  };
  return (
    <div className="w-full flex flex-wrap">
      <div className="w-full bg-[#f26a8d] text-white px-5 py-2 rounded-t-md font-semibold">
        <h2>Vui lòng chọn bác sĩ</h2>
      </div>
      <Container className="w-full p-4">
        <InputSearch
          className={`w-full flex items-center rounded-md border border-solid h-11 ${onFocus ? "border-[#ffb3c6]" : ""
            }`}
        >
          <label className="px-2" htmlFor="search">
            <IoSearchOutline></IoSearchOutline>
          </label>
          <input
            id="search"
            value={value}
            onChange={handleChange}
            onFocus={handleOnFocus}
            onBlur={handleOnBlur}
            className="w-full outline-none"
            placeholder="Tìm nhanh bác sĩ"
            type="text"
          />
          <ClearButton
            className={`${value === "" ? "hidden" : ""}`}
            onClick={ClearInput}
          >
            <span aria-hidden="true">×</span>
          </ClearButton>
        </InputSearch>
        <ContainerList>
          {
            isLoading ? (
              <LoadingEdit />
            ) : (
              <StyledList>
                {checkSearch &&
                  filteredDoctorList?.map((docterItem) => (
                    <ListItem key={docterItem._id}>
                      <Doctor
                        drId={docterItem._id}
                        name={docterItem.name}
                        dateOfWeek={docterItem.dateOfWeek}
                        sex={docterItem.sex}
                        price={formatNumber(specialist?.price)}
                        chooseDoctor={() => saveDoctorId(docterItem._id)}
                        specialistName={specialist?.specialistName}
                      ></Doctor>
                    </ListItem>
                  ))}
                {!checkSearch && (
                  <h2 className="mt-1 text-red-600">Không tồn tại bác sĩ này!</h2>
                )}
              </StyledList>
            )
          }
        </ContainerList>
      </Container>
    </div>
  );
}

export default DoctorsList;
