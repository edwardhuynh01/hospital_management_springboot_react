import React, { useEffect, useState } from "react";
import { IoCall } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createAxios } from "../../../createInstance";
import { loginSuccess } from "../../../redux/authSlice";
import appointmentApi from "../../../api/appointmentApi";
import specialistApi from "../../../api/specialistApi";
import doctorApi from "../../../api/doctorApi";
import recordApi from "../../../api/recordApi";

const NotificationListDetail = (props) => {
  const { check, isPayment, appointmentId } = props;
  const user = useSelector((state) => state.auth.login?.currentUser);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const accessToken = user?.accessToken;
  const axiosJWT = createAxios(user, dispatch, loginSuccess);
  const [appointment, setAppointment] = useState();
  const [specialist, setSpecialist] = useState(null);
  const [doctor, setDoctor] = useState(null);
  const [record, setRecord] = useState(null);

  const encode = (id) => {
    return btoa(id);
  };
  const decode = (id) => {
    return atob(id);
  };
  //   console.log(appointmentId);
  useEffect(() => {
    const fetchData = async () => {
      try {
        if (!accessToken || typeof accessToken !== "string") {
          throw new Error("Token không hợp lệ");
        }

        const appointmentData = await appointmentApi.getAppointmentById(
          axiosJWT,
          navigate,
          accessToken,
          encode(appointmentId)
        );
        console.log(appointmentData);
        setAppointment(appointmentData.data);

        if (appointmentData.data?.specialistId) {
          const specialistData = await specialistApi.getSpecialistById(
            encode(appointmentData.data.specialistId)
          );
          setSpecialist(specialistData);
        }

        if (appointmentData.data?.doctorId) {
          const doctorData = await doctorApi.getDoctorById(
            axiosJWT,
            navigate,
            accessToken,
            appointmentData.data.doctorId
          );
          setDoctor(doctorData.data);
        }
        if (appointmentData.data?.recordId) {
          const recordData = await recordApi.getRecordById(
            axiosJWT,
            navigate,
            accessToken,
            encode(appointmentData.data.recordId)
          );
          setRecord(recordData.data);
        }
      } catch (error) {
        console.error("Error fetching data: ", error);
      }
    };
    fetchData();
  }, []);
  const formatDate = (date) => {
    if (!date) return "Invalid Date"; // Trả về chuỗi thông báo nếu không có giá trị ngày
    const dateObj = new Date(date);
    if (isNaN(dateObj.getTime())) return "Invalid Date"; // Kiểm tra xem dateObj có hợp lệ không
    return dateObj.toISOString().split("T")[0];
  };
  const formatNumber = (number) => {
    if (typeof number !== "number" || isNaN(number)) {
      return "Invalid number"; // hoặc giá trị mặc định khác
    }
    return number.toLocaleString("de-DE"); // Định dạng theo chuẩn của Đức
  };
  return (
    <div className="flex justify-center">
      <div
        className={`flex justify-center flex-col items-center ${
          check ? "w-[80%]" : "w-[50%]"
        } bg-white rounded-3xl overflow-hidden my-6`}
      >
        <div className="mt-4">
          <p className="text-xl text-center font-semibold ">
            Phiếu khám bệnh trực tuyến
          </p>
          <p className="text-center text-3xl font-bold text-[#ff8fab] font-sans mb-4">
            Sinh Tố Dâu
          </p>
          <p className="text-center font-semibold">
            Bệnh viện đa khoa Sinh Tố Dâu
          </p>
        </div>
        <div className="m-4">
          <div className="p-4 border border-solid border-[#ffd7ba] rounded-2xl bg-gray-100">
            <p>
              Trong quá trình thanh toán gặp khó khăn, vui lòng gọi đến Tổng đài
              CSKH để được hỗ trợ.
            </p>
            <a
              className="flex justify-center gap-2 items-center w-auto mt-3 border border-solid border-[#ff8fab] 
                                        rounded-2xl mx-8 py-2 text-[#ff8fab] hover:bg-[#ff8fab] hover:text-white
                                        transition duration-150 ease-in delay-100"
              href="/"
            >
              <IoCall />
              <p>Tổng đài 0357192122</p>
            </a>
          </div>
        </div>
        <div className="w-full my-4 leading-7">
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Mã phiếu:</p>
            <p>{appointment?._id}</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Mã Dịch vụ:</p>
            <p>Khám dịch vụ</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Hình thức khám:</p>
            <p>Không có BHYT</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Chuyên khoa:</p>
            <p>{specialist?.specialistName}</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Bác sĩ:</p>
            <p>{doctor?.name}</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Ngày khám:</p>
            <p>
              {appointment?.day +
                "/" +
                appointment?.month +
                "/" +
                appointment?.year}
            </p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Giờ khám:</p>
            <p>{appointment?.timeStart + "-" + appointment?.timeEnd}</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Phí khám:</p>
            <p>{formatNumber(specialist?.price)}đ</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Thanh Toán:</p>
            <p>{isPayment ? "Đã thanh toán" : "Chưa thanh toán"}</p>
          </div>
        </div>
        <div className="w-full py-4 border-t-4 border-dashed border-slate-100">
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Bệnh nhân:</p>
            <p className="text-md font-semibold">{record?.fullName}</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Ngày sinh:</p>
            <p className="font-semibold text-md">{formatDate(record?.date)}</p>
          </div>
          <div className="flex justify-between mx-4 text-sm leading-7">
            <p>Mã bệnh nhân:</p>
            <p className="font-semibold text-md">{record?._id}</p>
          </div>
        </div>
        <div className="flex items-center justify-center gap-1 py-10 border-t-4 border-dashed border-slate-100 w-full">
          <p className="text-sm">Được phát triển bởi</p>
          <p className="text-lg font-semibold font-mono text-[#ff8fab]">
            Sinh Tố Dâu
          </p>
        </div>
      </div>
    </div>
  );
};

export default NotificationListDetail;
