package com.dev.server.Service;

import com.dev.server.model.Specialist;
import com.dev.server.repository.SpecialistRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class SpecialistService {
    private final SpecialistRepository specialistRepository;

    public Specialist createSpecialist(Specialist specialist) {
        try {
            return specialistRepository.save(specialist);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo chuyên gia: " + e.getMessage(), e);
        }
    }

    public List<Specialist> getAllSpecialists() {
        try {
            return specialistRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy danh sách tất cả chuyên gia: " + e.getMessage(), e);
        }
    }

    public Optional<Specialist> getSpecialistById(String id) {
        try {
            return specialistRepository.findById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy thông tin chuyên gia với id: " + id, e);
        }
    }

    public Specialist updateSpecialist(String id, Specialist specialistDetails) {
        try {
            Optional<Specialist> optionalSpecialist = specialistRepository.findById(id);

            if (optionalSpecialist.isPresent()) {
                Specialist existingSpecialist = optionalSpecialist.get();
                existingSpecialist.setSpecialistName(specialistDetails.getSpecialistName());
                existingSpecialist.setSpecialistDescription(specialistDetails.getSpecialistDescription());
                existingSpecialist.setSpecialistImg(specialistDetails.getSpecialistImg());
                existingSpecialist.setPrice(specialistDetails.getPrice());
                return specialistRepository.save(existingSpecialist);
            } else {
                throw new RuntimeException("Không tìm thấy chuyên gia với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi cập nhật chuyên gia: " + e.getMessage(), e);
        }
    }

    public boolean deleteSpecialist(String id) {
        try {
            Optional<Specialist> optionalSpecialist = specialistRepository.findById(id);

            if (optionalSpecialist.isPresent()) {
                specialistRepository.deleteById(id);
                return true;
            } else {
                throw new RuntimeException("Không tìm thấy chuyên gia với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi xóa chuyên gia với id: " + id, e);
        }
    }
}
