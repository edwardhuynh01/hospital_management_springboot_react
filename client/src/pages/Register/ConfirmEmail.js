import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { confirmEmail } from "../../redux/apiRequest";

function ConfirmEmail(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const navigate = useNavigate();
  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const token = query.get("token");
  const email = query.get("email");
  const [msg, setMsg] = useState();
  const [success, setSuccess] = useState(false);
  useEffect(() => {
    const test = async () => {
      try {
        if (user) {
          navigate("/");
        } else {
          const err = await confirmEmail(token, email);
          if (err) {
            setMsg(err);
            setSuccess(false);
          } else {
            setMsg("Bạn đã xác nhận tài khoản thành công");
            setSuccess(true);
          }
        }
      } catch (err) {
        setMsg(err.message);
        setSuccess(false);
      }
    };
    test();
  });
  return (
    <div className="flex justify-center w-full">
      <div
        className={`my-5 ${
          success ? "bg-[#b7e4c7]" : "bg-[#ffc2d1]"
        } w-11/12 h-16 px-5 pt-5 py-4 text-center font-semibold rounded-md ${
          success ? "text-[#22577a]" : "text-[#fb6f92]"
        } `}
      >
        {msg}
      </div>
    </div>
  );
}

export default ConfirmEmail;
