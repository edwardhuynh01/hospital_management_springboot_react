import React from "react";
import { useDispatch, useSelector } from "react-redux";
import AdminLayout from "../../components/AdminLayout/AdminLayout";
const AdminPage = () => {
    return (
        <AdminLayout />
    )
};

export default AdminPage