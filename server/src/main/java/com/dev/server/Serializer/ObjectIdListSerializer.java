package com.dev.server.Serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.util.List;

public class ObjectIdListSerializer extends StdSerializer<List<ObjectId>> {

    public ObjectIdListSerializer() {
        this(null);
    }

    public ObjectIdListSerializer(Class<List<ObjectId>> t) {
        super(t);
    }

    @Override
    public void serialize(List<ObjectId> value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartArray();
        for (ObjectId objectId : value) {
            gen.writeString(objectId.toString());
        }
        gen.writeEndArray();
    }
}
