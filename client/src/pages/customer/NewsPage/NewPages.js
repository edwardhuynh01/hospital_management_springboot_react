import React, { useEffect, useState } from 'react';
import '../servicePage/ServicePageStyle.css';
import Title from '../../../components/Title/Title';
import newsApi from '../../../api/newsApi';
import { Link } from 'react-router-dom';
import Loading from '../../../components/Loading/Loading';
const NewPages = () => {
    const [newsList, setNewsList] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        setIsLoading(true);
        const fetchNews = async () => {
            try {
                const response = await newsApi.getAllNews();
                setNewsList(response);
            } catch (error) {
                console.error('Error fetching news:', error);
            }
            finally {
                setIsLoading(false);
            }
        };

        fetchNews();
    }, []);
    const encode = (id) => {
        return btoa(id);
    }
    return (
        <div className='bg-slate-100'>
            <div className="container mx-auto">
                <Title>DANH SÁCH TIN TỨC</Title>
                {isLoading ? (
                    <Loading />
                ) : (
                    <>
                        {
                            newsList.length >= 1 && (
                                <div className="flex w-auto flex-col lg:flex-row m-10 rounded-xl justify-center overflow-hidden shadow-lg" id="col">
                                    <Link to={`/news/${encode(newsList[0]._id)}`} className="lg:w-1/2 w-full" id="img-main">
                                        <img className="mx-auto w-full h-full" src={`data:image/png;base64,${newsList[0]?.mainImage}`} alt={newsList[0].title} />
                                    </Link>
                                    <div className="lg:w-1/2 w-full bg-[#f8f9fa]">
                                        <div className=" text-start pt-[60px] pr-[30px] pb-[30px] pl-[45px] mb-0 lg:mb-10">
                                            <Link to={`/news/${encode(newsList[0]._id)}`} className="text-2xl font-bold leading-tight mb-6">{newsList[0].title}</Link>
                                            <p className="text-md text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{newsList[0].teaser}</p>
                                        </div>
                                        <div className="mb-10">
                                            <Link to={`/news/${encode(newsList[0]._id)}`} className="cursor-pointer mx-[45px] bg-[#FB6F92] py-2 px-6 rounded-md text-white
                                 hover:text-[#FB6F92] hover:bg-white border border-solid hover:border-[#FB6F92] ">
                                                Xem chi tiết
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        <div className="grid md:grid-cols-3 grid-cols-1 gap-6 mx-10 pb-10 ">
                            {
                                newsList.slice(1).map((newsItem) => (
                                    <div className="mx-auto bg-white shadow-lg rounded-lg overflow-hidden" id="col" key={newsItem?.id}>
                                        <div className="flex flex-col items-center">
                                            <Link to={`/news/${encode(newsItem?._id)}`} id="img-main">
                                                <img className="mx-auto w-full h-auto" src={`data:image/png;base64,${newsItem?.mainImage}`} alt={newsItem?.title} />
                                            </Link>
                                            <div className="text-start sm:text-left p-4 h-full w-full" id="text-content">
                                                <Link to={`/news/${encode(newsItem?._id)}`} className="text-xl font-bold leading-tight mb-2">{newsItem?.title}</Link>
                                                <p className="text-sm text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{newsItem?.teaser}</p>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </>
                )}
            </div>
        </div>
    );
};

export default NewPages;