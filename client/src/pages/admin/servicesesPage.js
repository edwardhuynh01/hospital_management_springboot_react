import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from 'react-router-dom'
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import servicesApi from '../../api/servicesApi'
import { confirmAlert } from "react-confirm-alert";
import { toast } from "react-toastify";
import AddServiceForm from "../../components/AdminAddPopup/AddServiceForm";
import EditServiceForm from "../../components/AdminAddPopup/EditServiceForm";
import CountUp from 'react-countup';
import LoadingEdit from "../../components/Loading/LoadingEdit";

const ServicesesPage = () => {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [serviceList, setServiceList] = useState();
  const [isDeleted, setIsDeleted] = useState(false);
  const [isModalAddOpen, setIsModalAddOpen] = useState(false);
  const [isModalEditOpen, setIsModalEditOpen] = useState(false);
  const [id, setId] = useState(false);
  const [search, setSearch] = useState("");
  const [confirmedSearch, setConfirmedSearch] = useState("");

  const handleFilter = () => {
    setConfirmedSearch(search);
    setSearch('');
  };
  const handleAddService = async (serviceData) => {
    try {
      const createdService = await servicesApi.createService(axiosJWT, navigated, accessToken, serviceData);
      if (createdService) {
        setIsDeleted(!isDeleted); // Refresh news list
        setIsModalAddOpen(false);
        toast("Thêm dịch vụ thành công!");
      } else {
        setIsModalAddOpen(false);
        toast.error("Đã xảy ra lỗi khi thêm dịch vụ.");
      }
    } catch (error) {
      setIsModalAddOpen(false);
      toast.error("Đã xảy ra lỗi khi thêm dịch vụ.");
      console.error("Error adding service:", error);
    }
  };
  const encode = (id) => {
    return btoa(id);
  }
  const handleEditService = async (serviceData) => {
    try {
      const createdService = await servicesApi.updateService(axiosJWT, navigated, accessToken, encode(id), serviceData);
      if (createdService) {
        setIsDeleted(!isDeleted); // Refresh news list
        setIsModalEditOpen(false);
        toast("Sửa dịch vụ thành công!");
      } else {
        setIsModalEditOpen(false);
        toast.error("Đã xảy ra lỗi khi sửa dịch vụ.");
      }
    } catch (error) {
      setIsModalEditOpen(false);
      toast.error("Đã xảy ra lỗi khi sửa dịch vụ.");
      console.error("Error adding service:", error);
    }
  };
  // const deleteFile = async (imageLink) => {
  //     try {
  //         const fileId = imageLink.split("/")[4];
  //         const response = await fetch(
  //             `https://script.google.com/macros/s/AKfycbxA2nd3dNnMh-UkBFJ4nGTE6mqosePHJp30gX2PILSea90yqgKaOT4joqP7zFlyTyU/exec?fileId=${fileId}`,
  //             {
  //                 method: "GET",
  //             }
  //         );
  //         const data = await response.json();
  //         // formData.image = data.link;
  //         console.log(data);
  //         // console.log(formData);
  //     } catch (error) {
  //         console.log("có lỗi xảy ra vui lòng thử lại", error);
  //     }
  // };
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {

          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchService = async () => {
            try {
              const data = await servicesApi.getAllService();
              setServiceList(data);
            } catch (error) {

            }
            finally {
              setIsLoading(false);
            }
          };
          fetchService();
        }
      }
    } catch (error) { }
  }, [isDeleted]);
  const handleDelete = (id) => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className="bg-slate-100 p-6 rounded-lg shadow-xl">
          <h1 className="text-2xl text-[#fb6f92] font-semibold mb-4 text-start h-full w-full">
            Xác nhận xóa
          </h1>
          <p className="mb-4">Bạn có chắc chắn muốn xóa dịch vụ này?</p>
          <div className="flex justify-center gap-4">
            <button
              onClick={async () => {
                try {
                  const deleteData = await servicesApi.deleteService(
                    axiosJWT,
                    navigated,
                    accessToken,
                    id
                  );
                  if (deleteData) {
                    setIsDeleted(!isDeleted);
                    // if (deleteData?.data?.mainImage) {
                    //     deleteFile(deleteData.data.mainImage);
                    // }
                    toast("Xóa dịch vụ thành công!");
                  } else {
                    console.log(id)
                    toast.error("Đã xảy ra lỗi khi xóa dịch vụ.");
                  }
                } catch (error) {
                  toast.error("Đã xảy ra lỗi khi xóa dịch vụ.");
                  console.error("Error deleting record:", error);
                }
                onClose();
              }}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-red-600 duration-300 bg-red-400 text-white px-8 py-2 rounded-lg mr-2"
            >
              Có
            </button>
            <button
              onClick={onClose}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-gray-400 duration-300 bg-gray-300 text-black px-4 py-2 rounded-lg"
            >
              Không
            </button>
          </div>
        </div>
      ),
    });
  };
  return (
    <main class="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
      <div className="p-6 h-full">
        <div class="grid grid-cols-1">
          <div class="bg-white border border-gray-100 shadow-md shadow-black/4 p-9 rounded-md ">
            <div class="flex justify-between mb-4 items-start">
              <div class="font-medium text-gray-500">Số Dịch Vụ: <CountUp end={serviceList?.length} duration={2} /></div>
            </div>
            <div class="flex items-center mb-4 order-tab gap-4">
              <button type="button" data-tab="order" data-tab-page="active" class="shadow-md shadow-black/4 bg-sky-400 text-sm font-medium text-white py-2 px-10 rounded-tl-md rounded-bl-md rounded-tr-md rounded-br-md hover:bg-sky-500 active" onClick={() => setIsModalAddOpen(true)}>Thêm</button>
              {/* <button type="button" data-tab="order" data-tab-page="completed" class=" bg-gray-100 text-sm font-medium text-gray-400 py-2 px-4 hover:text-gray-600">Completed</button>
                            <button type="button" data-tab="order" data-tab-page="canceled" class="bg-gray-100 text-sm font-medium text-gray-400 py-2 px-4 rounded-tr-md rounded-br-md hover:text-gray-600">Canceled</button> */}
              <input type="text" value={search} onChange={(e) => setSearch(e.target.value)} class="shadow-md shadow-black/4 ml-3 py-2 pr-4 pl-10 bg-gray-50 w-full outline-none border border-gray-100 rounded-md text-sm focus:border-blue-500" placeholder="Tìm kiếm..."></input>
              <button type="button" onClick={handleFilter} data-tab="order" data-tab-page="active" class="shadow-md shadow-black/4 bg-blue-400 text-sm font-medium text-white py-2 px-6 rounded-tl-md rounded-bl-md rounded-tr-md  rounded-br-md hover:bg-blue-500 active">Lọc</button>
            </div>
            <div class="overflow-x-auto">
              <div class="overflow-y-auto max-h-[508px]">
                <table class={`w-full min-w-[800px] ${isLoading ? 'flex flex-col justify-center items-center' : ''}`} data-tab-for="order" data-page="active">
                  <thead className="w-full">
                    <tr>
                      <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left rounded-tl-md rounded-bl-md">Tên Dịch Vụ</th>
                      <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left">Giới Thiệu</th>
                      <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left">Nội dung</th>
                      <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-5 bg-gray-100 text-left rounded-tr-md rounded-br-md">Chức Năng</th>
                    </tr>
                  </thead>
                  {
                    isLoading ? (
                      <LoadingEdit />
                    ) : (
                      <tbody>
                        {
                          serviceList?.filter((serviceItem) => {
                            return confirmedSearch.toLowerCase() === ''
                              ? true
                              : serviceItem.name.toLowerCase().includes(confirmedSearch.toLowerCase())
                          }).map((serviceItem) => (
                            <>
                              <tr key={serviceItem?._id}>
                                <td class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                  <div class="flex items-center">
                                    {/* <img src="https://placehold.co/32x32" alt="" class="w-8 h-8 rounded object-cover block"></img> */}
                                    <img src={`data:image/png;base64,${serviceItem?.mainImage}`} alt={serviceItem?.name} class="w-8 h-8 rounded object-cover block"></img>
                                    <a href="#" class="text-gray-600 text-sm font-medium hover:text-blue-500 ml-2 truncate">{serviceItem?.name}</a>
                                  </div>
                                </td>
                                <td class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                  <span class="text-[13px] font-medium text-gray-400">{serviceItem?.teaser}</span>
                                </td>
                                <td class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                  <span class="text-[13px] font-medium text-gray-400">{serviceItem?.description}</span>
                                </td>
                                <td class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                  <div class="flex items-center gap-1 justify-right">
                                    <div class="hover:bg-pink-100 rounded">
                                      <button type="button" class="px-2 mt-1" onClick={() => {
                                        setIsModalEditOpen(true);
                                        setId(serviceItem?._id);
                                      }}>
                                        <box-icon name='edit-alt' ></box-icon>
                                      </button>
                                    </div>
                                    <div class="hidden sm:block mx-2 lg:mx-px w-px h-5 bg-gray-200 dark:bg-gray-900"></div>
                                    <div class="hover:bg-pink-100 rounded" onClick={() => handleDelete(serviceItem?._id)}>
                                      <div class="px-2 mt-1">
                                        <box-icon name='trash' ></box-icon>
                                      </div>
                                    </div>
                                  </div>
                                </td>
                              </tr>
                            </>
                          ))
                        }
                      </tbody>
                    )
                  }
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <AddServiceForm
        isOpen={isModalAddOpen}
        onRequestClose={() => setIsModalAddOpen(false)}
        onSubmit={handleAddService}
      />
      <EditServiceForm
        isOpen={isModalEditOpen}
        onRequestClose={() => setIsModalEditOpen(false)}
        onSubmit={handleEditService}
        isId={id}
      />
    </main>
  )
}

export default ServicesesPage