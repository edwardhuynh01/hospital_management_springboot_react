import React from "react";
import {
  IoPersonCircleOutline,
  IoMaleFemaleOutline,
  IoFitnessOutline,
  IoCalendarClearOutline,
  IoCashOutline,
} from "react-icons/io5";
import { Link } from "react-router-dom";
import styled from "styled-components";
const Container = styled.div`
  transition: all ease-in 0.2s;
  border: 2px solid #ffe5ec;
  &:hover {
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 229, 236);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
function Doctor(props) {
  const { drId, name, dateOfWeek, sex, specialistName,price } = props;
  return (
    <Container className="w-full h-full py-4 px-2 cursor-pointer shadow-md my-3">
      <Link to={`/ChooseMedicalSchedule`} onClick={props.chooseDoctor}>
        <div className="w-11/12 flex justify-start mb-3">
          <div className="flex justify-start items-center text-[#fb6f92] font-medium">
            <div className="mx-2 ">
              <IoPersonCircleOutline></IoPersonCircleOutline>
            </div>
            <h3>{name}</h3>
          </div>
        </div>
        <div className="flex flex-wrap w-11/12">
          <div className="flex w-full justify-start items-center">
            <div className="w-auto pr-1 text-[#495057]">
              <div className="flex justify-start items-center">
                <div className="mx-2 ">
                  <IoMaleFemaleOutline></IoMaleFemaleOutline>
                </div>
                <h3 className="font-normal">Giới tính:</h3>
              </div>
            </div>
            <div className="text-[#495057] text-sm w-auto pl-1">{sex}</div>
          </div>
          <div className="flex w-full justify-start items-center">
            <div className="w-auto pr-1 text-[#495057]">
              <div className="flex justify-start items-center">
                <div className="mx-2 ">
                  <IoFitnessOutline></IoFitnessOutline>
                </div>
                <h3 className="font-normal">Chuyên khoa:</h3>
              </div>
            </div>
            <div className="text-[#495057] text-sm w-auto pl-1">
              {specialistName}
            </div>
          </div>
          <div className="flex w-full justify-start items-center">
            <div className="w-auto pr-1 text-[#495057]">
              <div className="flex justify-start items-center">
                <div className="mx-2 ">
                  <IoCalendarClearOutline></IoCalendarClearOutline>
                </div>
                <h3 className="font-normal">Lịch khám:</h3>
              </div>
            </div>
            <div className="text-[#495057] text-sm w-w-auto pl-1">{dateOfWeek === 1 ? `Chủ nhật` : `Thứ ${dateOfWeek}`}</div>
          </div>
          <div className="flex w-full justify-start items-center">
            <div className="w-auto pr-1 text-[#495057]">
              <div className="flex justify-start items-center">
                <div className="mx-2 ">
                  <IoCashOutline></IoCashOutline>
                </div>
                <h3 className="font-normal">GIá khám:</h3>
              </div>
            </div>
            <div className="text-[#495057] text-sm w-auto pl-1">{price}đ</div>
          </div>
        </div>
      </Link>
    </Container>
  );
}

export default Doctor;
