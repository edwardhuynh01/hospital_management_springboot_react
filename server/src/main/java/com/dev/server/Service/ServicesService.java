package com.dev.server.Service;

import com.dev.server.model.Services;
import com.dev.server.repository.ServiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class ServicesService {
    private final ServiceRepository servicesRepository;

    public Services createService(Services service) {
        try {
            return servicesRepository.save(service);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo dịch vụ: " + e.getMessage(), e);
        }
    }

    public List<Services> getAllServices() {
        try {
            return servicesRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy danh sách tất cả dịch vụ: " + e.getMessage(), e);
        }
    }

    public Optional<Services> getServiceById(String id) {
        try {
            return servicesRepository.findById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy thông tin dịch vụ với id: " + id, e);
        }
    }

    public Services updateService(String id, Services serviceDetails) {
        try {
            Optional<Services> optionalService = servicesRepository.findById(id);

            if (optionalService.isPresent()) {
                Services existingService = optionalService.get();
                existingService.setName(serviceDetails.getName());
                existingService.setTeaser(serviceDetails.getTeaser());
                existingService.setDescription(serviceDetails.getDescription());
                existingService.setMainImage(serviceDetails.getMainImage());
                return servicesRepository.save(existingService);
            } else {
                throw new RuntimeException("Không tìm thấy dịch vụ với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi cập nhật dịch vụ: " + e.getMessage(), e);
        }
    }

    public boolean deleteService(String id) {
        try {
            Optional<Services> optionalService = servicesRepository.findById(id);

            if (optionalService.isPresent()) {
                servicesRepository.deleteById(id);
                return true;
            } else {
                throw new RuntimeException("Không tìm thấy dịch vụ với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi xóa dịch vụ với id: " + id, e);
        }
    }
}
