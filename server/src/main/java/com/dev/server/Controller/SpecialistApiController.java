package com.dev.server.Controller;

import com.dev.server.Service.AccountService;
import com.dev.server.model.Specialist;
import com.dev.server.Service.SpecialistService;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/Specialist")
public class SpecialistApiController {
    @Autowired
    AccountService accountService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private SpecialistService specialistService;

    @GetMapping
    public ResponseEntity<?> getAllSpecialists() {
        List<Specialist> specialists = specialistService.getAllSpecialists();
        return ResponseEntity.ok(specialists);
    }

    @PostMapping
    public ResponseEntity<?> createSpecialist(@RequestBody Specialist specialist) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        return ResponseEntity.status(HttpStatus.CREATED).body(specialistService.createSpecialist(specialist));
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }

    }

    @GetMapping("/{_id}")
    public ResponseEntity<Specialist> getSpecialistById(@PathVariable String _id) {
        Specialist specialist = specialistService.getSpecialistById(_id)
                .orElseThrow(() -> new RuntimeException("Specialist not found on :: " + _id));
        return ResponseEntity.ok().body(specialist);
    }

    @PutMapping("/{_id}")
    public ResponseEntity<?> updateSpecialist(@PathVariable String _id, @RequestBody Specialist specialist) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        Specialist specialistUpdated = specialistService.updateSpecialist(_id, specialist);
                        return ResponseEntity.ok().body(specialistUpdated);
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }

    }

    @DeleteMapping("/{_id}")
    public ResponseEntity<?> deleteSpecialist(@PathVariable String _id) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        specialistService.deleteSpecialist(_id);
                        return ResponseEntity.noContent().build();
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }

    }
}
