package com.dev.server.Controller;

import com.dev.server.Service.AccountService;
import com.dev.server.Service.AppointmentService;
import com.dev.server.model.Appointment;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class AppointmentApiController {
    @Autowired
    AccountService accountService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private AppointmentService appointmentService;

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping("/Appointment")
    public ResponseEntity<?> getAllAppointments() {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    return ResponseEntity.ok().body(appointmentService.getAllAppointments());
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }
    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @GetMapping("/Report/Appointment/{_id}")
    public ResponseEntity<?> getAppointmentById(@PathVariable String _id) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    Appointment appointment = appointmentService.getAppointmentById(_id);
                    if (appointment != null) {
                        return ResponseEntity.ok().body(appointment);
                    } else {
                        return ResponseEntity.notFound().build();
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PostMapping("/Report/Appointment")
    public ResponseEntity<?> addAppointment(@RequestBody Appointment appointment) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        Appointment createdAppointment = appointmentService.createAppointment(appointment);
                        return ResponseEntity.status(HttpStatus.CREATED).body(createdAppointment);
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @PutMapping("/Report/Appointment/{_id}")
    public ResponseEntity<?> updateAppointment(@PathVariable String _id, @RequestBody Appointment appointment) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        Appointment updatedAppointment = appointmentService.updateAppointment(_id, appointment);
                        if (updatedAppointment != null) {
                            return ResponseEntity.ok().body(updatedAppointment);
                        } else {
                            return ResponseEntity.notFound().build();
                        }
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }

    }

    @CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
    @DeleteMapping("/Report/Appointment/{_id}")
    public ResponseEntity<?> deleteAppointment(@PathVariable String _id) {
        try {
            String token = request.getHeader("token");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String accessToken = token.split(" ")[1];
                Claims claims = accountService.verifyToken(accessToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("user", claims);
                    String role = claims.get("role", String.class);
                    if (role == null || !role.equals("admin")) {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Bạn không có quyền admin để làm điều đó");
                    } else {
                        appointmentService.deleteAppointment(_id);
                        return ResponseEntity.noContent().build();
                    }
                }
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }

    }
}
