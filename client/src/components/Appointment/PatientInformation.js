import React, { useEffect, useState } from "react";
import styled from "styled-components";
import {
  IoPersonOutline,
  IoCalendarClearOutline,
  IoAtOutline,
  IoCardOutline,
  IoMaleFemaleOutline,
  IoIdCardOutline,
  IoPeopleOutline,
  IoNavigateCircleOutline,
} from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import recordApi from "../../api/recordApi";
function PatientInformation(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [record, setRecord] = useState();
  const encode = (id) => {
    return btoa(id);
  }
  const recordId = useSelector((state) => state.appointment?.appointment?.recordId)
  useEffect(() => {
    try {
      const fetchRecord = async () => {
        const data = await recordApi.getRecordById(
          axiosJWT,
          navigated,
          accessToken,
          encode(recordId)
        );
        setRecord(data.data);
      }
      fetchRecord();
    } catch (error) {
      throw error;
    }
  }, [])
  return (
    <div className="hidden lg:flex justify-center w-full my-5">
      <div className="w-full">
        <div className="flex flex-wrap rounded-t shadow-md my-6">
          <div className="w-full bg-[#fb6f92] h-10 flex items-center pl-5 text-white text-sm font-semibold rounded-t">
            Chi tiết hồ sơ bệnh nhân
          </div>
          <div className="flex w-full my-5 pl-5 pr-7">
            <div className="w-1/2">
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoPersonOutline></IoPersonOutline>
                    </div>
                    <h3 className="font-normal text[#343a40]">Họ và tên</h3>
                  </div>
                </div>
                <div className="font-bold text-sm w-3/5 uppercase text-[#ff70a6]">
                  {record?.fullName}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoCalendarClearOutline></IoCalendarClearOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Ngày sinh</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record?.date ? new Date(record.date).toISOString().split("T")[0] : "Invalid date"}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoAtOutline></IoAtOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Email</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record?.email}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoCardOutline></IoCardOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Mã số BHYT</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  Chưa cập nhật
                </div>
              </div>
            </div>
            <div className="w-1/2">
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoMaleFemaleOutline></IoMaleFemaleOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Giới tính</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record?.sex}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoIdCardOutline></IoIdCardOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">CMND</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record?.identityCardNumber}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoPeopleOutline></IoPeopleOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Dân tộc</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record?.peoples}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoNavigateCircleOutline></IoNavigateCircleOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Địa chỉ</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record?.address}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PatientInformation;
