import React from "react";
import HomeService from "../../../components/HomePage/HomeService";
import HomeNews from "../../../components/HomePage/HomeNews";
import About1 from "../../../components/HomePage/About1";
import About2 from "../../../components/HomePage/About2";
import About3 from "../../../components/HomePage/About3";
const HomePage = () => {
    return (
        <div className="z-[-1]">
        <img src="/images/backgroundhopital.jpg" alt="backgroundhopital" className="w-full h-auto"/>
            <HomeService></HomeService>
            <HomeNews></HomeNews>
            <About1></About1>
            <About2></About2>
            <About3></About3>
        </div>
    );
}
export default HomePage;