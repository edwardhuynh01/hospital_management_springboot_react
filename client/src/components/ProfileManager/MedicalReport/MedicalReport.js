import React, { useEffect, useState } from "react";
import { IoChevronDownOutline, IoChevronForwardOutline } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
import recordApi from "../../../api/recordApi";
import appointmentApi from "../../../api/appointmentApi";
import specialistApi from "../../../api/specialistApi";
import doctorApi from "../../../api/doctorApi";

function MedicalReport(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const accessToken = user?.accessToken;
  const axiosJWT = createAxios(user, dispatch, loginSuccess);
  const { appointmentId, href, isActive, isPay } = props;
  const [appointment, setAppointment] = useState();
  const [specialist, setSpecialist] = useState(null);
  const [doctor, setDoctor] = useState(null);

  const encode = (id) => {
    return btoa(id);
  };
  useEffect(() => {
    const fetchData = async () => {
      try {
        if (!accessToken || typeof accessToken !== "string") {
          throw new Error("Token không hợp lệ");
        }

        const appointmentData = await appointmentApi.getAppointmentById(
          axiosJWT,
          navigate,
          accessToken,
          encode(appointmentId)
        );
        setAppointment(appointmentData.data);

        if (appointmentData.data?.specialistId) {
          const specialistData = await specialistApi.getSpecialistById(encode(appointmentData.data.specialistId));
          setSpecialist(specialistData);
        }
        if (appointmentData.data?.doctorId) {
          const doctorData = await doctorApi.getDoctorById(
            axiosJWT,
            navigate,
            accessToken,
            appointmentData.data.doctorId
          );
          setDoctor(doctorData.data);
        }
      } catch (error) {
        console.error("Error fetching data: ", error);
      }
    };
    fetchData();
  }, []);
  
  return (
    <a className={`flex justify-between items-center cursor-pointer p-4 border border-solid border-[#ffc2d1] 
    my-2 ${isActive ? "flex" : "hidden"}`} href={href}>
      <div className="text-sm text-[#ff8fab]">
        <p className="leading-7">Bệnh viện Đa khoa Sinh Tố Dâu</p>
        <p className="leading-7">Chuyên khoa: {specialist?.specialistName}</p>
        <p className="leading-7">Dịch vụ: Khám dịch vụ</p>
        <p className="leading-7">Bác sĩ: {doctor?.name}</p>
      </div>
      <div className={`py-1 px-6 ${isPay ? "bg-emerald-100 text-emerald-500" : "bg-gray-300"} rounded-lg`}>
        <p>{isPay ? "Đã thanh toán" : "Chưa thanh toán"}</p>
      </div>
    </a>
  );
}

export default MedicalReport;
