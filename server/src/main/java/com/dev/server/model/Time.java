package com.dev.server.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

public @Data
@RequiredArgsConstructor
@AllArgsConstructor
class Time {
    private String timeStart;
    private String timeEnd;
    private boolean isChoose;
}
