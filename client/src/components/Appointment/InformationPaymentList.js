import React, { useEffect, useState } from "react";
import InformationPayment from "./InformationPayment";
import { IoAlertCircleOutline } from "react-icons/io5";
import specialistApi from "../../api/specialistApi";
import { useSelector } from "react-redux";
function InformationPaymentList(props) {
  const [listSpecialists, setListSpecialists] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const appointmentsList = useSelector(
    (state) => state.appointment?.appointmentsList
  );
  const encode = (id) => {
    return btoa(id);
  };
  useEffect(() => {
    const fetchNews = async () => {
      try {
        const specialistPromises = appointmentsList.map(async (item) => {
          const response = await specialistApi.getSpecialistById(
            encode(item?.specialistId)
          );
          return response;
        });
        const specialists = await Promise.all(specialistPromises);
        setListSpecialists(specialists);
        const newTotalPrice = specialists.reduce((acc, specialist) => {
          return acc + Number(specialist.price);
        }, 0);

        setTotalPrice(newTotalPrice);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
    };
    fetchNews();
  }, [appointmentsList]);
  return (
    <div className="w-1/2 px-5">
      <div className="flex justify-start items-center text-[#ff8fab] text-base md:text-lg w-full">
        <div className="mx-2">
          <IoAlertCircleOutline></IoAlertCircleOutline>
        </div>
        <h3 className="font-normal">THÔNG TIN THANH TOÁN</h3>
      </div>
      {listSpecialists.map((item, index) => (
        <InformationPayment
          key={index}
          name={item.specialistName}
          price={item.price}
        />
      ))}
      <div className="w-full flex flex-wrap py-7">
        <div className="w-full text-end flex items-end text-[#495057] pb-3 justify-end">
          <p className="text-sm">Tổng tiền khám:</p>
          <p className="font-semibold w-40 text-sm text-[#ff8fab]">
            {totalPrice.toLocaleString("vi-VN")} VNĐ
          </p>
        </div>
        <div className="w-full text-end flex items-end text-[#495057] pb-3 justify-end">
          <p className="text-sm">Phí tiện ích:</p>
          <p className="font-semibold w-40 text-sm text-[#ff8fab]">
            {props.utilityFee.toLocaleString("vi-VN")} VNĐ
          </p>
        </div>
        <div className="w-full text-end flex items-end text-[#495057] py-3 border-t border-dashed justify-end border-[#fb6f92]">
          <p className="text-sm">TỔNG CỘNG:</p>
          <p className="font-semibold w-40 text-sm text-[#ff8fab]">
            {(totalPrice + props.utilityFee).toLocaleString("vi-VN")} VNĐ
          </p>
        </div>
      </div>
    </div>
  );
}

export default InformationPaymentList;
