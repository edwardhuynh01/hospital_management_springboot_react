package com.dev.server.Service;

import com.dev.server.DTO.UserDTO;
import com.dev.server.model.Account;
import com.dev.server.repository.AccountRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.servlet.http.HttpServletRequest;
import org.bson.types.ObjectId;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.*;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JavaMailSender mailSender;
    @Value("${jwt.secret}")
    private String jwtSecret;

    public void registerUser(UserDTO userDto) throws Exception {
        try {
            if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
                throw new IllegalArgumentException("Passwords do not match");
            }

            if (accountRepository.findByEmail(userDto.getEmail()).isPresent() ||
                    accountRepository.findByPhoneNumber(userDto.getPhoneNumber()).isPresent()) {
                throw new IllegalArgumentException("Email hoặc số điện thoại đã được sử dụng");
            }

            Account newAccount = new Account();
            newAccount.setEmail(userDto.getEmail());
            newAccount.setPhoneNumber(userDto.getPhoneNumber());
            newAccount.setUsername(userDto.getUsername());
            newAccount.setPassword(passwordEncoder.encode(userDto.getPassword()));

            accountRepository.save(newAccount);
            byte[] secretBytes = Base64.getEncoder().encode(jwtSecret.getBytes());
            SecretKeySpec secretKey = new SecretKeySpec(secretBytes, "HmacSHA512");
            String token = Jwts.builder()
                    .setSubject(newAccount.getEmail())
                    .setExpiration(new Date(System.currentTimeMillis() + 1800000)) // 30 minutes
                    .signWith(secretKey)
                    .compact();
            String host = "BVSinhToDau.com";
            String resetLink = "http://localhost:3000/confirmEmail?token=" + token + "&email=" + newAccount.getEmail();
            sendConfirmMail(newAccount, host, resetLink);
        }catch (Exception e){
            throw new Exception("Lỗi đăng ký user: " + e.getMessage(),e);
        }
    }
    private void sendConfirmMail(Account account,String host, String resetLink) throws MessagingException {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("nhat.tlm3173@gmail.com");
            helper.setTo(account.getEmail());
            helper.setSubject("[Bệnh viện đa khoa sinh tố dâu] Xác nhận mật khẩu");
            helper.setText(
                    "<h2>Chào " + account.getUsername() + ",</h2>" +
                            "<br/>" +
                            "<p>Bạn vừa mới yêu cầu đăng ký cho tài khoản " + host + ". Nhấn vào link bên dưới để tiến hành.</p>" +
                            "<br/>" +
                            "<a href=\"" + resetLink + "\">Xác nhận tài khoản</a>" +
                            "<br/>" +
                            "<p>Nếu bạn không yêu cầu đăng ký, Vui lòng bỏ qua điều này hoặc cho chúng tôi biết. Link xác nhận tài khoản này chỉ tồn tại 30 phút.</p>" +
                            "<br/>" +
                            "<p>Cảm ơn,</p>" +
                            "<p>Sinh tố dâu team</p>",
                    true
            );
            mailSender.send(message);
        }catch (Exception e){
            throw new MessagingException("Lỗi gửi xác nhận email: " + e.getMessage(), e);
        }
    }

    public Claims verifyToken(String token) {
        try {
            byte[] secretBytes = Base64.getEncoder().encode(jwtSecret.getBytes());
            SecretKeySpec secretKey = new SecretKeySpec(secretBytes, "HmacSHA512");
            return Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token).getPayload();
        } catch (JwtException e) {
            return null;
        }
    }
    public Account loginUser(String email, String password) {
        try {
            Optional<Account> accountOptional = accountRepository.findByEmail(email);
            if (accountOptional.isPresent()) {
                Account account = accountOptional.get();
                if (passwordEncoder.matches(password, account.getPassword())) {
                    return account;
                }
            }
            return null;
        }catch (Exception e){
            throw new RuntimeException("Lỗi đăng nhập vào user: " + e.getMessage(), e);
        }
    }
    public Account loginGG(String email, String name,boolean email_verified) {
        try {
            Optional<Account> accountOptional = accountRepository.findByEmail(email);
            if (accountOptional.isEmpty()) {
                Account account = new Account();
                account.setEmail(email);
                account.setUsername(name);
                account.setPassword(null);
                account.setPhoneNumber(new ObjectId().toString());
                account.setEmailConfirmed(email_verified);
                accountRepository.save(account);
                return account;
            }else{
                return accountOptional.get();
            }
        }catch (Exception e){
            throw new RuntimeException("Lỗi đăng nhập vào tài khoản google: " + e.getMessage(), e);
        }
    }
    public String generateAccessToken(Account account) {
        try {
            byte[] secretBytes = Base64.getEncoder().encode(jwtSecret.getBytes());
            SecretKeySpec secretKey = new SecretKeySpec(secretBytes, "HmacSHA512");
            return Jwts.builder()
                    .setSubject(account.getEmail())
                    .claim("id", account.get_id())
                    .claim("role", account.getRole())
                    .setIssuedAt(new Date())
                    .setExpiration(new Date(System.currentTimeMillis() + (60*1000)))
                    .signWith(secretKey)
                    .compact();
        }catch (Exception e){
            throw new RuntimeException("Lỗi tạo mã thông báo truy cập: " + e.getMessage(), e);
        }
    }
    public String generateRefreshToken(Account account) {
        try {
            byte[] secretBytes = Base64.getEncoder().encode(jwtSecret.getBytes());
            SecretKeySpec secretKey = new SecretKeySpec(secretBytes, "HmacSHA512");
            return Jwts.builder()
                    .setSubject(account.getEmail())
                    .claim("id", account.get_id())
                    .claim("role", account.getRole())
                    .setIssuedAt(new Date())
                    .setExpiration(new Date(System.currentTimeMillis() + (365L * 24 * 60 * 60 * 1000)))
                    .signWith(secretKey)
                    .compact();
        }catch (Exception e){
            throw new RuntimeException("Lỗi tạo mã làm mới thông báo : " + e.getMessage(), e);
        }
    }
    public String generateRefreshPassToken(String email) {
        try {
            byte[] secretBytes = Base64.getEncoder().encode(jwtSecret.getBytes());
            SecretKeySpec secretKey = new SecretKeySpec(secretBytes, "HmacSHA512");
            return Jwts.builder()
                    .setSubject(email)
                    .claim("email", email)
                    .setIssuedAt(new Date())
                    .setExpiration(new Date(System.currentTimeMillis() + (30 * 60 * 1000)))
                    .signWith(secretKey)
                    .compact();
        }catch (Exception e){
            throw new RuntimeException("Lỗi tạo mã thông báo làm mới mật khẩu: " + e.getMessage(), e);
        }
    }
    public void forgotPassword(String email, HttpServletRequest request) throws Exception {
        try {
            Optional<Account> accountOptional = accountRepository.findByEmail(email);
            if (accountOptional.isEmpty()) {
                throw new UsernameNotFoundException("Email không tồn tại!");
            }
            String token = generateRefreshPassToken(email);
            String host = request.getHeader("host");
            String resetLink = request.getScheme() + "://localhost:3000/reset?token=" + token + "&email=" + email;
            Account account = accountOptional.get();
            sendForgotPassMail(account, host, resetLink);
        }catch (Exception e){
            throw new Exception("Lỗi xử lý yêu cầu quên mật khẩu: " + e.getMessage(), e);
        }
    }
    private void sendForgotPassMail(Account account,String host, String resetLink) throws MessagingException {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("nhat.tlm3173@gmail.com");
            helper.setTo(account.getEmail());
            helper.setSubject("[Bệnh viện đa khoa sinh tố dâu] Lấy lại mật khẩu");
            helper.setText(
                    "<h2>Chào " + account.getUsername() + ",</h2>" +
                            "<br/>" +
                            "<p>Bạn vừa mới yêu cầu lấy lại mật khẩu cho tài khoản " + host + ". Nhấn vào link bên dưới để tiến hành.</p>" +
                            "<br/>" +
                            "<a href=\"" + resetLink + "\">Xác nhận tài khoản</a>" +
                            "<br/>" +
                            "<p>Nếu bạn không yêu cầu lấy lại mật khẩu, Vui lòng bỏ qua điều này hoặc cho chúng tôi biết. Link xác nhận tài khoản này chỉ tồn tại 30 phút.</p>" +
                            "<br/>" +
                            "<p>Cảm ơn,</p>" +
                            "<p>Sinh tố dâu team</p>",
                    true
            );
            mailSender.send(message);
        }catch (Exception e){
            throw new MessagingException("Lỗi gửi email quên mật khẩu: " + e.getMessage(), e);
        }
    }
    public Account createAccount(Account account) {
        try {
            return accountRepository.save(account);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi tạo tài khoản: " + e.getMessage(), e);
        }
    }

    // Cập nhật thông tin tài khoản
    public Account updateAccount(String id, Account account) {
        try {
            Optional<Account> optionalAccount = accountRepository.findById(id);
            if (optionalAccount.isPresent()) {
                Account existingAccount = optionalAccount.get();
                existingAccount.setRole(account.getRole());
                return accountRepository.save(existingAccount);
            } else {
                throw new RuntimeException("Account not found with id: " + id);
            }
        }catch (Exception e){
            throw new RuntimeException("Lỗi cập nhật tài khoản: " + e.getMessage(), e);
        }
    }

    // Lấy thông tin tài khoản bằng ID
    public Account getAccountById(String id) {
        try {
            Optional<Account> optionalAccount = accountRepository.findById(id);
            return optionalAccount.orElse(null);
        }catch (Exception e){
            throw new RuntimeException("Lỗi lấy tài khoản theo id: " + e.getMessage(), e);
        }
    }

    // Lấy danh sách tất cả tài khoản
    public List<Account> getAllAccounts() {
        try {
            return accountRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi nhận tất cả tài khoản: " + e.getMessage(), e);
        }
    }

    // Xóa tài khoản bằng ID
    public void deleteAccount(String id) {
        try {
            accountRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi xóa tài khoản: " + e.getMessage(), e);
        }
    }
}
