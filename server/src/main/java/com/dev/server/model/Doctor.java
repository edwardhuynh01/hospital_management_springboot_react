package com.dev.server.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import lombok.*;

import java.rmi.server.ObjID;
import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@Document(collection = "doctors")
public class Doctor {
    @Id
    private String _id;
    private String name;
    private String phoneNumber;
    private String email;
    private String avatarImage;
    private String experience;
    private int dateOfWeek;
    private String sex;
    private List<Day> days;
    @JsonSerialize(using = ToStringSerializer.class)
    private ObjectId specialist;
    @DBRef(lazy = true)
    private List<Degree> degrees;
}
