import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import styled from "styled-components";
import {
  IoPersonOutline,
  IoCalendarClearOutline,
  IoAtOutline,
  IoCardOutline,
  IoMaleFemaleOutline,
  IoIdCardOutline,
  IoPeopleOutline,
  IoNavigateCircleOutline,
} from "react-icons/io5";
import recordApi from "../../../api/recordApi";
import { useDispatch, useSelector } from "react-redux";
import { loginSuccess } from "../../../redux/authSlice";
import { createAxios } from "../../../createInstance";
const Back = styled.div`
  transition: ease-in-out all 0.2s;
`;
function DetailMedicalRecord(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const { id } = useParams();
  const [record, setRecord] = useState();
  useEffect(() => {
    if (!user) {
      navigated("/login");
    }
  });
  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchRecord = async () => {
        const data = await recordApi.getRecordById(
          axiosJWT,
          navigated,
          accessToken,
          id
        );
        console.log(data);
        setRecord(data.data);
      };
      fetchRecord();
    } catch (error) {}
  }, []);
  const date = new Date(record && record.date);
  const formattedDate = `${date.getDate()}/${
    date.getMonth() + 1
  }/${date.getFullYear()}`;
  return (
    <div className="flex justify-center w-full bg-white py-5">
      <div className="lg:w-6/12">
        <Back className="w-20 h-10 flex items-center justify-center rounded border border-solid hover:text-[#fb6f92] hover:border-[#fb6f92]">
          <a className="text-xs font-normal" href="/profile">
            Quay lại
          </a>
        </Back>
        <div className="flex flex-wrap rounded-t shadow-md my-6">
          <div className="w-full bg-[#fb6f92] h-10 flex items-center pl-5 text-white text-sm font-semibold rounded-t">
            Chi tiết hồ sơ bệnh nhân
          </div>
          <div className="flex w-full my-5 pl-5 pr-7">
            <div className="w-1/2">
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoPersonOutline></IoPersonOutline>
                    </div>
                    <h3 className="font-normal text[#343a40]">Họ và tên</h3>
                  </div>
                </div>
                <div className="font-bold text-sm w-3/5 uppercase text-[#ff70a6]">
                  {record && record.fullName}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoCalendarClearOutline></IoCalendarClearOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Ngày sinh</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {formattedDate}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoAtOutline></IoAtOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Email</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record && record.email}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoCardOutline></IoCardOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Mã số BHYT</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  Chưa cập nhật
                </div>
              </div>
            </div>
            <div className="w-1/2">
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoMaleFemaleOutline></IoMaleFemaleOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Giới tính</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record && record.sex}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoIdCardOutline></IoIdCardOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">CMND</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record && record.identityCardNumber}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoPeopleOutline></IoPeopleOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Dân tộc</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record && record.peoples}
                </div>
              </div>
              <div className="flex w-full items-center justify-center mb-2">
                <div className="w-2/5">
                  <div className="flex justify-start items-center">
                    <div className="mx-2 ">
                      <IoNavigateCircleOutline></IoNavigateCircleOutline>
                    </div>
                    <h3 className="font-normal text-[#343a40]">Địa chỉ</h3>
                  </div>
                </div>
                <div className="font-normal text-sm w-3/5 text[#343a40]">
                  {record && record.address}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DetailMedicalRecord;
