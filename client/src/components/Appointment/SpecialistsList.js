import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Specialist from "./Specialist";
import { IoSearchOutline } from "react-icons/io5";
import specialistApi from "../../api/specialistApi";
import { useDispatch } from "react-redux";
import { chooseSpecialist } from "../../redux/appointmentSlice";
import LoadingEdit from "../Loading/LoadingEdit";
const Container = styled.div`
  border-bottom: 2px solid #ffb3c6;
  border-left: 2px solid #ffb3c6;
  border-right: 2px solid #ffb3c6;
`;
const ClearButton = styled.button`
  position: absolute;
  /* top: 50%; */
  right: 12%;
  transform: translateY(-10%);
  background: none;
  border: none;
  cursor: pointer;
`;
const InputSearch = styled.div`
  transition: all ease-in-out 0.3s;
`;
const ContainerList = styled.div`
  max-height: 300px;
  overflow-y: auto;
`;
const StyledList = styled.ul`
  list-style-type: none;
  padding: 0;
  margin: 0;
`;
const ListItem = styled.li`
  border-bottom: 1px solid #ccc;
`;
function SpecialistsList(props) {
  const [value, setValue] = useState("");
  const [onFocus, setOnFocus] = useState(false);
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  const ClearInput = () => {
    setValue("");
  };
  const handleOnFocus = () => {
    setOnFocus(true);
  };
  const handleOnBlur = () => {
    setOnFocus(false);
  };
  const [specialists, setSpecialists] = useState([]);
  const [filteredSpecialist, setFilteredSpecialist] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    const fetchSpecialists = async () => {
      try {
        const data = await specialistApi.getAllSpecialist();
        setSpecialists(data);
      } catch (error) { }
      finally {
        setIsLoading(false);
      }
    };
    fetchSpecialists();
  }, []);
  useEffect(() => {
    const filteredSpecialist = specialists.filter((x) =>
      x.specialistName.toLowerCase().trim().includes(value.toLowerCase().trim())
    );
    setFilteredSpecialist(filteredSpecialist);
  }, [value, specialists]);
  const checkSearch =
    Array.isArray(filteredSpecialist) && filteredSpecialist.length > 0;
  const DISPATCH = useDispatch();
  const saveSpecialId = async (specId) => {
    DISPATCH(chooseSpecialist({ specialistId: specId }));
  };
  return (
    <div className="w-full flex flex-wrap">
      <div className="w-full bg-[#f26a8d] text-white px-5 py-2 rounded-t-md font-semibold">
        <h2>Vui lòng chọn chuyên khoa</h2>
      </div>
      <Container className="w-full p-4">
        <InputSearch
          className={`w-full flex items-center rounded-md border border-solid h-11 ${onFocus ? "border-[#ffb3c6]" : ""
            }`}
        >
          <label className="px-2" htmlFor="search">
            <IoSearchOutline></IoSearchOutline>
          </label>
          <input
            id="search"
            value={value}
            onChange={handleChange}
            onFocus={handleOnFocus}
            onBlur={handleOnBlur}
            className="w-full outline-none"
            placeholder="Tìm nhanh chuyên khoa"
            type="text"
          />
          <ClearButton
            className={`${value === "" ? "hidden" : ""}`}
            onClick={ClearInput}
          >
            <span aria-hidden="true">×</span>
          </ClearButton>
        </InputSearch>
        <ContainerList>
          {
            isLoading ? (
              <LoadingEdit />
            ) : (
              <StyledList>
                {checkSearch &&
                  filteredSpecialist.map((specialistsItems) => (
                    <ListItem key={specialistsItems._id}>
                      <Specialist
                        specId={specialistsItems._id}
                        chooseSpecialist={() => saveSpecialId(specialistsItems._id)}
                      >
                        {specialistsItems.specialistName}
                      </Specialist>
                    </ListItem>
                  ))}
                {!checkSearch && (
                  <h2 className="mt-1 text-red-600">
                    Không tồn tại chuyên khoa này!
                  </h2>
                )}
              </StyledList>
            )
          }
        </ContainerList>
      </Container>
    </div>
  );
}

export default SpecialistsList;
