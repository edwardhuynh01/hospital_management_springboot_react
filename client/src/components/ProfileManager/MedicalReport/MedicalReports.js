import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createAxios } from "../../../createInstance";
import { loginSuccess } from "../../../redux/authSlice";
import recordApi from "../../../api/recordApi";
import { IoChevronDownOutline } from "react-icons/io5";
import appointmentApi from "../../../api/appointmentApi";
import MedicalReport from "./MedicalReport";

const MedicalReports = (props) => {
  const [activeIndex, setActiveIndex] = useState(-1);

  const handleRecordClick = (index) => {
    if (index === activeIndex) {
      setActiveIndex(-1); // Đóng `Record` nếu đã mở
    } else {
      setActiveIndex(index); // Mở `Record` tại vị trí index
    }
  };
  const user = useSelector((state) => state.auth.login?.currentUser);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const accessToken = user?.accessToken;
  const axiosJWT = createAxios(user, dispatch, loginSuccess);
  const [record, setRecord] = useState();
  const { recordId, reportId, appointment, isPay, createAt } = props;
  console.log(appointment);
  const encode = (id) => {
    return btoa(id);
  };

  useEffect(() => {
    const fetchRecord = async () => {
      try {
        if (!accessToken || typeof accessToken !== "string") {
          throw new Error("Token không hợp lệ");
        }
        const data = await recordApi.getRecordById(
          axiosJWT,
          navigate,
          accessToken,
          encode(recordId)
        );
        setRecord(data.data);
      } catch (error) {
        console.error("Error fetching record:", error);
      }
    };
    fetchRecord();
  }, []);

  return (
    <div>
      {record && (
        <div
          className="flex justify-between items-center cursor-pointer bg-[#ff8fab] py-2 px-4 text-white 
                font-semibold mt-4"
          onClick={() => handleRecordClick(record?._id)}
        >
          <p>{record?.fullName}</p>
          <p>{createAt}</p>
          <IoChevronDownOutline />
        </div>
      )}
      {appointment?.map((appointmentItem, index) => (
        <MedicalReport
          href={`DetailNotification/${encode(reportId)}`}
          key={index}
          isPay={isPay}
          isActive={activeIndex === record?._id}
          appointmentId={appointmentItem}
        />
      ))}
    </div>
  );
};

export default MedicalReports;
