import React from 'react';
import Title from '../Title/Title';

const About2 = (props) => {
    // Các lớp CSS
    const divClass = 'relative w-full h-[300px] lg:w-[40%] lg:h-[400px] mb-8';
    const divBgClass = 'absolute w-full h-full left-0 top-0 bg-black bg-opacity-50';
    const divTextClass = 'absolute top-0 left-0 w-full h-full flex flex-col justify-center items-center text-center text-white';
    const divTitleClass = 'text-2xl font-bold my-4';
    const divContentClass = 'w-3/4 text-lg';

    return (
        <section className={`relative ${props.className || ""}`}>
            {/* Ảnh nền làm mờ */}
            <img className="absolute w-full h-full top-0 left-0 inset-0 object-cover object-center z-0 blur-lg opacity-30" src='/Images/sumenh.jpg' alt="Tầm nhìn" />
            <div className="container mx-auto overflow-hidden h-full w-full">
                <Title>TẦM NHÌN - SỨ MỆNH</Title>
                <div className="flex flex-col lg:flex-row items-center justify-center relative z-10 mt-10">
                    {/* Phần nội dung thứ nhất */}
                    <div className={divClass}>
                        <div className={divBgClass}></div>
                        <img className='w-full h-full object-cover object-center' src='/Images/sumenh1.jpg' alt="Tầm nhìn" />
                        <div className={divTextClass}>
                            <p className={divTitleClass}>TẦM NHÌN</p>
                            <p className={`${divContentClass} lg:px-6 lg:py-4`}>Trở thành phòng khám đa khoa chất lượng cao dẫn đầu Việt Nam và đạt chuẩn quốc tế.</p>
                        </div>
                    </div>
                    {/* Khoảng trống giữa hai tấm hình */}
                    <div className="hidden lg:block w-[5%]"></div>
                    {/* Phần nội dung thứ hai */}
                    <div className={divClass}>
                        <div className={divBgClass}></div>
                        <img className='w-full h-full object-cover object-center' src='/Images/sumenh3.png' alt="Sứ mệnh" />
                        <div className={divTextClass}>
                            <p className={divTitleClass}>SỨ MỆNH</p>
                            <p className={`${divContentClass} lg:px-6 lg:py-4`}>Xây dựng một mô hình chuẩn mực phòng khám đa khoa cung cấp dịch vụ khám bệnh, chữa bệnh chất lượng cao làm hài lòng người bệnh.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default About2;
