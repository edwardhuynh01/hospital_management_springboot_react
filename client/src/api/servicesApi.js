import axiosclient from "./axiosClient";
const decode = (id) => {
    return atob(id);
}
const servicesApi = {
    getAllService(params) {
        const url = '/Service';
        return axiosclient.get(url, { params });
    },
    getServiceById(id) {
        const url = `/Service/${decode(id)}`;
        return axiosclient.get(url);
    },
    createService: async (axiosJWT, navigate, accessToken, data) => {
        try {
            const url = "http://localhost:5000/api/Service";
            return axiosJWT.post(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    updateService: async (axiosJWT, navigate, accessToken, id, data) => {
        try {
            const url = `http://localhost:5000/api/Service/${decode(id)}`;
            return axiosJWT.put(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    deleteService: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `http://localhost:5000/api/Service/${id}`;
            return axiosJWT.delete(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
};
export default servicesApi;