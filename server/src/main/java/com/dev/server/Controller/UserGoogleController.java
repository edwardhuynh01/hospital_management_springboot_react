package com.dev.server.Controller;

import com.dev.server.DTO.Root;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/google")
public class UserGoogleController {
    @GetMapping("/signing")
    public Map<String, Object> signingGoogle(OAuth2AuthenticationToken authentication) {
        try {
            System.out.println(toPerson(authentication.getPrincipal().getAttributes()).getEmail());
            System.out.println(toPerson(authentication.getPrincipal().getAttributes()).getName());
            return authentication.getPrincipal().getAttributes();
        }catch (Exception e) {
            String errorMessage = "Đã xảy ra lỗi khi đăng nhập bằng Google: " + e.getMessage();
            e.printStackTrace();
            return Collections.singletonMap("error", errorMessage);
        }
    }

    public Root toPerson(Map<String, Object> map) {
        try {
            if (map == null) {
                return null;
            }
            Root root = new Root();
            root.setName((String) map.get("name"));
            root.setEmail((String) map.get("email"));
            return root;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Đã xảy ra lỗi trong quá trình chuyển đổi thông tin người dùng từ Map sang đối tượng Root.");
        }
    }
}
