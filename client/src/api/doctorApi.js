const decode = (id) => {
  return atob(id);
};
const doctorApi = {
  getAllDoctor: async (axiosJWT, navigate, accessToken) => {
    try {
      const url = "http://localhost:5000/api/Doctor";
      return axiosJWT.get(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  getDoctorById: async (axiosJWT, navigate, accessToken, id) => {
    try {
      const url = `http://localhost:5000/api/Doctor/${id}`;
      return axiosJWT.get(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  createDoctor: async (axiosJWT, navigate, accessToken, data) => {
    try {
      const url = "http://localhost:5000/api/Doctor";
      return axiosJWT.post(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
          'Content-Type': 'application/json'
        },
      });
    } catch (error) {
      navigate("/admin/doctor");
    }
  },
  updateDoctor: async (axiosJWT, navigate, accessToken, id, data) => {
    try {
      const url = `http://localhost:5000/api/Doctor/${decode(id)}`;
      return axiosJWT.put(url, data, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
  deleteDoctor: async (axiosJWT, navigate, accessToken, id) => {
    try {
      const url = `http://localhost:5000/api/Doctor/${id}`;
      return axiosJWT.delete(url, {
        headers: {
          token: `Bearer ${accessToken}`,
        },
      });
    } catch (error) {
      navigate("/login");
    }
  },
};
export default doctorApi;