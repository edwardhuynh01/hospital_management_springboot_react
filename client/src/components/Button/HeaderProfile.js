import React from "react";
import styled from "styled-components";
import {
  IoDocumentTextOutline,
  IoDocumentOutline,
  IoNotificationsOutline,
  IoPowerOutline,
} from "react-icons/io5";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
const DropdownProfile = styled.div``;
const AdminBtn = styled.a`
  transition: ease-in-out all 0.2s;
  &:hover {
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 104, 159);
    transform: translate(0, -0.25em);
  }
`;
function HeaderProfile(props) {
  const user = useSelector((state) => state.auth.login?.currentUser);
  return (
    <DropdownProfile className="<=lg:w-auto flex flex-col xl:shadow-md  xl:absolute top-[3.9rem] right-[8.3rem] w-1/4 xl:rounded-2xl xl:bg-white xl:border-solid xl:border <=lg:mt-2 xl:border-[#fb6f92] px-5 xl:p-5 z-50">
      <div className="xl:text-start xl:flex xl:justify-between xl:flex-wrap text-center px-4 py-5 border-b <=lg:border-t border-solid border-b-[#fb6f92] <=lg:border-t-[#fb6f92]">
        <div className="xl:w-1/2">
          <h4>Xin chào!</h4>
          <h4 className="font-bold text-[#fb6f92]">{props.name}</h4>
        </div>
        {user?.role === "admin" ? (
          <div className="w-1/2 h-10 m-auto xl:mt-0 mt-3">
            <AdminBtn
              className="w-full h-full xl:text-sm text-xs bg-[#fb6f92] text-white rounded-md flex justify-center items-center cursor-pointer font-semibold"
              href="/admin"
            >
              Trang Admin
            </AdminBtn>
          </div>
        ) : (
          ""
        )}
      </div>
      <ul className="list-none flex flex-col gap-4 py-5 <=lg:border-b <=lg:border-solid <=lg:border-b-[#fb6f92]">
        <li>
          <div>
            <div className="flex justify-start items-center">
              <Link
                to="/profile?component=medicalRecords"
                onClick={props.onClick}
                className="flex justify-start items-center gap-2 hover:text-[#fb6f92] transition duration-150 ease-in-out delay-150"
              >
                <IoDocumentTextOutline></IoDocumentTextOutline>
                <h3 className="font-normal ">Hồ sơ bệnh nhân</h3>
              </Link>
            </div>
          </div>
        </li>
        <li>
          <div>
            <div className="flex justify-start items-center">
              <Link
                to="/profile?component=medicalReports"
                onClick={props.onClick}
                className="flex items-center gap-2 hover:text-[#fb6f92] transition duration-150 ease-in-out delay-150"
              >
                <IoDocumentOutline></IoDocumentOutline>
                <h3 className="font-normal">Phiếu khám bệnh</h3>
              </Link>
            </div>
          </div>
        </li>
        <li>
          <div>
            <div className="flex justify-start items-center">
              <Link
                to="/profile?component=notifications"
                onClick={props.onClick}
                className="flex items-center gap-2 hover:text-[#fb6f92] transition duration-150 ease-in-out delay-150"
              >
                <IoNotificationsOutline></IoNotificationsOutline>
                <h3 className="font-normal">Thông báo</h3>
              </Link>
            </div>
          </div>
        </li>
        <li>
          <div className="flex justify-start items-center">
            <div
              onClick={props.logout}
              className="flex cursor-pointer items-center gap-2 hover:text-[#fb6f92] transition duration-150 ease-in-out delay-150"
            >
              <IoPowerOutline></IoPowerOutline>
              <h3 className="font-normal">Thoát</h3>
            </div>
          </div>
        </li>
      </ul>
    </DropdownProfile>
  );
}

export default HeaderProfile;
