package com.dev.server.repository;

import com.dev.server.model.Appointment;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppointmentRepository extends MongoRepository<Appointment, String> {
    Optional<Appointment> findByIdAndRecordIdAndDayOfWeekAndDayAndMonthAndYearAndTimeStartAndTimeEndAndSpecialistIdAndDoctorId(
            ObjectId Id,
            ObjectId recordId,
            String dayOfWeek,
            String day,
            String month,
            String year,
            String timeStart,
            String timeEnd,
            ObjectId specialistId,
            ObjectId doctorId
    );
}
