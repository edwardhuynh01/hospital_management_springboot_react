import { createSlice } from "@reduxjs/toolkit";
const appointmentSlice = createSlice({
  name: "appointment",
  initialState: {
    appointment: {
      recordId: null,
      specialistId: null,
      doctorId: null,
      DayOfWeek: null,
      day: null,
      month: null,
      year: null,
      timeStart: null,
      timeEnd: null,
    },
    appointmentsList: [],
    paymentToken: null,
  },
  reducers: {
    chooseRecord: (state, action) => {
      state.appointment = { ...state.appointment, ...action.payload };
    },
    chooseSpecialist: (state, action) => {
      state.appointment = { ...state.appointment, ...action.payload };
    },
    chooseDoctor: (state, action) => {
      state.appointment = { ...state.appointment, ...action.payload };
    },
    chooseDate: (state, action) => {
      state.appointment = { ...state.appointment, ...action.payload };
    },
    chooseTime: (state, action) => {
      state.appointment = { ...state.appointment, ...action.payload };
      if (
        state.appointmentsList.some(
          (x) => x.recordId !== state.appointment.recordId
        )
      ) {
        state.appointmentsList = [];
        state.appointmentsList.push(state.appointment);
      } else {
        if (
          state.appointmentsList.some(
            (x) => x.specialistId === state.appointment.specialistId
          )
        ) {
          state.appointmentsList = state.appointmentsList.filter(
            (x) => x.specialistId !== state.appointment.specialistId
          );
          state.appointmentsList.push(state.appointment);
        } else {
          state.appointmentsList.push(state.appointment);
        }
      }
    },
    deleteSpecialist: (state, action) => {
      state.appointmentsList = state.appointmentsList.filter(
        (x) => x.specialistId !== action.payload.specialistId
      );
    },
    addToken: (state, action) => {
      state.paymentToken = action.payload;
    },
    clearAppointment: (state) => {
      state.paymentToken = null;
      state.appointmentsList = [];
      state.appointment = {
        recordId: null,
        specialistId: null,
        doctorId: null,
        DayOfWeek: null,
        day: null,
        month: null,
        year: null,
        timeStart: null,
        timeEnd: null,
      };
    },
  },
});
export const {
  chooseRecord,
  chooseSpecialist,
  chooseDoctor,
  chooseDate,
  chooseTime,
  clearAppointment,
  deleteSpecialist,
  addToken,
} = appointmentSlice.actions;
export default appointmentSlice.reducer;
