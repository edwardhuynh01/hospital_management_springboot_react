import { exitOutline } from "ionicons/icons";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from 'react-router-dom'
import { createAxios } from "../../createInstance";
import { loginSuccess } from "../../redux/authSlice";
import accountApi from "../../api/accountApi";
import CountUp from "react-countup";

//deprecateds
const AccountsPage = () => {
    const user = useSelector((state) => state.auth.login?.currentUser);
    const DISPATCH = useDispatch();
    const navigated = useNavigate();
    let accessToken = user?.accessToken;
    let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
    const [account, setAccount] = useState([]);
    const [checked,setChecked] = useState(true);
    useEffect(() => {
        const fetchAccount = async () => {
            const data = await accountApi.getAllAccount(
                axiosJWT,
                navigated,
                accessToken
            );
            setAccount(data.data);
        }
        fetchAccount();
    }, [checked]);
    const fetchAccount = async (id,role) => {
        const accountUpdateData = {
            role: role
        }
        console.log(accountUpdateData)
        const data = await accountApi.updateAccount(
            axiosJWT,
            navigated,
            accessToken,
            id,
            accountUpdateData,
        );
        setChecked(!checked);
    }
    const handleCheckboxChange = (role, id) => {
        if (role === "admin") { 
            fetchAccount(id, "customer");
            
        } else {
            fetchAccount(id, "admin");
        }
    }
    return (
        <main class="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
            <div className="p-6 h-full">
                <div class="grid grid-cols-1">
                    <div class="bg-white border border-gray-100 shadow-md shadow-black/4 p-9 rounded-md ">
                        <div class="flex justify-between mb-4 items-start">
                            <div class="font-medium text-gray-500">Số Tài Khoản:{" "}
                                <CountUp end={account?.length} duration={2} /></div>
                        </div>
                        <div class="flex items-center mb-4 order-tab gap-4">
                            <button type="button" data-tab="order" data-tab-page="active" class="shadow-md shadow-black/4 bg-sky-400 text-sm font-medium text-white py-2 px-10 rounded-tl-md rounded-bl-md rounded-tr-md rounded-br-md hover:bg-sky-500 active">Thêm</button>
                            {/* <button type="button" data-tab="order" data-tab-page="completed" class=" bg-gray-100 text-sm font-medium text-gray-400 py-2 px-4 hover:text-gray-600">Completed</button>
                            <button type="button" data-tab="order" data-tab-page="canceled" class="bg-gray-100 text-sm font-medium text-gray-400 py-2 px-4 rounded-tr-md rounded-br-md hover:text-gray-600">Canceled</button> */}
                            <input type="text" class="shadow-md shadow-black/4 ml-3 py-2 pr-4 pl-10 bg-gray-50 w-full outline-none border border-gray-100 rounded-md text-sm focus:border-blue-500" placeholder="Tìm kiếm..."></input>
                            <button type="button" data-tab="order" data-tab-page="active" class="shadow-md shadow-black/4 bg-blue-400 text-sm font-medium text-white py-2 px-6 rounded-tl-md rounded-bl-md rounded-tr-md  rounded-br-md hover:bg-blue-500 active">Lọc</button>
                        </div>
                        <div class="overflow-x-auto">
                            <div class="overflow-y-auto max-h-[508px]">
                                <table class="w-full min-w-[800px] " data-tab-for="order" data-page="active">
                                    <thead>
                                        <tr>
                                            <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left rounded-tl-md rounded-bl-md">Tên Tài Khoản</th>
                                            <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left">Email</th>
                                            <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left">Role</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            account?.map((accountItem) => (
                                                <tr>
                                                    <td class="py-2 px-4 border-b border-b-gray-50 overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                        <span class="text-[13px] font-medium text-gray-400">{accountItem?.username}</span>
                                                    </td>
                                                    <td class="py-2 px-4 border-b border-b-gray-50 overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                        <span class="text-[13px] font-medium text-gray-400">{accountItem?.email}</span>
                                                    </td>
                                                    <td class="py-2 px-4 flex justify-start items-center border-b border-b-gray-50 overflow-auto gap-2" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                                                        <label className='flex cursor-pointer select-none items-center'>
                                                            <div className='relative'>
                                                                <input
                                                                    type='checkbox'
                                                                    checked={accountItem?.role === "admin"}
                                                                    onClick={()=>{
                                                                        handleCheckboxChange(accountItem?.role,accountItem?._id)
                                                                    }}
                                                                    className='sr-only'
                                                                />
                                                                <div
                                                                    className={`box block h-8 w-14 rounded-full ${accountItem?.role === "admin" ? 'bg-blue-400' : 'bg-pink-200'
                                                                        }`}
                                                                ></div>
                                                                <div
                                                                    className={`absolute left-1 top-1 flex h-6 w-6 items-center justify-center rounded-full bg-white transition ${accountItem?.role === "admin" ? 'translate-x-full' : ''
                                                                        }`}
                                                                ></div>
                                                            </div>
                                                        </label>
                                                        <span class="text-[13px] font-medium text-gray-400">Admin</span>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    )
}

export default AccountsPage