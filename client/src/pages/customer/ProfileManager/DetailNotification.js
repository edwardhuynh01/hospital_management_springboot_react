import React, { useEffect, useState } from "react";
import { IoCall } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { createAxios } from "../../../createInstance";
import { loginSuccess } from "../../../redux/authSlice";
import reportApi from "../../../api/reportApi";
import NotificationListDetail from "../../../components/ProfileManager/Notification/NotificationListDetail";
const DetailNotification = () => {
  const user = useSelector((state) => state.auth.login?.currentUser);
  const dispatch = useDispatch();
  const navigated = useNavigate();
  const accessToken = user?.accessToken;
  const axiosJWT = createAxios(user, dispatch, loginSuccess);
  const { id } = useParams();
  const [report, setReport] = useState();
  useEffect(() => {
    if (!user) {
      navigated("/login");
    }
  });
  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchRecord = async () => {
        const data = await reportApi.getReportById(
          axiosJWT,
          navigated,
          accessToken,
          id
        );
        // console.log(data);
        setReport(data.data);
        // console.log(report);
      };
      fetchRecord();
    } catch (error) {}
  }, []);
  return (
    <div className="bg-white">
      <div className="container mx-auto">
        <div className="flex flex-col xl:mx-64">
          <div className="h-auto w-auto mt-16 mb-6">
            <a
              className="py-2 text-xs px-4 border border-solid border-[#ff8fab] rounded-lg text-[#ff8fab] hover:text-white hover:bg-[#ff8fab]
                    transition duration-150 ease-in delay-150"
              href="/profile?component=medicalReports"
            >
              Danh sách phiếu khám
            </a>
          </div>
          <div className="bg-slate-100 flex flex-col">
            <div className="bg-[#ff8fab] py-2 px-4 font-semibold text-white">
              <p>Thông tin phiếu khám bệnh</p>
            </div>
            {report?.appointment &&
              (report.appointment.length > 1 &&
              report.appointment.length < 3 ? (
                <div className=" flex justify-center <=md:flex-col items-center">
                  {report.appointment.map((appointmentItem, index) => (
                    <NotificationListDetail
                      key={index}
                      isPayment={report?.isPayment}
                      check={true}
                      appointmentId={appointmentItem}
                      // amount={report?.amount}
                    />
                  ))}
                </div>
              ) : report.appointment.length === 1 ? (
                <div className=" flex justify-center <=md:flex-col items-center">
                  {report.appointment.map((appointmentItem, index) => (
                    <NotificationListDetail
                      key={index}
                      isPayment={report?.isPayment}
                      check={false}
                      appointmentId={appointmentItem}
                      // amount={report?.amount}
                    />
                  ))}
                </div>
              ) : (
                report.appointment.length >= 3 && (
                  <div className=" grid grid-cols-2 <=md:grid-cols-1 items-center">
                    {report.appointment.map((appointmentItem, index) => (
                      <NotificationListDetail
                        key={index}
                        isPayment={report?.isPayment}
                        check={true}
                        appointmentId={appointmentItem}
                        // amount={report?.amount}
                      />
                    ))}
                  </div>
                )
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailNotification;
