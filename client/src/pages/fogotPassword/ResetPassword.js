import React, { useEffect, useReducer, useState } from "react";
import styled from "styled-components";
import {
  //registerUser,
  resetPassword,
  resetPasswordShow,
} from "../../redux/apiRequest";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
const initialState = {
  password: "",
  CFpassword: "",
  isPasswordFocused: false,
  isCFPasswordFocused: false,
  checkPassword: false,
  checkCFPassword: false,
  checkCFPasswordFit: false,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "FOCUS_PASSWORD":
      return { ...state, isPasswordFocused: true };
    case "BLUR_PASSWORD":
      return { ...state, isPasswordFocused: false, checkPassword: true };
    case "FOCUS_CFPASSWORD":
      return { ...state, isCFPasswordFocused: true };
    case "BLUR_CFPASSWORD":
      return { ...state, isCFPasswordFocused: false, checkCFPassword: true };
    case "CHECK_FIT_CFPASSWORD_FALSE":
      return { ...state, checkCFPasswordFit: false };
    case "CHECK_FIT_CFPASSWORD_TRUE":
      return { ...state, checkCFPasswordFit: true };
    case "CHANGE_PASSWORD":
      return { ...state, password: action.payload };
    case "CHANGE_CFPASSWORD":
      return { ...state, CFpassword: action.payload };
    default:
      return state;
  }
};
const BtnSubmit = styled.button`
  width: 90%;
  height: 2.5em;
  border-radius: 8px;
  border: 1px solid #d1d1d1;
  background: rgba(255, 104, 159, 0.75);
  box-shadow: 0px 15px 30px -10px rgba(255, 189, 213, 0.3);
  color: #fff;
  text-align: center;
  font-size: 1em;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  transition: color 0.25s, border-color 0.25s, box-shadow 0.25s, transform 0.25s;
  cursor: pointer;
  padding: 0.5rem 1rem;
  padding-bottom: 2rem;
  align-items: center;
  margin-top: 30px;
  &:hover {
    border-color: rgba(255, 104, 159, 0.75);
    color: white;
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 104, 159);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
function ResetPassword(props) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [msg, setMsg] = useState();
  const [failed, setFailed] = useState(false);
  const [success, setSuccess] = useState(false);
  const handlePasswordFocus = () => {
    dispatch({ type: "FOCUS_PASSWORD" });
  };

  const handlePasswordBlur = () => {
    dispatch({ type: "BLUR_PASSWORD" });
  };
  const handleCFPasswordFocus = () => {
    dispatch({ type: "FOCUS_CFPASSWORD" });
  };

  const handleCFPasswordBlur = () => {
    dispatch({ type: "BLUR_CFPASSWORD" });
  };

  const handleFitCFPassword = () => {
    if (state.CFpassword !== state.password) {
      dispatch({ type: "CHECK_FIT_CFPASSWORD_TRUE" });
    } else {
      dispatch({ type: "CHECK_FIT_CFPASSWORD_FALSE" });
    }
  };
  const handlePasswordChange = (e) => {
    dispatch({ type: "CHANGE_PASSWORD", payload: e.target.value });
  };
  const handleCFPasswordChange = (e) => {
    dispatch({ type: "CHANGE_CFPASSWORD", payload: e.target.value });
  };
  const user = useSelector((state) => state.auth.login?.currentUser);
  const navigate = useNavigate();
  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const token = query.get("token");
  const email = query.get("email");
  useEffect(() => {
    const test = async () => {
      if (user) {
        navigate("/");
      } else {
        const err = await resetPasswordShow(token, email);
        if (err) {
          setMsg(err);
          setFailed(true);
          setSuccess(false);
        }
      }
    };
    test();
  });
  const handleResetPassword = async (e) => {
    e.preventDefault();
    try {
      if (!state.password) {
        // Hiển thị thông báo lỗi cho người dùng
        setMsg("Vui lòng cung cấp mật khẩu và xác nhận mật khẩu đó");
        return;
      }
      const obj = {
        password: state.password,
        confirmPassword: state.CFpassword,
      };
      const errorMessage = await resetPassword(obj, token, email);
      if (errorMessage) {
        // alert(errorMessage);
        setMsg(errorMessage);
        setFailed(true);
        setSuccess(false);
      } else {
        setMsg("Cập nhật mật khẩu thành công!");
        setFailed(true);
        setSuccess(true);
      }
    } catch (error) {
      setMsg(error.message);
      setFailed(true);
      setSuccess(false);
    }
  };

  const handleOnSubmit = () => {
    if (state.password === "") {
      dispatch({ type: "BLUR_PASSWORD" });
    }
    if (state.CFpassword === "") {
      dispatch({ type: "BLUR_CFPASSWORD" });
    }
  };
  const formLoginDiv =
    "flex flex-wrap text-[#818181] font-bold w-1/2 h-auto justify-center items-center rounded-3xl border-solid border-[#ffcbd94d] border bg-[#ffffffd9] shadow-md";
  return (
    <div className="flex justify-center items-center w-full h-full my-8">
      {failed ? (
        <div
          className={`my-5 ${
            success ? "bg-[#b7e4c7]" : "bg-[#ffc2d1]"
          } w-11/12 h-16 px-5 pt-5 py-4 text-center font-semibold rounded-md ${
            success ? "text-[#22577a]" : "text-[#fb6f92]"
          } `}
        >
          {msg}
        </div>
      ) : (
        <div className={formLoginDiv}>
          <h1 className="text-center text-[#FF689F] font-[600] py-0 px-[1em] pt-[1em] flex-grow text-4xl w-full ">
            Cập nhật mật khẩu
          </h1>
          <section className="mx-12 flex-grow p-12 ">
            <form action="" onSubmit={handleResetPassword}>
              <div className="mb-4 relative">
                <input
                  id="Input_Password"
                  type="password"
                  className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                  onFocus={handlePasswordFocus}
                  onBlur={handlePasswordBlur}
                  onChange={handlePasswordChange}
                  value={state.password}
                />
                <label
                  htmlFor="Input_Password"
                  className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${
                    state.isPasswordFocused || state.password
                      ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                      : ""
                  }`}
                >
                  Mật khẩu
                </label>
              </div>
              <p className="text-red-600 font-semibold mx-2 pb-4">
                {state.checkPassword &&
                  !state.password &&
                  "Mật khẩu không được để trống"}
              </p>
              <p className="text-red-600 font-semibold mx-2 pb-4"></p>
              <div className="mb-4 relative">
                <input
                  id="Input_CFPassword"
                  type="password"
                  className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                  onFocus={handleCFPasswordFocus}
                  onBlur={handleCFPasswordBlur}
                  onChange={handleCFPasswordChange}
                  onKeyUp={handleFitCFPassword}
                  value={state.CFpassword}
                />
                <label
                  htmlFor="Input_CFPassword"
                  className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${
                    state.isCFPasswordFocused || state.CFpassword
                      ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                      : ""
                  }`}
                >
                  Xác nhận mật khẩu
                </label>
              </div>
              <p className="text-red-600 font-semibold mx-2 pb-4">
                {state.checkCFPassword &&
                  !state.CFpassword &&
                  "Xác nhận mật khẩu không được để trống"}
              </p>
              <p className="text-red-600 font-semibold mx-2 pb-4">
                {state.checkCFPasswordFit &&
                  state.CFpassword &&
                  "Mật khẩu không trùng khớp"}
              </p>
              <div className="text-center">
                <BtnSubmit onClick={handleOnSubmit} type="submit">
                  Xác nhận
                </BtnSubmit>
              </div>
            </form>
            <div className="text-start text-red-600 font-semibold mt-6 px-3">
              {msg}
            </div>
          </section>
        </div>
      )}
    </div>
  );
}

export default ResetPassword;
