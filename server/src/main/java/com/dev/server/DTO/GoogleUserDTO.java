package com.dev.server.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoogleUserDTO {
    private String aud;
    private String azp;
    private String email;
    private boolean email_verified;
    private long exp;
    private String family_name;
    private String given_name;
    private long iat;
    private String iss;
    private String jti;
    private String name;
    private long nbf;
    private String picture;
    private String sub;
}
