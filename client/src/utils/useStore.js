import create from 'zustand';

const DEFAULT_STATE = 'Thống Kê'

const useStore = create((set) => ({
    selectedItem: DEFAULT_STATE,
    setSelectedItem: (item) => set({ selectedItem: item }),
}));

export default useStore;
//change title stack when navigate into admin