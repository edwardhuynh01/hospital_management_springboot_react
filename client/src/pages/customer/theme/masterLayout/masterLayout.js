import React from "react";
import Footer from "../footer/footer";
import Header from "../header/header";

const MasterLayout = ({children,...props}) =>{
    return (
        <div {...props}>
            <Header></Header>
            {children}
            <Footer></Footer>
        </div>
    );
};
export default MasterLayout;