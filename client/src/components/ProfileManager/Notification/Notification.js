import React from "react";

function Notification(props) {
  const { reportId, createdAt } = props;
  const timeDifference = (dateString) => {
    const date = new Date(dateString);
    const now = new Date();
    const diffInSeconds = Math.floor((now - date) / 1000);

    if (diffInSeconds < 60) {
      return `${diffInSeconds} giây trước`;
    } else if (diffInSeconds < 3600) {
      const minutes = Math.floor(diffInSeconds / 60);
      return `${minutes} phút trước`;
    } else if (diffInSeconds < 86400) {
      const hours = Math.floor(diffInSeconds / 3600);
      return `${hours} tiếng trước`;
    } else {
      const days = Math.floor(diffInSeconds / 86400);
      return `${days} ngày trước`;
    }
  };
  const encode = (id) => {
    return btoa(id);
  };
  return (
    <div className="flex gap-2">
      <div className="flex flex-col items-center ">
        <div className="bg-[#ff8fab] h-4 rounded-full w-4 border-2 border-solid border-[#ff8fab] hover:bg-white"></div>
        <div className=" w-[2px] h-24 bg-slate-200"></div>
      </div>
      <div className="w-full">
        <p className="text-sm font-light text-slate-400">{timeDifference(createdAt)}</p>
        <p className="my-2 text-lg ">Bạn đã đặt khám thành công. Mã phiếu khám của bạn là: {reportId}</p>
        <a className="text-end cursor-pointer text-slate-400" href={`/DetailNotification/${encode(reportId)}`}><p className="mr-4">Xem chi tiết...</p></a>
      </div>
    </div>
  );

}

export default Notification;
