package com.dev.server.repository;

import com.dev.server.model.Degree;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DegreeRepository extends MongoRepository<Degree, String> {}
