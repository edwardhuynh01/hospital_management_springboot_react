import React, { useEffect, useState } from "react";
import Title from "../Title/Title";
import Service from "../../api/servicesApi";
import { Link } from "react-router-dom";
import Loading from "../Loading/Loading";
import LoadingEdit from "../Loading/LoadingEdit";

const HomeService = (props) => {
  const [serviceList, setServiceList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    const fetchService = async () => {
      try {
        const data = await Service.getAllService();
        setServiceList(data);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
      finally {
        setIsLoading(false);
      }
    };
    fetchService();
  }, []);
  const encode = (id) => {
    return btoa(id);
  };
  return (
    <section className={`p-5 bg-white ${props.className || ""}`}>
      <div className="container mx-auto">
        <Title>CÁC DỊCH VỤ CHÍNH</Title>
        {
          isLoading ? (
            <LoadingEdit />
          ) : (
            <div className="grid lg:grid-cols-5 grid-cols-1 gap-6 mt-6 ">
              {serviceList.slice(0, 5).map((serviceItem) => (
                <div className="flex flex-col items-center">
                  <Link
                    to={`/service/${encode(serviceItem._id)}`}
                    className="h-52 lg:h-auto w-full lg:w-auto rounded-lg overflow-hidden"
                  >
                    <img
                      className="h-full w-full rounded-lg transition-transform duration-300 transform hover:scale-110"
                      src={`data:image/png;base64,${serviceItem?.mainImage}`}
                      alt={serviceItem.name}
                    />
                  </Link>
                  <Link
                    to={`/service/${encode(serviceItem._id)}`}
                    className="text-xl font-semibold my-2 text-[#9d4edd]"
                  >
                    {serviceItem.name.toUpperCase()}
                  </Link>
                </div>
              ))}
            </div>
          )
        }
      </div>
    </section>
  );
};

export default HomeService;
