import React from "react";

const HeaderButton = (props) => {
  return (
    <a
      href={props.href}
      onClick={props.onClick}
      className="cursor-pointer hover:bg-main-gradient-to-right text-white w-auto mx-4 px-10 py-2 text-lg font-bold bg-main-gradient-to-left rounded-lg <=lg:mx-0 <=lg:my-2 <=lg:px-2"
    >
      {props.children}
    </a>
  );
};

export default HeaderButton;
