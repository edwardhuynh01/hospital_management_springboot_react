import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import specialistApi from "../../../api/specialistApi";
import Title from "../../../components/Title/Title";

const SpecialistPageDetail = () => {
  const [specialist, setSpecialist] = useState();
  const { id } = useParams();

  useEffect(() => {
    const fetchNews = async () => {
      try {
        const response = await specialistApi.getSpecialistById(id);
        setSpecialist(response);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
    };

    fetchNews();
  }, [id]);
  return (
    <div className="bg-white">
      <Title>{specialist?.specialistName.toUpperCase()}</Title>
      <div className="container mx-auto px-20 py-10">
        <div className="mx-20 ">
          <div
            className="py-4"
            dangerouslySetInnerHTML={{
              __html: specialist?.specialistDescription,
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default SpecialistPageDetail;
