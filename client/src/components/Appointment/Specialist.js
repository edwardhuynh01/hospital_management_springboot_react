import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
const Container = styled.div`
  transition: all ease-in-out 0.2s;
`;
function Specialist(props) {
  const { specId } = props;
  return (
    <Container className="w-full h-full py-4 hover:text-[#ffb3c6] cursor-pointer" onClick={props.chooseSpecialist}>
      <Link to={`/ChooseDoctor`} className="uppercase">
        {props.children}
      </Link>
    </Container>
  );
}

export default Specialist;
