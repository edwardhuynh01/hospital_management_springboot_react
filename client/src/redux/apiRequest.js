import axios from "axios";
import {
  loginFalse,
  loginStart,
  loginSuccess,
  logoutFalse,
  logoutStart,
  logoutSuccess,
  registerFalse,
  registerStart,
  registerSuccess,
} from "./authSlice";
import {
  deleteUserFailed,
  deleteUserStart,
  deleteUserSuccess,
  getUsersFailed,
  getUsersStart,
  getUsersSuccess,
} from "./userSlice";
import { clearAppointment } from "./appointmentSlice";
export const loginUser = async (user, dispatch, navigate) => {
  dispatch(loginStart());
  try {
    const res = await axios.post("http://localhost:5000/api/auth/login", user, {
      withCredentials: true,
    });
    dispatch(loginSuccess(res.data));
    return res.data;
  } catch (error) {
    dispatch(loginFalse(error.response.data));
    return error.response?.data;
  }
};
export const loginGG = async (user, dispatch, navigate) => {
  dispatch(loginStart());
  try {
    const res = await axios.post(
      "http://localhost:5000/api/auth/loginGG",
      user,
      {
        withCredentials: true,
      }
    );
    dispatch(loginSuccess(res.data));
    return res.data;
  } catch (error) {
    dispatch(loginFalse(error.response.data));
    return error.response?.data;
  }
};
export const registerUser = async (user, dispatch, navigate) => {
  dispatch(registerStart());
  try {
    const res = await axios.post(
      "http://localhost:5000/api/auth/register",
      user
    );
    if (res) {
      console.log(res);
      return res;
    } else {
      dispatch(registerSuccess());
      navigate("/login");
    }
  } catch (error) {
    dispatch(registerFalse(error.response.data));
    return error.response?.data;
  }
};
export const getAllUsers = async (accessToken, dispatch) => {
  dispatch(getUsersStart());
  try {
    const res = await axios.get("http://localhost:5000/api/user", {
      headers: { token: `Bearer ${accessToken}` },
    });
    dispatch(getUsersSuccess(res.data));
  } catch (error) {
    dispatch(getUsersFailed());
  }
};

export const deleteUser = async (accessToken, dispatch, id) => {
  dispatch(deleteUserStart());
  try {
    const res = await axios.delete("http://localhost:5000/api/user" + id, {
      headers: { token: `Bearer ${accessToken}` },
    });
    dispatch(deleteUserSuccess(res.data));
  } catch (error) {
    dispatch(deleteUserFailed(error.response.data));
  }
};

export const logout = async (dispatch, id, navigate, accessToken, axiosJWT) => {
  dispatch(logoutStart());
  try {
    await axiosJWT.post("http://localhost:5000/api/auth/logout", id, {
      headers: {
        token: `Bearer ${accessToken}`,
      },
    });
    dispatch(logoutSuccess());
    dispatch(clearAppointment());
    navigate("/login");
  } catch (error) {
    dispatch(logoutFalse(error.response?.data));
  }
};
export const forgotPassword = async (email) => {
  try {
    await axios.post("http://localhost:5000/api/auth/forgot", email);
  } catch (error) {
    return error.response?.data;
  }
};
export const resetPasswordShow = async (token, email) => {
  try {
    await axios.get(
      `http://localhost:5000/api/auth/reset?token=${token}&email=${email}`
    );
  } catch (error) {
    return error.response?.data;
  }
};
export const resetPassword = async (password, token, email) => {
  try {
    await axios.post(
      `http://localhost:5000/api/auth/reset?token=${token}&email=${email}`,
      password
    );
  } catch (error) {
    return error.response?.data;
  }
};
export const confirmEmail = async (token, email) => {
  try {
    await axios.get(
      `http://localhost:5000/api/auth/confirmEmail?token=${token}&email=${email}`
    );
  } catch (error) {
    return error.response?.data;
  }
};
export const PaymentReport = async (data) => {
  try {
    const response = await axios.post(
      "http://localhost:5000/api/appointment/create-payment-link",
      data
    );
    return response.data; // Trả về URL từ API
  } catch (error) {
    return error.response;
  }
};
export const successPayment = async (
  paymentToken,
  paymentParams,
  id,
  dispatch
) => {
  try {
    const queryParams = new URLSearchParams(paymentParams).toString();
    const url = `http://localhost:5000/api/appointment/success?${queryParams}`;
    const res = await axios.get(url, {
      headers: {
        tokenpayment: `Bearer ${paymentToken}`,
      },
    });
    dispatch(clearAppointment());
    // console.log(res);
    return res;
  } catch (error) {
    return error.response;
  }
};
export const makeReportNonPayment = async (dataNonPayment, dispatch) => {
  try {
    const response = await axios.post(
      "http://localhost:5000/api/appointment/MakeAppointmentNonPayment",
      dataNonPayment
    );
    dispatch(clearAppointment());
    console.log(response);
    return response; // Trả về URL từ API
  } catch (error) {
    console.log(error);
    return error.response;
  }
};
