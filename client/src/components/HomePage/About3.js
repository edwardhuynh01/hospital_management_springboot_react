import React from 'react';
import Title from '../Title/Title';
const About3 = (props) => {
    const divMainItem = 'mx-20';
    const divImageClass = 'h-[124px] w-[124px] flex justify-center item-center';
    const divImgClass = 'rounded-full';
    const divTextClass = 'text-center text-[18px] font-bold text-[#9d4edd] my-2';
    return (
        <section className={`bg-white pb-10 ${props.className}`}>
            <div className="container mx-auto">
                <Title>GIÁ TRỊ CỐT LÕI</Title>
                <div className="flex flex-wrap justify-center mx-auto mt-8" >
                    <div className={divMainItem}>
                        <div className={divImageClass}>
                            <img className={divImgClass} src='/Images/tienphong.png' alt="tiên phong" />
                        </div>
                        <div className={divTextClass}>
                            <p>TIÊN PHONG</p>
                        </div>
                    </div>
                    <div className={divMainItem}>
                        <div className={divImageClass}>
                            <img className={divImgClass} src='/Images/brain.png' alt="thấu hiểu" />
                        </div>
                        <div className={divTextClass}>
                            <p>THẤU HIỂU</p>
                        </div>
                    </div>
                    <div className={divMainItem}>
                        <div className={divImageClass}>
                            <img className={divImgClass} src='/Images/certificate.png' alt="chuẩn mực" />
                        </div>
                        <div className={divTextClass}>
                            <p>CHUẨN MỰC</p>
                        </div>
                    </div>
                    <div className={divMainItem}>
                        <div className={divImageClass}>
                            <img className={divImgClass} src='/Images/security.png' alt="an toàn" />
                        </div>
                        <div className={divTextClass}>
                            <p>AN TOÀN</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default About3;