package com.dev.server.DTO;

import com.dev.server.model.Appointment;
import com.dev.server.model.Report;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDataDTO {
    private List<Appointment> listAppointment;
    private Report report;
    private String paymentLinkId;
}
