import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import newsApi from '../../../api/newsApi';
import servicesApi from '../../../api/servicesApi';
import { server } from 'ionicons/icons';

const SearchPage = () => {
    const { query } = useParams();
    const [newsList, setNewsList] = useState([]);
    const [serviceList, setServiceList] = useState([]);
    const [filteredNews, setFilteredNews] = useState([]);
    const [filteredServices, setFilteredServices] = useState([]);
    useEffect(() => {
        const fetchNews = async () => {
            try {
                const response = await newsApi.getAllNews();
                setNewsList(response);
            } catch (error) {
                console.error('Error fetching news:', error);
            }
        };
        const fetchServices = async () => {
            try {
                const data = await servicesApi.getAllService();
                setServiceList(data);
            } catch (error) {
                console.error('Error fetching services:', error);
            }
        };
        fetchNews();
        fetchServices();
    }, []);

    useEffect(() => {
        console.log(query)
        if (query === "all") {
            const filteredNews = newsList;
            setFilteredNews(filteredNews);
            const filteredServices = serviceList;
            setFilteredServices(filteredServices);
        } else {
            const filteredNews = newsList.filter(news => news.title.toLowerCase().trim().includes(query.toLowerCase().trim()));
            setFilteredNews(filteredNews);
            const filteredServices = serviceList.filter(service => service.name.toLowerCase().trim().includes(query.toLowerCase().trim()));
            setFilteredServices(filteredServices);
        }

    }, [query, newsList, serviceList]);
    const encode = (id) => {
        return btoa(id);
    }
    const isHaveBoth = Array.isArray(filteredServices) && Array.isArray(filteredNews) && filteredServices.length > 0 && filteredNews.length > 0;
    const isServiceNull = Array.isArray(filteredServices) && filteredServices.length > 0;
    const isNewsNull = Array.isArray(filteredNews) && filteredNews.length > 0;
    const isNullBoth = filteredServices.length === 0 && filteredNews.length === 0;
    return (
        <div className=' bg-slate-100'>
            {
                query === "all" && (
                    <>
                        <div className="grid md:grid-cols-2 grid-cols-1 mx-10 pb-10 pt-6">
                            <div>
                                <h2 className='text-4xl font-bold text-center my-4 text-[#5a189a]' >SERVICES</h2>
                                {
                                    filteredServices?.map((serviceItem) => (
                                        <div div className="mx-auto  p-4 border-r-[2px] border-solid border-r-[#cdb4db]" id="col" key={serviceItem._id}>
                                            <div className="flex flex-col items-center bg-white shadow-lg rounded-lg overflow-hidden">
                                                <Link className='w-full h-auto' to={`/service/${encode(serviceItem._id)}`} id="img-main">
                                                    <img className="mx-auto w-full h-auto" src={`data:image/png;base64,${serviceItem.mainImage}`} alt={serviceItem.name} />
                                                </Link>
                                                <div className="text-start sm:text-left p-4" id="text-content">
                                                    <Link to={`/service/${encode(serviceItem._id)}`} className="text-xl font-bold leading-tight mb-2">{serviceItem.name}</Link>
                                                    <p className="text-sm text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{serviceItem.teaser}</p>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                            <div>
                                <h2 className='text-4xl font-bold text-center my-4 text-[#5a189a]'>NEWS</h2>
                                {
                                    filteredNews?.map((newsItem) => (
                                        <div className="mx-auto p-4" id="col" key={newsItem.id}>
                                            <div className="flex flex-col items-center bg-white shadow-lg rounded-lg overflow-hidden">
                                                <Link className='w-full h-auto' to={`/news/${encode(newsItem._id)}`} id="img-main">
                                                    <img className="mx-auto w-full h-auto" src={`data:image/png;base64,${newsItem.mainImage}`} alt={newsItem.title} />
                                                </Link>
                                                <div className="text-start sm:text-left p-4 h-full w-full" id="text-content">
                                                    <Link to={`/news/${encode(newsItem._id)}`} className="text-xl font-bold leading-tight mb-2">{newsItem.title}</Link>
                                                    <p className="text-sm text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{newsItem.teaser}</p>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                    </>
                )
            }
            {
                isNullBoth && (
                    <>
                        <div className='flex flex-col justify-center items-center bg-white'>
                            <iframe src="https://giphy.com/embed/gfO3FcnL8ZK9wVgr6t" width="480" height="403" class="giphy-embed pointer-events-none" allowFullScreen></iframe>
                            <p className='px-5 pb-6 h-full w-full text-center font-sans font-medium text-3xl'>Không tồn tại dịch vụ và tin tức cần tìm :(</p>
                        </div>
                    </>
                )
            }
            {isHaveBoth && (
                <>
                    <div className="grid md:grid-cols-2 grid-cols-1 mx-10 pb-10 pt-6">
                        <div>
                            <h2 className='text-4xl font-bold text-center my-4 text-[#5a189a]' >SERVICES</h2>
                            {
                                filteredServices?.map((serviceItem) => (
                                    <div div className="mx-auto  p-4 border-r-[2px] border-solid border-r-[#cdb4db]" id="col" key={serviceItem._id}>
                                        <div className="flex flex-col items-center bg-white shadow-lg rounded-lg overflow-hidden">
                                            <Link className='w-full h-auto' to={`/service/${encode(serviceItem._id)}`} id="img-main">
                                                <img className="mx-auto w-full h-auto" src={`data:image/png;base64,${serviceItem.mainImage}`} alt={serviceItem.name} />
                                            </Link>
                                            <div className="text-start sm:text-left p-4" id="text-content">
                                                <Link to={`/service/${encode(serviceItem._id)}`} className="text-xl font-bold leading-tight mb-2">{serviceItem.name}</Link>
                                                <p className="text-sm text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{serviceItem.teaser}</p>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                        <div>
                            <h2 className='text-4xl font-bold text-center my-4 text-[#5a189a]'>NEWS</h2>
                            {
                                filteredNews?.map((newsItem) => (
                                    <div className="mx-auto p-4" id="col" key={newsItem.id}>
                                        <div className="flex flex-col items-center bg-white shadow-lg rounded-lg overflow-hidden">
                                            <Link className='w-full h-auto' to={`/news/${encode(newsItem._id)}`} id="img-main">
                                                <img className="mx-auto w-full h-auto" src={`data:image/png;base64,${newsItem.mainImage}`} alt={newsItem.title} />
                                            </Link>
                                            <div className="text-start sm:text-left p-4 h-full w-full" id="text-content">
                                                <Link to={`/news/${encode(newsItem._id)}`} className="text-xl font-bold leading-tight mb-2">{newsItem.title}</Link>
                                                <p className="text-sm text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{newsItem.teaser}</p>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </>
            )}

            {
                !isHaveBoth && (
                    <>
                        {isServiceNull && (
                            <>
                                <div className="pt-6">
                                    <h2 className='text-4xl font-bold text-center my-4 text-[#5a189a]' >SERVICES</h2>
                                    {filteredServices.map((serviceItem) => (
                                        <div className="flex w-auto flex-col lg:flex-row m-10 rounded-xl justify-center overflow-hidden shadow-lg" id="col">
                                            <Link to={`/service/${encode(serviceItem._id)}`} className="lg:w-1/2 w-full" id="img-main">
                                                <img className="mx-auto w-full h-full" src={`data:image/png;base64,${serviceItem.mainImage}`} alt={serviceItem.name} />
                                            </Link>
                                            <div className="lg:w-1/2 w-full bg-[#f8f9fa]">
                                                <div className=" text-start pt-[60px] pr-[30px] pb-[30px] pl-[45px] mb-0 lg:mb-10">
                                                    <Link to={`/service/${encode(serviceItem._id)}`} className="text-2xl font-bold leading-tight mb-6">{serviceItem.name}</Link>
                                                    <p className="text-md text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{serviceItem.teaser}</p>
                                                </div>
                                                <div className="mb-10">
                                                    <Link to={`/service/${encode(serviceItem._id)}`} className="cursor-pointer mx-[45px] bg-[#FB6F92] py-2 px-6 rounded-md text-white
                             hover:text-[#FB6F92] hover:bg-white border border-solid hover:border-[#FB6F92] ">
                                                        Xem chi tiết
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </>
                        )}
                        {
                            isNewsNull && (
                                <>
                                    <div className="pt-6">
                                        <h2 className='text-4xl font-bold text-center my-4 text-[#5a189a]'>NEWS</h2>
                                        {
                                            filteredNews?.map((newsItem) => (
                                                <div className="flex w-auto flex-col lg:flex-row m-10 rounded-xl justify-center overflow-hidden shadow-lg" id="col">
                                                    <Link to={`/news/${encode(newsItem._id)}`} className="lg:w-1/2 w-full" id="img-main">
                                                        <img className="mx-auto w-full h-full" src={`data:image/png;base64,${newsItem.mainImage}`} alt={newsItem.title} />
                                                    </Link>
                                                    <div className="lg:w-1/2 w-full bg-[#f8f9fa]">
                                                        <div className=" text-start pt-[60px] pr-[30px] pb-[30px] pl-[45px] mb-0 lg:mb-10">
                                                            <Link to={`/news/${encode(newsItem._id)}`} className="text-2xl font-bold leading-tight mb-6">{newsItem.title}</Link>
                                                            <p className="text-md text-gray-600 overflow-hidden whitespace-pre-line line-clamp-4">{newsItem.teaser}</p>
                                                        </div>
                                                        <div className="mb-10">
                                                            <Link to={`/news/${encode(newsItem._id)}`} className="cursor-pointer mx-[45px] bg-[#FB6F92] py-2 px-6 rounded-md text-white
                                 hover:text-[#FB6F92] hover:bg-white border border-solid hover:border-[#FB6F92] ">
                                                                Xem chi tiết
                                                            </Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))
                                        }
                                    </div>
                                </>
                            )
                        }
                    </>
                )
            }
        </div>
    );
};

export default SearchPage;