import React, { useEffect, useState } from "react";
import styled from "styled-components";
import {
  IoDocumentTextOutline,
  IoHelpCircleOutline,
  IoReaderOutline,
  IoTrashOutline,
  IoAlarmOutline
} from "react-icons/io5";
import specialistApi from "../../api/specialistApi";
import doctorApi from "../../api/doctorApi";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import { deleteSpecialist } from "../../redux/appointmentSlice";
import { confirmAlert } from "react-confirm-alert";
import { toast } from "react-toastify";
const Delete = styled.button`
  transition: ease-in-out all 0.2s;
  font-weight: 600;
  color: #4a4e69;
  border-radius: 4px;
  cursor: pointer;
  &:hover {
    background-color: rgba(144, 224, 239, 0.2);
    color: #48cae4;
  }
`;
function MedicalExaminationInformation(props) {
  const { item, stt } = props;
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const encode = (id) => {
    return btoa(id);
  };
  const [specialist, setSpecialist] = useState();
  const [doctor, setDoctor] = useState();
  const [refresh, setRefresh] = useState(false);
  useEffect(() => {
    try {
      const fetctSpec = async () => {
        const data = await specialistApi.getSpecialistById(
          encode(item?.specialistId)
        );
        setSpecialist(data);
      };
      const fetchDoctor = async () => {
        const data = await doctorApi.getDoctorById(
          axiosJWT,
          navigated,
          accessToken,
          item?.doctorId
        );
        setDoctor(data.data);
        return data.data;
      };
      fetctSpec();
      fetchDoctor();
    } catch (error) {
      throw error;
    }
  }, [item]);
  const removeSpecialist = async () => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className="bg-slate-100 rounded-lg shadow-xl overflow-hidden md:w-[600px]">
          <p className="mb-4 flex items-center justify-start gap-1 px-4 py-2 bg-[#ff8fab] text-white">
            <IoHelpCircleOutline className="text-2xl" />
            Bạn có muốn xóa thông tin đặt khám ?
          </p>
          <ul className="px-4 py-2 border-b border-solid border-b-slate-300">
            <li className="flex justify-start items-center mb-2">
              <p className="flex justify-start w-40 gap-1 items-center text-sm">
                <IoReaderOutline className="text-lg" />
                Chuyên khoa
              </p>
              <p className="text-md font-medium text-[#ff8fab]">
                {specialist?.specialistName?.toUpperCase()}
              </p>
            </li>
            <li className="flex justify-start items-center mb-2">
              <p className="flex justify-start w-40 gap-1 items-center text-sm">
                <IoDocumentTextOutline className="text-lg" />
                Dịch vụ
              </p>
              <p className="text-md font-medium text-[#ff8fab]">Khám dịch vụ</p>
            </li>
            <li className="flex justify-start items-center mb-2">
              <p className="flex justify-start w-40 gap-1 items-center text-sm">
                <IoAlarmOutline className="text-lg" />
                Ngày khám
              </p>
              <p className="text-md font-medium text-[#ff8fab]">{`${item?.day}/${item?.month} (${item?.timeStart}-${item?.timeEnd})`}</p>
            </li>
          </ul>
          <div className="flex justify-end gap-4 m-6">
            <button
              onClick={async () => {
                try {
                  await DISPATCH(
                    deleteSpecialist({ specialistId: specialist?._id })
                  );
                  setRefresh(!refresh);
                  // window.location.reload();
                  toast("Xóa hồ sơ thành công!");
                } catch (error) {
                  toast.error("Đã xảy ra lỗi khi xóa hồ sơ.");
                  console.error("Error deleting record:", error);
                }
                onClose();
              }}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-red-600 duration-300 bg-red-400 text-white px-8 py-2 rounded-lg mr-2"
            >
              Có
            </button>
            <button
              onClick={onClose}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-gray-400 duration-300 bg-gray-300 text-black px-4 py-2 rounded-lg"
            >
              Không
            </button>
          </div>
        </div>
      ),
    });
  };
  return (
    <tr className="border-t border-solid">
      <td className="text-start px-3 py-4">{stt}</td>
      <td className="text-start px-3 py-4">{specialist?.specialistName}</td>
      <td className="text-start px-3 py-4">Khám dịch vụ</td>
      <td className="text-start px-3 py-4">{doctor?.name}</td>
      <td className="text-start px-3 py-4">{item?.timeStart}</td>
      <td className="text-start px-3 py-4">{`${specialist?.price}đ`}</td>
      <td className="text-center px-3 py-4">
        <Delete
          className="flex justify-center items-center w-16 lg:w-24 h-11 pr-2 text-xs lg:text-sm"
          onClick={removeSpecialist}
        >
          <div className="mx-2">
            <IoTrashOutline></IoTrashOutline>
          </div>
          <h3 className="text[#343a40]">Xóa</h3>
        </Delete>
      </td>
      <td></td>
    </tr>
  );
}

export default MedicalExaminationInformation;
