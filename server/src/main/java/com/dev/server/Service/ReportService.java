package com.dev.server.Service;

import com.dev.server.model.Report;
import com.dev.server.repository.ReportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class ReportService {
    private final ReportRepository reportRepository;

    public Report createReport(Report report) {
        try {
            return reportRepository.save(report);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi tạo báo cáo: " + e.getMessage(), e);
        }
    }

    public List<Report> getAllReports() {
        try {
            return reportRepository.findAll();
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy danh sách tất cả báo cáo: " + e.getMessage(), e);
        }
    }

    public Optional<Report> getReportById(String id) {
        try {
            return reportRepository.findById(id);
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi lấy thông tin báo cáo với id: " + id, e);
        }
    }

    public Report updateReport(String id, Report reportDetails) {
        try {
            Optional<Report> optionalReport = reportRepository.findById(id);

            if (optionalReport.isPresent()) {
                Report existingReport = optionalReport.get();
                existingReport.setAccount(reportDetails.getAccount());
                existingReport.setRecord(reportDetails.getRecord());
                existingReport.setAppointment(reportDetails.getAppointment());
                existingReport.setCreatedAt(reportDetails.getCreatedAt());
                existingReport.setUpdatedAt(reportDetails.getUpdatedAt());
                existingReport.setAmount(reportDetails.getAmount());
                existingReport.setPayment(reportDetails.isPayment());
                existingReport.setPaymentId(reportDetails.getPaymentId());
                return reportRepository.save(existingReport);
            } else {
                throw new RuntimeException("Không tìm thấy báo cáo với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi cập nhật báo cáo: " + e.getMessage(), e);
        }
    }

    public boolean deleteReport(String id) {
        try {
            Optional<Report> optionalReport = reportRepository.findById(id);

            if (optionalReport.isPresent()) {
                reportRepository.deleteById(id);
                return true;
            } else {
                throw new RuntimeException("Không tìm thấy báo cáo với id: " + id);
            }
        } catch (Exception e) {
            throw new RuntimeException("Lỗi khi xóa báo cáo với id: " + id, e);
        }
    }
}
