import React, { useEffect, useReducer, useState } from "react";
import styled from "styled-components";
import { registerUser } from "../../redux/apiRequest";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import LoadingEdit from "../../components/Loading/LoadingEdit";
const initialState = {
  fullname: "",
  phonenum: "",
  email: "",
  password: "",
  CFpassword: "",
  isFullNameFocused: false,
  isPhoneNumFocused: false,
  isEmailFocused: false,
  isPasswordFocused: false,
  isCFPasswordFocused: false,
  checkFullName: false,
  checkPhoneNum: false,
  checkFormatPhoneNum: false,
  checkEmail: false,
  checkFormatEmail: false,
  checkPassword: false,
  checkCFPassword: false,
  checkCFPasswordFit: false,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "FOCUS_FULLNAME":
      return { ...state, isFullNameFocused: true };
    case "BLUR_FULLNAME":
      return { ...state, isFullNameFocused: false, checkFullName: true };
    case "FOCUS_PHONENUM":
      return { ...state, isPhoneNumFocused: true };
    case "BLUR_PHONENUM":
      return { ...state, isPhoneNumFocused: false, checkPhoneNum: true };
    case "CHECK_FORMAT_PHONENUM_FALSE":
      return { ...state, checkFormatPhoneNum: false };
    case "CHECK_FORMAT_PHONENUM_TRUE":
      return { ...state, checkFormatPhoneNum: true };
    case "FOCUS_EMAIL":
      return { ...state, isEmailFocused: true };
    case "BLUR_EMAIL":
      return { ...state, isEmailFocused: false, checkEmail: true };
    case "CHECK_FORMAT_EMAIL_FALSE":
      return { ...state, checkFormatEmail: false };
    case "CHECK_FORMAT_EMAIL_TRUE":
      return { ...state, checkFormatEmail: true };
    case "FOCUS_PASSWORD":
      return { ...state, isPasswordFocused: true };
    case "BLUR_PASSWORD":
      return { ...state, isPasswordFocused: false, checkPassword: true };
    case "FOCUS_CFPASSWORD":
      return { ...state, isCFPasswordFocused: true };
    case "BLUR_CFPASSWORD":
      return { ...state, isCFPasswordFocused: false, checkCFPassword: true };
    case "CHECK_FIT_CFPASSWORD_FALSE":
      return { ...state, checkCFPasswordFit: false };
    case "CHECK_FIT_CFPASSWORD_TRUE":
      return { ...state, checkCFPasswordFit: true };
    case "CHANGE_FULLNAME":
      return { ...state, fullname: action.payload };
    case "CHANGE_PHONENUM":
      return { ...state, phonenum: action.payload };
    case "CHANGE_EMAIL":
      return { ...state, email: action.payload };
    case "CHANGE_PASSWORD":
      return { ...state, password: action.payload };
    case "CHANGE_CFPASSWORD":
      return { ...state, CFpassword: action.payload };
    default:
      return state;
  }
};
const BtnSubmit = styled.button`
  width: 90%;
  height: 2.5em;
  border-radius: 8px;
  border: 1px solid #d1d1d1;
  background: rgba(255, 104, 159, 0.75);
  box-shadow: 0px 15px 30px -10px rgba(255, 189, 213, 0.3);
  color: #fff;
  text-align: center;
  font-size: 1em;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  transition: color 0.25s, border-color 0.25s, box-shadow 0.25s, transform 0.25s;
  cursor: pointer;
  padding: 0.5rem 1rem;
  padding-bottom: 2rem;
  align-items: center;
  margin-top: 30px;
  &:hover {
    border-color: rgba(255, 104, 159, 0.75);
    color: white;
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 104, 159);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
function Register() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [msg, setMsg] = useState();
  const handleFullNameFocus = () => {
    dispatch({ type: "FOCUS_FULLNAME" });
  };

  const handleFullNameBlur = () => {
    dispatch({ type: "BLUR_FULLNAME" });
  };
  const handlePhoneNumFocus = () => {
    dispatch({ type: "FOCUS_PHONENUM" });
  };

  const handlePhoneNumBlur = () => {
    dispatch({ type: "BLUR_PHONENUM" });
  };
  const handleEmailFocus = () => {
    dispatch({ type: "FOCUS_EMAIL" });
  };

  const handleEmailBlur = () => {
    dispatch({ type: "BLUR_EMAIL" });
  };

  const handlePasswordFocus = () => {
    dispatch({ type: "FOCUS_PASSWORD" });
  };

  const handlePasswordBlur = () => {
    dispatch({ type: "BLUR_PASSWORD" });
  };
  const handleCFPasswordFocus = () => {
    dispatch({ type: "FOCUS_CFPASSWORD" });
  };

  const handleCFPasswordBlur = () => {
    dispatch({ type: "BLUR_CFPASSWORD" });
  };
  const handleFullNameChange = (e) => {
    dispatch({ type: "CHANGE_FULLNAME", payload: e.target.value });
  };
  const handlePhoneNumChange = (event) => {
    const { value } = event.target;
    if (/^\d*$/.test(value)) {
      dispatch({ type: "CHANGE_PHONENUM", payload: event.target.value });
    }
  };
  const handleFormatPhoneNumCheck = () => {
    if (!/^0\d{9}$/.test(state.phonenum)) {
      dispatch({ type: "CHECK_FORMAT_PHONENUM_TRUE" });
    } else {
      dispatch({ type: "CHECK_FORMAT_PHONENUM_FALSE" });
    }
  };
  const handleFormatEmailCheck = () => {
    // Mẫu regex để kiểm tra định dạng email
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

    if (!emailRegex.test(state.email)) {
      dispatch({ type: "CHECK_FORMAT_EMAIL_TRUE" });
    } else {
      dispatch({ type: "CHECK_FORMAT_EMAIL_FALSE" });
    }
  };
  const handleFitCFPassword = () => {
    if (state.CFpassword !== state.password) {
      dispatch({ type: "CHECK_FIT_CFPASSWORD_TRUE" });
    } else {
      dispatch({ type: "CHECK_FIT_CFPASSWORD_FALSE" });
    }
  };

  const handleEmailChange = (e) => {
    dispatch({ type: "CHANGE_EMAIL", payload: e.target.value });
  };

  const handlePasswordChange = (e) => {
    dispatch({ type: "CHANGE_PASSWORD", payload: e.target.value });
  };
  const handleCFPasswordChange = (e) => {
    dispatch({ type: "CHANGE_CFPASSWORD", payload: e.target.value });
  };
  const DISPATCH = useDispatch();
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const handleRegister = async (e) => {
    setIsLoading(true);
    try {
      e.preventDefault();
      const newUser = {
        email: state.email,
        phoneNumber: state.phonenum,
        username: state.fullname,
        password: state.password,
        confirmPassword: state.CFpassword,
      };
      const err = await registerUser(newUser, DISPATCH, navigate);
      if (err) {
        alert(err);
      }
    } catch (error) {
      alert(error.message);
      alert("meomeo");
    } finally {
      setIsLoading(false);
    }
  };
  const user = useSelector((state) => state.auth.login?.currentUser);
  useEffect(() => {
    if (user) {
      navigate("/");
    }
  });
  const handleOnSubmit = () => {
    if (state.fullname === "") {
      dispatch({ type: "BLUR_FULLNAME" });
    }
    if (state.phonenum === "") {
      dispatch({ type: "BLUR_PHONENUM" });
    }
    if (state.email === "") {
      dispatch({ type: "BLUR_EMAIL" });
    }
    if (state.password === "") {
      dispatch({ type: "BLUR_PASSWORD" });
    }
    if (state.CFpassword === "") {
      dispatch({ type: "BLUR_CFPASSWORD" });
    }
  };
  const formLoginDiv =
    "flex flex-wrap text-[#818181] font-bold w-1/2 h-auto justify-center items-center rounded-3xl border-solid border-[#ffcbd94d] border bg-[#ffffffd9] shadow-md";
  return (
    <div className="flex justify-center items-center w-full h-full my-8">
      <div className={formLoginDiv}>
        <h1 className="text-center text-[#FF689F] font-[600] py-0 px-[1em] pt-[1em] flex-grow text-4xl w-full ">
          Đăng ký
        </h1>
        {
          isLoading ? (
            <LoadingEdit />
          ) : (
            <section className="mx-12 flex-grow p-12 ">
              <form action="" onSubmit={handleRegister}>
                <div className="mb-4 relative">
                  <input
                    id="Input_FullName"
                    type="text"
                    className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                    onFocus={handleFullNameFocus}
                    onBlur={handleFullNameBlur}
                    onChange={handleFullNameChange}
                    value={state.fullname}
                  />
                  <label
                    htmlFor="Input_FullName"
                    className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${state.isFullNameFocused || state.fullname
                        ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                        : ""
                      }`}
                  >
                    Họ và tên
                  </label>
                </div>

                <p className="text-red-600 font-semibold mx-2 pb-4">
                  {state.checkFullName &&
                    !state.fullname &&
                    "Họ và tên không được để trống"}
                </p>
                <p className="text-red-600 font-semibold mx-2 pb-4"></p>
                <div className="mb-4 relative">
                  <input
                    id="Input_PhoneNum"
                    type="text"
                    className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                    onFocus={handlePhoneNumFocus}
                    onBlur={handlePhoneNumBlur}
                    onChange={handlePhoneNumChange}
                    onKeyUp={handleFormatPhoneNumCheck}
                    value={state.phonenum}
                  />
                  <label
                    htmlFor="Input_PhoneNum"
                    className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${state.isPhoneNumFocused || state.phonenum
                        ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                        : ""
                      }`}
                  >
                    Số điện thoại
                  </label>
                </div>
                <p className="text-red-600 font-semibold mx-2 pb-4">
                  {state.checkPhoneNum &&
                    !state.phonenum &&
                    "Số điện thoại không được để trống"}
                </p>
                <p className="text-red-600 font-semibold mx-2 pb-4">
                  {state.checkFormatPhoneNum &&
                    state.phonenum &&
                    "Số điện thoại không hợp lệ"}
                </p>
                <div className="mb-4 relative">
                  <input
                    id="Input_Email"
                    type="email"
                    className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                    onFocus={handleEmailFocus}
                    onBlur={handleEmailBlur}
                    onChange={handleEmailChange}
                    onKeyUp={handleFormatEmailCheck}
                    value={state.email}
                  />
                  <label
                    htmlFor="Input_Email"
                    className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${state.isEmailFocused || state.email
                        ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                        : ""
                      }`}
                  >
                    Email
                  </label>
                </div>
                <p className="text-red-600 font-semibold mx-2 pb-4">
                  {state.checkEmail && !state.email && "Email không được để trống"}
                </p>
                <p className="text-red-600 font-semibold mx-2 pb-4">
                  {state.checkFormatEmail && state.email && "Email không hợp lệ"}
                </p>
                <div className="mb-4 relative">
                  <input
                    id="Input_Password"
                    type="password"
                    className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                    onFocus={handlePasswordFocus}
                    onBlur={handlePasswordBlur}
                    onChange={handlePasswordChange}
                    value={state.password}
                  />
                  <label
                    htmlFor="Input_Password"
                    className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${state.isPasswordFocused || state.password
                        ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                        : ""
                      }`}
                  >
                    Mật khẩu
                  </label>
                </div>
                <p className="text-red-600 font-semibold mx-2 pb-4">
                  {state.checkPassword &&
                    !state.password &&
                    "Mật khẩu không được để trống"}
                </p>
                <p className="text-red-600 font-semibold mx-2 pb-4"></p>
                <div className="mb-4 relative">
                  <input
                    id="Input_CFPassword"
                    type="password"
                    className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                    onFocus={handleCFPasswordFocus}
                    onBlur={handleCFPasswordBlur}
                    onChange={handleCFPasswordChange}
                    onKeyUp={handleFitCFPassword}
                    value={state.CFpassword}
                  />
                  <label
                    htmlFor="Input_CFPassword"
                    className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${state.isCFPasswordFocused || state.CFpassword
                        ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                        : ""
                      }`}
                  >
                    Xác nhận mật khẩu
                  </label>
                </div>
                <p className="text-red-600 font-semibold mx-2 pb-4">
                  {state.checkCFPassword &&
                    !state.CFpassword &&
                    "Xác nhận mật khẩu không được để trống"}
                </p>
                <p className="text-red-600 font-semibold mx-2 pb-4">
                  {state.checkCFPasswordFit &&
                    state.CFpassword &&
                    "Mật khẩu không trùng khớp"}
                </p>
                <div className="text-center">
                  <BtnSubmit onClick={handleOnSubmit} type="submit">
                    Đăng ký
                  </BtnSubmit>
                </div>
              </form>
              <div className="text-start text-red-600 font-semibold mt-6 px-3">
                {msg}
              </div>
            </section>
          )
        }
      </div>
    </div>
  );
}

export default Register;
