
const accountApi = {
    getAllAccount: async (axiosJWT, navigate, accessToken) => {
        try {
            const url = `http://localhost:5000/api/Account`;
            return axiosJWT.get(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    getAccountById: async (axiosJWT, navigate, accessToken, id) => {
        try {
            const url = `http://localhost:5000/api/Account/${id}`;
            return axiosJWT.get(url, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
    updateAccount: async (axiosJWT, navigate, accessToken, id, data) => {
        try {
            const url = `http://localhost:5000/api/Account/${id}`;
            return axiosJWT.put(url, data, {
                headers: {
                    token: `Bearer ${accessToken}`,
                },
            });
        } catch (error) {
            navigate("/login");
        }
    },
};
export default accountApi;
