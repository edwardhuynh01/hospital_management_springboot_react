import React, { useEffect, useState } from "react";
import Record from "./Record";
import recordApi from "../../api/recordApi";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import { chooseRecord } from "../../redux/appointmentSlice";
import { data } from "jquery";

function RecordsList(props) {
  const [activeIndex, setActiveIndex] = useState(-1);

  const handleRecordClick = (index) => {
    if (index === activeIndex) {
      setActiveIndex(-1); // Đóng `Record` nếu đã mở
    } else {
      setActiveIndex(index); // Mở `Record` tại vị trí index
    }
    console.log(index);
  };
  const user = useSelector((state) => state.auth.login?.currentUser);

  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [recordList, setRecordList] = useState();
  const [isDeleted, setIsDeleted] = useState(false);
  useEffect(() => {
    try {
      if (!accessToken || typeof accessToken !== "string") {
        throw new Error("Token không hợp lệ");
      }
      const fetchRecord = async () => {
        const data = await recordApi.getAllRecord(
          axiosJWT,
          navigated,
          accessToken
        );
        const databyUserId = data.data.filter((x) => x.account === user._id);
        setRecordList(databyUserId);

      };
      fetchRecord();

    } catch (error) { }
  }, [isDeleted]);
  const saveRecordId = async (recordId) => {
    DISPATCH(chooseRecord({ recordId: recordId }));
  };
  const handleDelete = () => {
    setIsDeleted(!isDeleted);
  };
  return (
    <div className="w-full lg:w-1/2">
      {recordList?.map((recordItem, index) => (
        <Record
          onClick={() => handleRecordClick(index)}
          isActive={activeIndex === index}
          id={recordItem._id}
          chooseRecord={() => saveRecordId(recordItem._id)}
          fullName={recordItem.fullName}
          email={recordItem.email}
          date={new Date(recordItem.date).toISOString().split("T")[0]}
          sex={recordItem.sex}
          IdentityCardNumber={recordItem.identityCardNumber}
          address={recordItem.address}
          Peoples={recordItem.peoples}
          phoneNumber={recordItem.phoneNumber}
          delete={handleDelete}
        ></Record>
      ))}
    </div>
  );
}

export default RecordsList;
