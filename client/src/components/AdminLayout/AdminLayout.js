import React from "react";
import Navbar from "../AdminNavbar/Navbar";
import SideBar from "../AdminSidebar/Sidebar";
import DashboardPage from "../../pages/admin/dashboardPage";
import { Outlet } from "react-router-dom";

const AdminLayout = (props) => {
    return (
        <div>
            <SideBar />
            <Navbar />
            <main>
                <Outlet /> {/* Nội dung của các nested route sẽ được render tại đây */}
            </main>
        </div>
    );
}

export default AdminLayout;