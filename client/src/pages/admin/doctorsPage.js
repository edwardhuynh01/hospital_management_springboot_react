import React, { useState, useEffect } from 'react';
import AddDoctorForm from '../../components/AdminAddPopup/AddDoctorForm.js';
import doctorApi from '../../api/doctorApi.js'; // Import doctorApi
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { loginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../createInstance";
import { confirmAlert } from 'react-confirm-alert';
import { toast } from 'react-toastify';
import specialistApi from '../../api/specialistApi.js';
import EditDoctorForm from '../../components/AdminAddPopup/EditDoctorForm.js';
import CountUp from 'react-countup';
import LoadingEdit from '../../components/Loading/LoadingEdit.js';
import ModelDoctorDetails from "../../components/AdminAddPopup/ModelDoctorDetails.js";
const DoctorsPage = () => {
  const [isModalAddOpen, setIsModalAddOpen] = useState(false);
  const [isModalEditOpen, setIsModalEditOpen] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const navigate = useNavigate();
  const user = useSelector((state) => state.auth.login?.currentUser);
  const DISPATCH = useDispatch();
  const navigated = useNavigate();
  let accessToken = user?.accessToken;
  let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
  const [docterList, setDocterList] = useState([]);
  const [specialists, setSpecialists] = useState([]);
  const [id, setId] = useState();
  const [search, setSearch] = useState("");
  const [confirmedSearch, setConfirmedSearch] = useState("");

  const handleFilter = () => {
    setConfirmedSearch(search);
    setSearch('');
  };
  const encode = (id) => {
    return btoa(id);
  }
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    setIsLoading(true);
    try {
      if (!user) {
        navigated("/login");
      } else {
        if (user.role !== "admin") {
          navigated("/");
        } else {

          if (!accessToken || typeof accessToken !== "string") {
            throw new Error("Token không hợp lệ");
          }
          const fetchDocter = async () => {
            try {
              const data = await doctorApi.getAllDoctor(
                axiosJWT,
                navigated,
                accessToken
              );
              setDocterList(data.data);
            } catch (error) {

            }
            finally {
              setIsLoading(false);
            }
          };
          fetchDocter();
          const fetchSpec = async () => {
            try {
              const data = await specialistApi.getAllSpecialist();
              setSpecialists(data);
            } catch (error) {
            }

          }
          fetchSpec();
        }
      }
    } catch (error) { }
  }, [isDeleted]);

  const handleAddDoctor = async (newDoctor, set) => {
    try {
      const response = await doctorApi.createDoctor(axiosJWT, navigate, accessToken, newDoctor);
      setIsDeleted(!isDeleted)
      setIsModalAddOpen(false);
      toast("Thêm bác sĩ thành công!");
    } catch (error) {
      set([]);
      toast.error("Đã xảy ra lỗi khi thêm bác sĩ!");
      console.error('Failed to add doctor:', error);
    }
  };
  const handleEditDoctor = async (newDoctor) => {
    try {
      const response = await doctorApi.updateDoctor(axiosJWT, navigate, accessToken, encode(id), newDoctor);
      setIsDeleted(!isDeleted)
      setIsModalEditOpen(false);
      toast("Cập nhật bác sĩ thành công!")
    } catch (error) {
      console.error('Failed to add doctor:', error);
    }
  };
  const handleDelete = (id) => {
    confirmAlert({
      customUI: ({ onClose }) => (
        <div className="bg-slate-100 p-6 rounded-lg shadow-xl">
          <h1 className="text-2xl text-[#fb6f92] font-semibold mb-4 text-start h-full w-full">
            Xác nhận xóa
          </h1>
          <p className="mb-4">Bạn có chắc chắn muốn xóa bác sĩ ?</p>
          <div className="flex justify-center gap-4">
            <button
              onClick={async () => {
                try {
                  const deleteData = await doctorApi.deleteDoctor(
                    axiosJWT,
                    navigated,
                    accessToken,
                    id
                  );
                  if (deleteData) {
                    setIsDeleted(!isDeleted);
                    // if (deleteData?.data?.avatarImage) {
                    //     deleteFile(deleteData.data.avatarImage);
                    // }
                    toast("Xóa bác sĩ thành công!");
                  } else {
                    toast.error("Đã xảy ra lỗi khi xóa bác sĩ.");
                  }
                } catch (error) {
                  toast.error("Đã xảy ra lỗi khi xóa bác sĩ.");
                  console.error("Error deleting record:", error);
                }
                onClose();
              }}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-red-600 duration-300 bg-red-400 text-white px-8 py-2 rounded-lg mr-2"
            >
              Có
            </button>
            <button
              onClick={onClose}
              className="transition ease-in-out delay-100 hover:scale-110 hover:bg-gray-400 duration-300 bg-gray-300 text-black px-4 py-2 rounded-lg"
            >
              Không
            </button>
          </div>
        </div>
      ),
    });
  };
  // const deleteFile = async (imageLink) => {
  //     try {
  //         const fileId = imageLink.split("/")[4];
  //         const response = await fetch(
  //             `https://script.google.com/macros/s/AKfycbxA2nd3dNnMh-UkBFJ4nGTE6mqosePHJp30gX2PILSea90yqgKaOT4joqP7zFlyTyU/exec?fileId=${fileId}`,
  //             {
  //                 method: "GET",
  //             }
  //         );
  //         const data = await response.json();
  //         // formData.image = data.link;
  //         console.log(data);
  //         // console.log(formData);
  //     } catch (error) {
  //         console.log("có lỗi xảy ra vui lòng thử lại", error);
  //     }
  // };
  const handleResetDoctorDays = async (newDoctor, DrId) => {
    try {
      const response = await doctorApi.updateDoctor(
        axiosJWT,
        navigate,
        accessToken,
        encode(DrId),
        newDoctor
      );
    } catch (error) {
      console.error("Failed to edit doctor:", error);
    }
  };
  const [daysOfWeek, setDaysOfWeek] = useState([]);
  const autoCalday = (dayOfWeek) => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth();
    const currentDay = currentDate.getDate();
    const days = [];
    let date = new Date(currentYear, currentMonth, 1);
    while (date.getMonth() === currentMonth) {
      if (date.getDay() === Number(dayOfWeek) && date.getDate() >= currentDay) {
        days.push(new Date(date).getDate()); // Thêm ngày phù hợp vào mảng
      }
      date.setDate(date.getDate() + 1); // Tăng ngày lên 1;
    }
    if (days.length <= 0) {
      while (date.getMonth() === (currentMonth + 1) % 12) {
        if (date.getDay() === Number(dayOfWeek)) {
          days.push(new Date(date).getDate()); // Thêm ngày phù hợp vào mảng
        }
        date.setDate(date.getDate() + 1); // Tăng ngày lên 1;
      }
    }
    return days;
  };
  // useEffect(() => {
  //   console.log("Days of week updated:", daysOfWeek);
  // }, [daysOfWeek]);
  const handleResetDaysWork = () => {
    try {
      if (docterList.length > 0) {
        docterList.forEach((doctor) => {
          // console.log(doctor);
          const times = doctor?.days[0]?.times;
          if (doctor?.dateOfWeek !== null) {
            // console.log(doctor.dateOfWeek);
            const days = autoCalday(doctor?.dateOfWeek);
            if (days !== null) {
              const dataDay = [];
              days.map((day, index) => dataDay.push({ day, times }));
              const doctorData = {
                ...doctor,
                days: dataDay,
              };
              handleResetDoctorDays(doctorData, doctor._id);
            }
          }
          // console.log(dataDay);
        });
        toast("Cập nhật thành công!")
      }
    } catch (error) {

    }
  };
  const [selectedAccount, setSelectedAccount] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const handleAccountClick = (name, phoneNumber, email, experience, specialist, sex) => {
    const data = {
      name: name,
      phoneNumber: phoneNumber,
      email: email,
      experience: experience,
      specialist: specialist,
      sex: sex
    }
    setSelectedAccount(data);
    setShowModal(true);
  }
  return (
    <main class="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 bg-gray-50 transition-all main">
      <div className="p-6 h-full">
        <div class="grid grid-cols-1">
          <div class="bg-white border border-gray-100 shadow-md shadow-black/4 p-9 rounded-md ">
            <div class="flex justify-between mb-4 items-start">
              <div class="font-medium text-gray-500">Số Bác Sĩ: <CountUp end={docterList?.length} duration={2} /></div>
            </div>
            <div class="flex items-center mb-px order-tab gap-2">
              <div class="flex items-center mb-4 order-tab gap-2">
                <button style={{ width: '100px' }} type="button" data-tab="order" data-tab-page="active" class="shadow-md shadow-black/4 bg-sky-400 text-sm font-medium text-white py-2 px-5 rounded-tl-md rounded-bl-md hover:bg-sky-500 active" onClick={() => setIsModalAddOpen(true)}>Thêm</button>
                <button style={{ width: '140px' }} onClick={handleResetDaysWork} type="button" data-tab="order" data-tab-page="active" class="shadow-md shadow-black/4 bg-sky-400 text-sm font-medium text-white py-2 px-5 rounded-tr-md rounded-br-md hover:bg-sky-500 active">Đặt lại ngày</button>
              </div>
              <div class="flex items-center mb-5 order-tab gap-6">
                <input style={{ width: '720px' }} type="text" value={search} onChange={(e) => setSearch(e.target.value)} class="shadow-md shadow-black/4 ml-3 py-2 pr-4 pl-10 bg-gray-50 w-full outline-none border border-gray-100 rounded-md text-sm focus:border-blue-500" placeholder="Tìm kiếm..."></input>
                <button type="button" onClick={handleFilter} data-tab="order" data-tab-page="active" class="shadow-md shadow-black/4 bg-blue-400 text-sm font-medium text-white py-2 px-6 rounded-tl-md rounded-bl-md rounded-tr-md  rounded-br-md hover:bg-blue-500 active">Lọc</button>
              </div>
            </div>
            <div class="overflow-x-auto">
              <div class="overflow-y-auto max-h-[508px]">
                <table class={`w-full min-w-[800px] ${isLoading ? 'flex flex-col justify-center items-center' : ''}`} data-tab-for="order" data-page="active">
                  <thead className="w-full">
                    <tr>
                      <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left rounded-tl-md rounded-bl-md">Tên</th>
                      <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left">Email</th>
                      <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-4 bg-gray-100 text-left">Chuyên Khoa</th>
                      <th style={{ minWidth: '250px', tableLayout: 'fixed' }} class="whitespace-nowrap text-[12px] uppercase tracking-wide font-medium text-gray-500 py-2 px-5 bg-gray-100 text-left rounded-tr-md rounded-br-md">Chức Năng</th>
                    </tr>
                  </thead>

                  {
                    isLoading ? (
                      <LoadingEdit />
                    ) : (
                      <tbody>
                        {docterList?.filter((doctor) => {
                          return confirmedSearch.toLowerCase() === ''
                            ? true
                            : doctor.name.toLowerCase().includes(confirmedSearch.toLowerCase())
                        }).map((doctor) => (
                          <tr key={doctor?._id} onClick={() => {
                            handleAccountClick(
                              doctor.name,
                              doctor?.phoneNumber,
                              doctor.email,
                              doctor?.experience,
                              specialists.find(x => x._id === doctor?.specialist)?.specialistName || 'N/A',
                              doctor?.sex,
                            )
                          }} className="cursor-pointer hover:bg-blue-50 transition-all ease-in-out">
                            <td class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                              <div class="flex items-center">
                                {/* <img src="https://placehold.co/32x32" alt="" class="w-8 h-8 rounded object-cover block"></img> */}
                                <img src={`data:image/png;base64,${doctor?.avatarImage}`} alt={doctor?.name} class="w-8 h-8 rounded object-cover block" />
                                <a href="#" class="text-gray-600 text-sm font-medium hover:text-blue-500 ml-2 truncate">{doctor?.name}</a>
                              </div>
                            </td>
                            <td class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                              <span class="text-[13px] font-medium text-gray-400">{doctor?.email}</span>
                            </td>
                            <td class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                              <span class="text-[13px] font-medium text-gray-400">{specialists.find(x => x._id === doctor?.specialist)?.specialistName || 'N/A'}</span>
                            </td>
                            <td class="py-2 px-4 border-b border-b-gray-50 whitespace-nowrap overflow-auto" style={{ maxWidth: '200px', tableLayout: 'fixed' }}>
                              <div class="flex items-center gap-1 justify-right">
                                <div class="hover:bg-pink-100 rounded">
                                  <div type="button" class="px-2 mt-1" onClick={() => {
                                    setIsModalEditOpen(true);
                                    setId(doctor?._id);
                                  }}>
                                    <box-icon name='edit-alt' ></box-icon>
                                  </div>
                                </div>
                                <div class="hidden sm:block mx-2 lg:mx-px w-px h-5 bg-gray-200 dark:bg-gray-900"></div>
                                <div class="hover:bg-pink-100 rounded">
                                  <div type="button" class="px-2 mt-1" onClick={() => handleDelete(doctor?._id)}>
                                    <box-icon name='trash' ></box-icon>
                                  </div>
                                </div>
                              </div>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    )
                  }
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <AddDoctorForm
        isOpen={isModalAddOpen}
        onRequestClose={() => setIsModalAddOpen(false)}
        onSubmit={handleAddDoctor}
      />
      <EditDoctorForm isOpen={isModalEditOpen}
        onRequestClose={() => setIsModalEditOpen(false)}
        onSubmit={handleEditDoctor}
        isId={id}
      />
      <ModelDoctorDetails
        showModal={showModal}
        setShowModal={setShowModal}
        selectedAccount={selectedAccount}
      />
    </main>
  )
}

export default DoctorsPage