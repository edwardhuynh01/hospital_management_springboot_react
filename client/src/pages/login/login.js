import React, { useEffect, useReducer, useState } from "react";
import styled from "styled-components";
import { loginGG, loginUser } from "../../redux/apiRequest";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { GoogleOAuthProvider, GoogleLogin } from "@react-oauth/google";
import { jwtDecode } from "jwt-decode";
const initialState = {
  email: "",
  password: "",
  isEmailFocused: false,
  isPasswordFocused: false,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "FOCUS_EMAIL":
      return { ...state, isEmailFocused: true };
    case "BLUR_EMAIL":
      return { ...state, isEmailFocused: false };
    case "FOCUS_PASSWORD":
      return { ...state, isPasswordFocused: true };
    case "BLUR_PASSWORD":
      return { ...state, isPasswordFocused: false };
    case "CHANGE_EMAIL":
      return { ...state, email: action.payload };
    case "CHANGE_PASSWORD":
      return { ...state, password: action.payload };
    default:
      return state;
  }
};
const BtnSubmit = styled.button`
  width: 90%;
  height: 2.5em;
  border-radius: 8px;
  border: 1px solid #d1d1d1;
  background: rgba(255, 104, 159, 0.75);
  box-shadow: 0px 15px 30px -10px rgba(255, 189, 213, 0.3);
  color: #fff;
  text-align: center;
  font-size: 1em;
  font-style: normal;
  font-weight: 700;
  line-height: normal;
  transition: color 0.25s, border-color 0.25s, box-shadow 0.25s, transform 0.25s;
  cursor: pointer;
  padding: 0.5rem 1rem;
  padding-bottom: 2rem;
  align-items: center;
  margin-top: 30px;
  &:hover {
    border-color: rgba(255, 104, 159, 0.75);
    color: white;
    box-shadow: 0 0.5em 0.5em -0.4em rgb(255, 104, 159);
    transform: translate(0, -0.25em);
    cursor: pointer;
  }
`;
const Register = styled.a`
  /* color: #90e0ef; */
  &:hover {
    text-decoration: underline;
  }
`;
const Login = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const handleEmailFocus = () => {
    dispatch({ type: "FOCUS_EMAIL" });
  };

  const handleEmailBlur = () => {
    dispatch({ type: "BLUR_EMAIL" });
  };

  const handlePasswordFocus = () => {
    dispatch({ type: "FOCUS_PASSWORD" });
  };

  const handlePasswordBlur = () => {
    dispatch({ type: "BLUR_PASSWORD" });
  };

  const handleEmailChange = (e) => {
    dispatch({ type: "CHANGE_EMAIL", payload: e.target.value });
  };

  const handlePasswordChange = (e) => {
    dispatch({ type: "CHANGE_PASSWORD", payload: e.target.value });
  };
  const user = useSelector((state) => state.auth.login?.currentUser);
  useEffect(() => {
    if (user) {
      navigate("/");
    }
  });
  const DISPATCH = useDispatch();
  const navigate = useNavigate();
  const [msg, setMsg] = useState();
  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      if (!state.email || !state.password) {
        // Hiển thị thông báo lỗi cho người dùng
        setMsg("Vui lòng cung cấp email và password");
        return;
      }
      const newUser = {
        email: state.email,
        password: state.password,
      };
      const res = await loginUser(newUser, DISPATCH, navigate);
      if (res) {
        // alert(errorMessage);
        setMsg(res);
      } else {
        setMsg("");
      }
      if (res.role === "admin") {
        navigate("/admin");
      }
      if (res.role === "customer") {
        navigate("/");
      }
    } catch (error) {
      // Hiển thị thông điệp lỗi từ máy chủ cho người dùng
      alert(error.message);
    }
  };
  const handleLoginSuccess = async (credentialResponse) => {
    const credentialres = credentialResponse.credential;
    const decodeInfo = jwtDecode(credentialres);
    console.log(decodeInfo);
    const res = await loginGG(decodeInfo, DISPATCH, navigate);
    console.log(res);
    if (res) {
      // alert(errorMessage);
      setMsg(res);
    } else {
      setMsg("");
    }
  };
  const handleFailure = (error) => {
    console.error("Google login failed:", error);
  };
  const formLoginDiv =
    "flex flex-wrap text-[#818181] font-bold w-1/2 h-auto justify-center items-center rounded-3xl border-solid border-[#ffcbd94d] border bg-[#ffffffd9] shadow-md";
  return (
    <div className="flex justify-center items-center w-full h-full my-8">
      <div className={formLoginDiv}>
        <h1 className="text-center text-[#FF689F] font-[600] py-0 px-[1em] pt-[1em] flex-grow text-4xl w-full ">
          Đăng nhập
        </h1>
        <section className="mx-12 flex-grow p-12 ">
          <form action="" onSubmit={handleLogin}>
            <div className="mb-4 relative">
              <input
                id="Input_Email"
                type="email"
                className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                onFocus={handleEmailFocus}
                onBlur={handleEmailBlur}
                onChange={handleEmailChange}
                value={state.email}
              />
              <label
                htmlFor="Input_Email"
                className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${state.isEmailFocused || state.email
                    ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                    : ""
                  }`}
              >
                Email
              </label>
            </div>
            <div className="mb-4 relative">
              <input
                id="Input_Password"
                type="password"
                className="py-4 px-3 h-[calc(3.5rem + 2px)] leading-tight border border-[#CDB4DB] block w-full text-base font-normal text-black bg-white bg-clip-padding appearance-none rounded-2xl transition-all pt-7 pb-3 focus-visible:shadow-lg focus-visible:border-red-300 outline-none"
                onFocus={handlePasswordFocus}
                onBlur={handlePasswordBlur}
                onChange={handlePasswordChange}
                value={state.password}
              />
              <label
                htmlFor="Input_Password"
                className={`absolute left-0 top-0 h-full py-4 px-3 pointer-events-none border border-transparent border-solid transition-all text-[#818181] font-normal mb-2 inline-block cursor-default ${state.isPasswordFocused || state.password
                    ? "opacity-65 scale-75 -translate-y-2 -translate-x-[0.2rem]"
                    : ""
                  }`}
              >
                Mật khẩu
              </label>
            </div>
            <div className="text-center">
              <BtnSubmit type="submit">Đăng nhập</BtnSubmit>
            </div>
            <div>
              <div className="mt-6 mb-12 w-full flex justify-center items-start">
                <div className="flex flex-wrap justify-center w-2/3">
                  <p className="w-full mb-3 p-0 text-center font-medium text-red-300">
                    Đăng nhập bằng phương thức khác
                  </p>
                  <div className="w-2/3">
                    <GoogleOAuthProvider clientId="161168036130-nh91m1e22p2k71r6q6jp0eb6bliujq7r.apps.googleusercontent.com">
                      <div className="flex justify-center items-center gap-2">
                        <GoogleLogin
                          buttonText="Login with Google"
                          onSuccess={handleLoginSuccess}
                          onFailure={handleFailure}
                          cookiePolicy={"single_host_origin"}
                          type="icon"
                          shape="rectangular"
                        />Login with Google
                      </div>
                    </GoogleOAuthProvider>
                  </div>
                </div>
              </div>
              <p className="text-start my-5 font-medium text-red-300">
                Bạn chưa có tài khoản? Đăng ký{" "}
                <Register className="text-[#90e0ef]" href="/register">
                  tại đây
                </Register>
              </p>
              <p className="text-start my-5 font-medium text-red-300">
                <Register href="/forgot">Quên mật khẩu?</Register>
              </p>
            </div>
          </form>
          <div className="text-start text-red-600 font-semibold mt-6 px-3">
            {msg}
          </div>
        </section>
      </div>
    </div>
  );
};

export default Login;
