import React, { useEffect, useState } from "react";
import newsApi from "../../../api/newsApi";
import { useParams } from "react-router-dom";
import Title from "../../../components/Title/Title";

const NewsPagesDetail = () => {
  const [news, setNews] = useState();
  const { id } = useParams();

  useEffect(() => {
    const fetchNews = async () => {
      try {
        const response = await newsApi.getNewsById(id);
        setNews(response);
      } catch (error) {
        console.error("Error fetching news:", error);
      }
    };

    fetchNews();
  }, [id]);

  return (
    <div className="bg-white">
      <Title>{news?.title.toUpperCase()}</Title>
      <div className="container mx-auto py-6">
        <div className="md:mx-20">
          <img
            className="w-full h-[500px] <=md:h-auto"
            src={`data:image/png;base64,${news?.mainImage}`}
            alt={news?.title}
          />
          <p className="font-light my-4">{news?.teaser}</p>
          <div
            className="my-4"
            dangerouslySetInnerHTML={{ __html: news?.content }}
          />
        </div>
      </div>
    </div>
  );
};

export default NewsPagesDetail;
