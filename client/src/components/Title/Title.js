import React from 'react';

const Title = (props) => {
    return (
        <div className={`flex items-center flex-col flex-nowrap justify-center ${props.className || ""}`}>
            <span className='text-[28px] text-[#5a189a] font-bold mt-8 text-center'>{props.children}</span>
            <span className='text-4xl'>🍀</span>
        </div>
    );
};

export default Title;