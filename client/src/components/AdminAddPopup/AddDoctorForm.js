import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import specialistApi from '../../api/specialistApi';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate, useNavigate } from 'react-router-dom';
import { loginSuccess } from '../../redux/authSlice';
import { createAxios } from '../../createInstance';
import doctorApi from '../../api/doctorApi';
import { toast } from "react-toastify";
import Loading from '../Loading/Loading';
const AddDoctorForm = ({ isOpen, onRequestClose, onSubmit }) => {
    const user = useSelector((state) => state.auth.login?.currentUser);
    const DISPATCH = useDispatch();
    const navigated = useNavigate();
    let accessToken = user?.accessToken;
    let axiosJWT = createAxios(user, DISPATCH, loginSuccess);
    const [fileName, setFileName] = useState("Không có tệp nào được chọn");
    const [isLoading, setIsLoading] = useState(false);
    const [doctorData, setDoctorData] = useState({
        name: '',
        phoneNumber: '',
        email: '',
        avatarImage: null,
        experience: '',
        dateOfWeek: '',
        specialist: '',
        sex: '',
        days: '',
    });
    // const handleFileChange = (e) => {
    //     const file = e.target.files[0];
    //     setDoctorData((prevData) => ({
    //         ...prevData,
    //         image: file,
    //     }));
    //     setFileName(file ? file.name : "Không có tệp nào được chọn");
    // };
    const handleChange = (e) => {
        setDoctorData({ ...doctorData, [e.target.name]: e.target.value });
    };
    const validateForm = () => {
        let isValid = true;
        if (
            doctorData.name.trim() === ""
        ) {
            isValid = false;
        }
        if (doctorData.phoneNumber === "") {
            isValid = false;
        }
        if (doctorData.phoneNumber.trim() === "") {
            isValid = false;
        }
        if (doctorData.email.trim() === "") {
            isValid = false;
        }
        if (doctorData.experience === "") {
            isValid = false;
        }
        if (doctorData.dateOfWeek === "-1") {
            isValid = false;
        }
        if (doctorData.specialist === "-1") {
            isValid = false;
        }
        if (doctorData.sex === "-1") {
            isValid = false;
        }
        return isValid;
    };

    const [dateOfWeek, setDateOfWeek] = useState(0);
    const [daysOfWeek, setDaysOfWeek] = useState([]);
    const [times, setTimes] = useState([]);
    const dataDay = [];
    daysOfWeek.map((day, index) => dataDay.push({ day, times }));
    const handleSelectTime = (e) => {
        const time = e.target.value;
        const timeStart = time.split("-")[0];
        const timeEnd = time.split("-")[1];
        const isChecked = e.target.checked;
        if (isChecked) {
            setTimes([...times, { timeStart, timeEnd }]);
        } else {
            setTimes(times.filter((t) => t.timeStart !== timeStart));
        }
    };
    //   console.log(times);
    const handleDateOfWeekSelect = (e) => {
        const selectedDay = e.target.value;
        setDateOfWeek(selectedDay);
        autoCalday(selectedDay);
    };
    const autoCalday = (dayOfWeek) => {
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const currentMonth = currentDate.getMonth();
        const currentDay = currentDate.getDate();
        const days = [];
        let date = new Date(currentYear, currentMonth, 1);
        while (date.getMonth() === currentMonth) {
            if (date.getDay() === Number(dayOfWeek) && date.getDate() >= currentDay) {
                days.push(new Date(date).getDate()); // Thêm ngày phù hợp vào mảng
            }
            date.setDate(date.getDate() + 1); // Tăng ngày lên 1;
        }
        if (days.length <= 0) {
            while (date.getMonth() === (currentMonth + 1) % 12) {
                if (date.getDay() === Number(dayOfWeek)) {
                    days.push(new Date(date).getDate()); // Thêm ngày phù hợp vào mảng
                }
                date.setDate(date.getDate() + 1); // Tăng ngày lên 1;
            }
        }
        setDaysOfWeek(days);
    };
    const [specialists, setSpecialists] = useState();
    useEffect(() => {
        try {
            const fetchService = async () => {
                const data = await specialistApi.getAllSpecialist();
                setSpecialists(data);
            };
            fetchService();
        } catch (error) { }
    }, []);
    const doctors = {
        name: doctorData?.name,
        phoneNumber: doctorData?.phoneNumber,
        email: doctorData?.email,
        experience: doctorData?.experience,
        dateOfWeek: (Number)(doctorData?.dateOfWeek),
        specialist: doctorData?.specialist,
        // degrees: doctorData.degrees,
        avatarImage: '',
        sex: doctorData?.sex,
        days: dataDay,
    }

    // const handleChooseFile = () => {
    //     return new Promise((resolve, reject) => {
    //         const btnChooseFile = document.getElementById("image-upload");

    //         const file = btnChooseFile.files[0];
    //         if (!file) {
    //             return toast.error("Vui lòng chọn hình ảnh");
    //         }

    //         const reader = new FileReader();
    //         reader.readAsDataURL(file);
    //         reader.addEventListener("load", () => {
    //             const data = reader.result.split(",")[1];
    //             const postData = {
    //                 name: doctorData?.name,
    //                 type: file.type,
    //                 data: data,
    //             };

    //             postFile(postData)
    //                 .then((response) => resolve(response))
    //                 .catch((error) => reject(error));
    //         });
    //         reader.onerror = () => {
    //             reject(toast.error("Failed to read file"));
    //         };
    //     });
    // };

    // const postFile = async (postData) => {
    //     try {
    //         const response = await fetch(
    //             "https://script.google.com/macros/s/AKfycbxA2nd3dNnMh-UkBFJ4nGTE6mqosePHJp30gX2PILSea90yqgKaOT4joqP7zFlyTyU/exec",
    //             {
    //                 method: "POST",
    //                 body: JSON.stringify(postData),
    //             }
    //         );

    //         const data = await response.json();
    //         doctors.avatarImage = data.link;
    //         return data;
    //         //   console.log(data);
    //         //   console.log(formData);
    //     } catch (error) {
    //         console.log("có lỗi xảy ra vui lòng thử lại", error);
    //     }
    // };
    const [base64String, setBase64String] = useState('');

    const handleFileChangeBase64 = (event) => {
        const file = event.target.files[0];
        if (file) {
            setFileName(file ? file.name : "Không có tệp nào được chọn");
            const reader = new FileReader();
            reader.onloadend = () => {
                const base64 = reader.result.replace("data:", "").replace(/^.+,/, "");
                setBase64String(base64);
                doctorData.avatarImage = base64;
            };
            reader.readAsDataURL(file);
        }
    };
    const handleSubmit = async (e) => {
        e.preventDefault();
        setIsLoading(true);

        const isValidForm = validateForm();
        try {
            if (isValidForm) {
                // const image = await handleChooseFile();
                doctors.avatarImage = base64String;
                // console.log(doctors);
                onSubmit(doctors, setTimes);
                setFileName("Không có tệp nào được chọn");
                setBase64String('');
                doctorData.avatarImage = "";
            } else {
                toast.error("Vui lòng điền đầy đủ thông tin trước khi thêm bác sĩ!");
            }
        } catch (error) {

        }
        finally {
            setIsLoading(false);
        }
    };
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Add Doctor Form"
            className="bg-white p-4 rounded-lg shadow-md overflow-auto max-h-screen"
        >
            {isLoading ? (
                <Loading />
            ) : (
                <main class="min-h-[calc(100vh-67px)] w-full md:w-[calc(100%-256px)] md:ml-64 md:mt-8 bg-white transition-all main">
                    <div className="p-6">
                        <div class="grid grid-cols-1 grid-rows-1">
                            <h2 className="text-2xl font-bold mb-2">Thêm Bác Sĩ</h2>
                            <form onSubmit={handleSubmit} className="space-y-4">
                                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <input type="text" id="name" name="name" placeholder=" " onChange={handleChange} required style={{ width: "1189px" }} className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                                    <label htmlFor="name" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Họ và tên</label>
                                </div>
                                {/* <input type="text" name="name" placeholder="Họ và tên" onChange={handleChange} required className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" /> */}
                                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <input type="text" id="phoneNumber" name="phoneNumber" placeholder=" " onChange={handleChange} required style={{ width: "1189px" }} className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                                    <label htmlFor="phoneNumber" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Số điện thoại</label>
                                </div>
                                {/* <input type="text" name="phoneNumber" placeholder="Số điện thoại" onChange={handleChange} required className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" /> */}
                                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <input type="email" id="email" name="email" placeholder=" " onChange={handleChange} required style={{ width: "1189px" }} className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                                    <label htmlFor="email" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Email</label>
                                </div>
                                {/* <input type="email" name="email" placeholder="Email" onChange={handleChange} required className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" /> */}
                                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <input type="text" id="experience" name="experience" placeholder=" " onChange={handleChange} required style={{ width: "1189px" }} className="block w-full px-3 py-2 mt-4 text-zinc-900 dark:text-white bg-white dark:bg-zinc-800 border border-zinc-300 dark:border-zinc-700 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent peer dark:peer"></input>
                                    <label htmlFor="experience" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Kinh nghiệm</label>
                                </div>
                                {/* <input type="text" name="experience" placeholder="Kinh nghiệm" onChange={handleChange} required className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" /> */}
                                {/* <input type="text" name="degree" placeholder="Bằng cấp" onChange={handleChange} required className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500" /> */}
                                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <select id="spec" name='specialist' onChange={handleChange} className='w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500'>
                                        <option value="-1">Chọn chuyên khoa</option>
                                        {
                                            specialists?.map((specItem) => (
                                                <option key={specItem?._id} value={specItem?._id}>{specItem?.specialistName}</option>
                                            ))
                                        }
                                    </select>
                                    <label htmlFor="spec" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Chuyên khoa</label>
                                </div>
                                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <select id="GT" name='sex' onChange={handleChange} className='w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500'>
                                        <option value="-1">Chọn giới tính</option>
                                        <option value="Nam">Nam</option>
                                        <option value="Nữ">Nữ</option>
                                    </select>
                                    <label htmlFor="GT" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Giới tính</label>
                                </div>
                                <div className="relative w-full max-w-xs dark:bg-zinc-900">
                                    <select
                                        name="dateOfWeek"
                                        className="w-full px-4 py-2 border border-gray-300 rounded-md focus:outline-none focus:border-blue-500"
                                        id=""
                                        onInput={handleDateOfWeekSelect}
                                        onChange={handleChange}
                                    >
                                        <option value="-1">chọn thứ khám</option>
                                        <option value="1">thứ hai</option>
                                        <option value="2">thứ ba</option>
                                        <option value="3">thứ tư</option>
                                        <option value="4">thứ năm</option>
                                        <option value="5">thứ sáu</option>
                                        <option value="6">thứ bảy</option>
                                        <option value="0">chủ nhật</option>
                                    </select>
                                    <label htmlFor="" className="absolute left-3 top-0 px-1 text-sm text-zinc-500 dark:text-zinc-400 bg-white dark:bg-zinc-800 transform -translate-y-1/2 peer-placeholder-shown:top-4 peer-placeholder-shown:text-base peer-placeholder-shown:text-zinc-400 peer-focus:top-0 peer-focus:text-sm peer-focus:text-blue-500 dark:peer-placeholder-shown:top-4 dark:peer-placeholder-shown:text-base dark:peer-placeholder-shown:text-zinc-400 dark:peer-focus:top-0 dark:peer-focus:text-sm dark:peer-focus:text-blue-500">Chọn thứ</label>
                                </div>
                                <div className="mt-2">
                                    <label className="block text-gray-500 font-medium mb-2">Chọn giờ khám:</label>
                                    <div className="grid grid-cols-2 sm:grid-cols-4 gap-4">
                                        {[
                                            "07:00-08:00",
                                            "08:00-09:00",
                                            "09:00-10:00",
                                            "10:00-11:00",
                                            "13:00-14:00",
                                            "14:00-15:00",
                                            "15:00-16:00",
                                            "16:00-17:00"
                                        ].map((timeSlot, index) => (
                                            <label key={index} className="flex items-center space-x-3">
                                                <input
                                                    id={index}
                                                    value={timeSlot}
                                                    type="checkbox"
                                                    onChange={handleSelectTime}
                                                    className="form-checkbox h-5 w-5 text-blue-600"
                                                />
                                                <span className="text-gray-700">{timeSlot}</span>
                                            </label>
                                        ))}
                                    </div>
                                </div>
                                <div>
                                    <label
                                        htmlFor="image-upload"
                                        className="block text-gray-500 font-medium mb-2"
                                    >
                                        Chọn hình ảnh:
                                    </label>
                                    <div className="flex items-center">
                                        <input
                                            type="file"
                                            id="image-upload"
                                            name="image"
                                            accept="image/*"
                                            onChange={handleFileChangeBase64}
                                            className="hidden"
                                        />
                                        <label htmlFor="image-upload" className="custom-file-upload cursor-pointer flex items-center justify-center bg-pink-400 text-white py-2 px-4 rounded-md hover:bg-pink-500 focus:outline-none">
                                            Chọn tệp
                                        </label>
                                        <span id="file-name" className="ml-3 text-gray-500">
                                            {fileName}
                                        </span>
                                    </div>
                                    <img src={`${doctorData.avatarImage !== null ? `data:image/png;base64,${doctorData?.avatarImage}` : '/images/NoImage.jpg'}`} alt="No Image"
                                        className="h-28 w-28 mt-4 rounded-md" />
                                </div>
                                <div className="flex justify-between">
                                    <button type="submit" className="bg-blue-500 text-white py-2 px-6 rounded-md hover:bg-blue-600 focus:outline-none">Thêm Bác Sĩ</button>
                                    <button
                                        onClick={() => {
                                            onRequestClose();
                                            setFileName("Không có tệp nào được chọn");
                                            setBase64String("");
                                            doctorData.avatarImage = null;
                                        }}
                                        className="bg-gray-300 text-gray-700 py-2 px-6 rounded-md hover:bg-gray-400 focus:outline-none">Đóng</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </main>
            )}
        </Modal>
    );
};

export default AddDoctorForm;
