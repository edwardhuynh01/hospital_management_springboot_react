package com.dev.server.Controller;

import com.dev.server.DTO.AppointmentDTO;
import com.dev.server.DTO.CheckOutDataDTO;
import com.dev.server.DTO.PaymentDataDTO;
import com.dev.server.DTO.ResposeDTO;
import com.dev.server.Service.AppointmentService;
import com.dev.server.model.*;
import com.dev.server.model.Record;
import com.dev.server.repository.AppointmentRepository;
import com.dev.server.repository.DoctorRepository;
import com.dev.server.repository.RecordRepository;
import com.dev.server.repository.ReportRepository;
import io.jsonwebtoken.Claims;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/appointment")
@CrossOrigin(origins = "http://localhost:3000")
public class PaymentController {
    @Autowired
    private AppointmentRepository appointmentRepository;
    @Autowired
    private ReportRepository reportRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private RecordRepository recordRepository;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private AppointmentService appointmentService;
    @Value("${vnpay.tmn_code}")
    private String tmnCode;

    @Value("${vnpay.hash_secret}")
    private String hashSecret;

    @Value("${vnpay.vnp_url}")
    private String vnpUrl;

    @Value("${vnpay.return_url}")
    private String returnUrl;

    @PostMapping("/MakeAppointmentNonPayment")
    public ResponseEntity<?> makeAppointmentNonPayment(@RequestBody AppointmentDTO appointmentDTO) throws Exception {
        try {
            List<Appointment> listAppointments = appointmentDTO.getListAppointments();
            ObjectId account = appointmentDTO.getUserId();
            ObjectId record = appointmentDTO.getRecordId();
            boolean isPayment = appointmentDTO.isPayment();
            double amount = appointmentDTO.getAmount();
            for (Appointment item : listAppointments) {
                Optional<Appointment> existAppointment = appointmentRepository.findByIdAndRecordIdAndDayOfWeekAndDayAndMonthAndYearAndTimeStartAndTimeEndAndSpecialistIdAndDoctorId(
                        account,
                        record,
                        item.getDayOfWeek(),
                        item.getDay(),
                        item.getMonth(),
                        item.getYear(),
                        item.getTimeStart(),
                        item.getTimeEnd(),
                        item.getSpecialistId(),
                        item.getDoctorId()
                );
                if (existAppointment.isPresent()) {
                    return ResponseEntity.status(400).body("Lịch khám này đã được đặt");
                }
            }
            List<Appointment> newListAppointments = listAppointments.stream().map(item -> {
                Appointment newAppointment = new Appointment();
                newAppointment.set_id(new ObjectId().toString());
                newAppointment.setId(account);
                newAppointment.setRecordId(record);
                newAppointment.setDayOfWeek(item.getDayOfWeek());
                newAppointment.setDay(item.getDay());
                newAppointment.setMonth(item.getMonth());
                newAppointment.setYear(item.getYear());
                newAppointment.setTimeStart(item.getTimeStart());
                newAppointment.setTimeEnd(item.getTimeEnd());
                newAppointment.setSpecialistId(item.getSpecialistId());
                newAppointment.setServices("Khám dịch vụ");
                newAppointment.setDoctorId(item.getDoctorId());
                return newAppointment;
            }).toList();
            appointmentRepository.saveAll(newListAppointments);
            Report newReport = new Report();
            newReport.set_id(new ObjectId().toString());
            newReport.setAccount(account);
            newReport.setRecord(record);
            newReport.setAppointment(newListAppointments.stream().map(item -> new ObjectId(item.get_id())).collect(Collectors.toList()));
            newReport.setAmount(amount);
            newReport.setPayment(isPayment);
            newReport.setCreatedAt(new Date());
            reportRepository.save(newReport);
            for (Appointment item : listAppointments) {
                Doctor doctor = doctorRepository.findById(item.getDoctorId().toString()).orElseThrow(() -> new Exception("Doctor not found"));
                for (Day day : doctor.getDays()) {
                    if (day.getDay() == Integer.parseInt(item.getDay())) {
                        for (Time time : day.getTimes()) {
                            if (time.getTimeStart().equals(item.getTimeStart())) {
                                time.setChoose(true);
                            }
                        }
                    }
                }
                doctorRepository.save(doctor);
            }
            Record record1 = recordRepository.findById(record.toString()).orElseThrow(() -> new Exception("Record not found"));
            String RPid = Base64.getEncoder().encodeToString(newReport.get_id().getBytes());
            String reportLink = String.format("%s://localhost:3000/DetailNotification/%s", request.getScheme(), RPid);
            appointmentService.sendReportMail(record1, reportLink);
            return ResponseEntity.status(200).body("Tạo lịch khám thành công");
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    private String getRandomNumber(int length) {
        try {
            Random rnd = new Random();
            StringBuilder sb = new StringBuilder(length);
            for (int i = 0; i < length; i++) {
                sb.append((char) ('0' + rnd.nextInt(10)));
            }
            return sb.toString();
        }catch (IllegalArgumentException ex){
            ex.printStackTrace();
            return "";
        }
    }

    public static String hmacSHA512(final String key, final String data) {
        try {

            if (key == null || data == null) {
                throw new NullPointerException();
            }
            final Mac hmac512 = Mac.getInstance("HmacSHA512");
            byte[] hmacKeyBytes = key.getBytes();
            final SecretKeySpec secretKey = new SecretKeySpec(hmacKeyBytes, "HmacSHA512");
            hmac512.init(secretKey);
            byte[] dataBytes = data.getBytes(StandardCharsets.UTF_8);
            byte[] result = hmac512.doFinal(dataBytes);
            StringBuilder sb = new StringBuilder(2 * result.length);
            for (byte b : result) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();

        } catch (Exception ex) {
            return "";
        }
    }

    public String hashAllFields(Map<String, String> fields) {
        try {
            // create a list and sort it
            List<String> fieldNames = new ArrayList<String>(fields.keySet());
            Collections.sort(fieldNames);
            // create a buffer for the md5 input and add the secure secret first
            StringBuilder sb = new StringBuilder();
            //sb.append(com.vnpay.common.Config.vnp_HashSecret);
            Iterator<String> itr = fieldNames.iterator();
            while (itr.hasNext()) {
                String fieldName = itr.next();
                String fieldValue = fields.get(fieldName);
                if ((fieldValue != null) && (!fieldValue.isEmpty())) {
                    sb.append(fieldName);
                    sb.append("=");
                    sb.append(fieldValue);
                }
                if (itr.hasNext()) {
                    sb.append("&");
                }
            }
            //return Sha256(sb.toString());
            return hmacSHA512(hashSecret, sb.toString());
        }catch (Exception ex){
            ex.printStackTrace();
            return "";
        }
    }

    @PostMapping("/create-payment-link")
    public ResponseEntity<?> doPost(@RequestBody AppointmentDTO appointmentDTO, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            List<Appointment> listAppointments = appointmentDTO.getListAppointments();
            ObjectId account = appointmentDTO.getUserId();
            ObjectId record = appointmentDTO.getRecordId();
            boolean isPayment = appointmentDTO.isPayment();
            double amount = appointmentDTO.getAmount();
            for (Appointment item : listAppointments) {
                Optional<Appointment> existAppointment = appointmentRepository.findByIdAndRecordIdAndDayOfWeekAndDayAndMonthAndYearAndTimeStartAndTimeEndAndSpecialistIdAndDoctorId(
                        account,
                        record,
                        item.getDayOfWeek(),
                        item.getDay(),
                        item.getMonth(),
                        item.getYear(),
                        item.getTimeStart(),
                        item.getTimeEnd(),
                        item.getSpecialistId(),
                        item.getDoctorId()
                );
                if (existAppointment.isPresent()) {
                    return ResponseEntity.status(400).body("Lịch khám này đã được đặt");
                }
            }
            List<Appointment> newListAppointments1 = listAppointments.stream().map(item -> {
                Appointment newAppointment1 = new Appointment();
                newAppointment1.set_id(new ObjectId().toString());
                newAppointment1.setId(account);
                newAppointment1.setRecordId(record);
                newAppointment1.setDayOfWeek(item.getDayOfWeek());
                newAppointment1.setDay(item.getDay());
                newAppointment1.setMonth(item.getMonth());
                newAppointment1.setYear(item.getYear());
                newAppointment1.setTimeStart(item.getTimeStart());
                newAppointment1.setTimeEnd(item.getTimeEnd());
                newAppointment1.setSpecialistId(item.getSpecialistId());
                newAppointment1.setServices("Khám dịch vụ");
                newAppointment1.setDoctorId(item.getDoctorId());
                return newAppointment1;
            }).toList();
            Report newReport1 = new Report();
            newReport1.set_id(new ObjectId().toString());
            newReport1.setAccount(account);
            newReport1.setRecord(record);
            newReport1.setAppointment(newListAppointments1.stream().map(item -> new ObjectId(item.get_id())).collect(Collectors.toList()));
            newReport1.setAmount(amount);
            newReport1.setPayment(isPayment);
            newReport1.setCreatedAt(new Date());
            String vnp_Version = "2.1.0";
            String vnp_Command = "pay";
            String vnp_TxnRef = getRandomNumber(8);
            String vnp_IpAddr = request.getRemoteAddr();
            String vnp_TmnCode = tmnCode;
            String vnp_CreateDate = new java.text.SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            String vnp_CurrCode = "VND";
            String vnp_Locale = "vn";
            String vnp_OrderInfo = "Thanh toan vien phi: " + vnp_TxnRef;
            String vnp_OrderType = "other";
            String vnp_ReturnUrl = returnUrl;
            int amountInt = (int) amount;
            String vnp_Amount = String.valueOf(amountInt * 100);

            Map<String, String> vnp_Params = new TreeMap<>();
            vnp_Params.put("vnp_Version", vnp_Version);
            vnp_Params.put("vnp_Command", vnp_Command);
            vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
            vnp_Params.put("vnp_Amount", vnp_Amount);
            vnp_Params.put("vnp_CurrCode", vnp_CurrCode);
            String bank_code = req.getParameter("bankcode");
            if (bank_code != null && !bank_code.isEmpty()) {
                vnp_Params.put("vnp_BankCode", bank_code);
            }
            vnp_Params.put("vnp_TxnRef", vnp_TxnRef);
            vnp_Params.put("vnp_OrderInfo", vnp_OrderInfo);
            vnp_Params.put("vnp_OrderType", vnp_OrderType);

            String locate = req.getParameter("language");
            if (locate != null && !locate.isEmpty()) {
                vnp_Params.put("vnp_Locale", locate);
            } else {
                vnp_Params.put("vnp_Locale", vnp_Locale);
            }
            vnp_Params.put("vnp_ReturnUrl", vnp_ReturnUrl);
            vnp_Params.put("vnp_IpAddr", "13.160.92.202");
            vnp_Params.put("vnp_CreateDate", vnp_CreateDate);
            Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+7"));
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
            cld.add(Calendar.MINUTE, 15);
            String vnp_ExpireDate = formatter.format(cld.getTime());
            vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);


            List<String> fieldNames = new ArrayList<>(vnp_Params.keySet());
            Collections.sort(fieldNames);
            StringBuilder hashData = new StringBuilder();
            StringBuilder query = new StringBuilder();
            Iterator<String> itr = fieldNames.iterator();
            while (itr.hasNext()) {
                String fieldName = itr.next();
                String fieldValue = vnp_Params.get(fieldName);
                if ((fieldValue != null) && (!fieldValue.isEmpty())) {
                    //Build hash data
                    hashData.append(fieldName);
                    hashData.append('=');
                    hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII));
                    //Build query
                    query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII));
                    query.append('=');
                    query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII));
                    if (itr.hasNext()) {
                        query.append('&');
                        hashData.append('&');
                    }
                }
            }
            String queryUrl = query.toString();
            String vnp_SecureHash = hmacSHA512(hashSecret, hashData.toString());
            queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
            String paymentUrl = vnpUrl + "?" + queryUrl;
            PaymentDataDTO paymentDataDTO = new PaymentDataDTO();
            paymentDataDTO.setListAppointment(newListAppointments1);
            paymentDataDTO.setReport(newReport1);
            paymentDataDTO.setPaymentLinkId(vnp_TxnRef);
            final String paymentToken = appointmentService.generateDataPaymentToken(paymentDataDTO);
            CheckOutDataDTO checkOutDataDTO = new CheckOutDataDTO();
            checkOutDataDTO.setPaymentToken(paymentToken);
            checkOutDataDTO.setCheckoutUrl(paymentUrl);
            System.out.println(paymentUrl);
            return ResponseEntity.ok().body(checkOutDataDTO);
        }catch (Exception ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Đã xảy ra lỗi trong quá trình xử lý yêu cầu");
        }
    }

    @GetMapping("/success")
    public ResponseEntity<?> successPayment(HttpServletRequest req) {
        try {
            Map<String, String> fields = new HashMap<>();
            String token = request.getHeader("tokenpayment");
            if (token == null || token.isEmpty()) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token không hợp lệ");
            } else {
                String paymentToken = token.split(" ")[1];
                Map<String, Object> claims = appointmentService.verifyDataPaymentToken(paymentToken);
                if (claims == null) {
                    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Token hết hạn");
                } else {
                    request.setAttribute("info", claims);
                    if ("00".equals(request.getParameter("vnp_ResponseCode"))) {
                        String vnp_TxnRef = request.getParameter("vnp_TxnRef");
                        if (vnp_TxnRef.isEmpty()) {
                            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Phiếu khám không tồn tại");
                        }
                        Report existReport = reportRepository.findReportByPaymentId(vnp_TxnRef);
                        if (existReport != null) {
                            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Mã thanh toán này đã được sử dụng");
                        }
                        String paymentLinkId =(String) claims.get("paymentLinkId");
                        if (!vnp_TxnRef.equals(paymentLinkId)) {
                            ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Vui lòng chờ chuyển hướng đến trang hoàn tất sau khi thanh toán thành công");
                        }
                        List<Map<String, Object>> appointmentDataList = (List<Map<String, Object>>) claims.get("listAppointment");
                        Map<String, Object> reportData = (Map<String, Object>) claims.get("report");
                        Report report = convertMapToReport(reportData);
                        List<Appointment> appointmentList = new ArrayList<>();
                        for (Map<String, Object> appointmentData : appointmentDataList) {
                            Appointment appointment = convertMapToAppointment(appointmentData);
                            appointmentList.add(appointment);
                        }

                        for (Appointment item : appointmentList) {
                            Optional<Appointment> existAppointment = appointmentRepository.findByIdAndRecordIdAndDayOfWeekAndDayAndMonthAndYearAndTimeStartAndTimeEndAndSpecialistIdAndDoctorId(
                                    item.getId(),
                                    item.getRecordId(),
                                    item.getDayOfWeek(),
                                    item.getDay(),
                                    item.getMonth(),
                                    item.getYear(),
                                    item.getTimeStart(),
                                    item.getTimeEnd(),
                                    item.getSpecialistId(),
                                    item.getDoctorId()
                            );
                            if (existAppointment.isPresent()) {
                                return ResponseEntity.status(400).body("lịch khám này đã được đặt vui lòng liên hệ số điện thoại: 0786571369(Nhật) để được hỗ trợ");
                            }
                        }
                        List<Appointment> newListAppointments = appointmentList.stream().map(item -> {
                            Appointment newAppointment = new Appointment();
                            newAppointment.set_id(new ObjectId().toString());
                            newAppointment.setId(item.getId());
                            newAppointment.setRecordId(item.getRecordId());
                            newAppointment.setDayOfWeek(item.getDayOfWeek());
                            newAppointment.setDay(item.getDay());
                            newAppointment.setMonth(item.getMonth());
                            newAppointment.setYear(item.getYear());
                            newAppointment.setTimeStart(item.getTimeStart());
                            newAppointment.setTimeEnd(item.getTimeEnd());
                            newAppointment.setSpecialistId(item.getSpecialistId());
                            newAppointment.setServices("Khám dịch vụ");
                            newAppointment.setDoctorId(item.getDoctorId());
                            return newAppointment;
                        }).toList();
                        appointmentRepository.saveAll(newListAppointments);
                        Report newReport = new Report();
                        newReport.set_id(report.get_id());
                        newReport.setAccount(report.getAccount());
                        newReport.setRecord(report.getRecord());
                        newReport.setAppointment(report.getAppointment());
                        newReport.setAmount(report.getAmount());
                        newReport.setPayment(report.isPayment());
                        newReport.setCreatedAt(report.getCreatedAt());
                        reportRepository.save(newReport);
                        for (Appointment item : appointmentList) {
                            Doctor doctor = doctorRepository.findById(item.getDoctorId().toString()).orElseThrow(() -> new Exception("Doctor not found"));
                            for (Day day : doctor.getDays()) {
                                if (day.getDay() == Integer.parseInt(item.getDay())) {
                                    for (Time time : day.getTimes()) {
                                        if (time.getTimeStart().equals(item.getTimeStart())) {
                                            time.setChoose(true);
                                        }
                                    }
                                }
                            }
                            doctorRepository.save(doctor);
                        }
                        Record record1 = recordRepository.findById(appointmentList.getFirst().getRecordId().toString()).orElseThrow(() -> new Exception("Record not found"));
                        String RPid = Base64.getEncoder().encodeToString(newReport.get_id().getBytes());
                        String reportLink = String.format("%s://localhost:3000/DetailNotification/%s", request.getScheme(), RPid);
                        appointmentService.sendReportMail(record1, reportLink);
                        for (Enumeration<String> params = request.getParameterNames(); params.hasMoreElements(); ) {
                            String fieldName = params.nextElement();
                            String fieldValue = request.getParameter(fieldName);
                            if ((fieldValue != null) && (!fieldValue.isEmpty())) {
                                fields.put(fieldName, fieldValue);
                            }
                        }
                        String vnp_SecureHash = request.getParameter("vnp_SecureHash");
                        if (fields.containsKey("vnp_SecureHashType")) {
                            fields.remove("vnp_SecureHashType");
                        }
                        if (fields.containsKey("vnp_SecureHash")) {
                            fields.remove("vnp_SecureHash");
                        }
                        String signValue = hashAllFields(fields);
                        System.out.println(signValue);
                        System.out.println(vnp_SecureHash);
//        if (signValue.equals(vnp_SecureHash)) {
                        ResposeDTO dataSuccess = new ResposeDTO();
                        dataSuccess.setData("Đặt khám thành công");
                        return ResponseEntity.ok("Đặt khám thành công");
                    } else {
                        ResposeDTO dataNonSuccess = new ResposeDTO();
                        dataNonSuccess.setData("Đặt khám không thành công");
                        return ResponseEntity.ok("Đặt khám không thành công");
                    }

//        } else {
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Chu ky khong hop le");
//        }
                }
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("đã có lỗi xảy ra");
        }
    }
    private static Appointment convertMapToAppointment(Map<String, Object> appointmentData) {
        try {
            // Phương thức giả định để chuyển đổi Map thành Appointment
            // Bạn cần thay thế bằng logic thực tế để chuyển đổi Map thành Appointment
            Appointment newAppointment = new Appointment();
            newAppointment.set_id((String) appointmentData.get("_id"));
            newAppointment.setId(new ObjectId((String)appointmentData.get("id")));
            newAppointment.setRecordId(new ObjectId((String)appointmentData.get("recordId")));
            newAppointment.setDayOfWeek((String) appointmentData.get("dayOfWeek"));
            newAppointment.setDay((String) appointmentData.get("day"));
            newAppointment.setMonth((String) appointmentData.get("month"));
            newAppointment.setYear((String) appointmentData.get("year"));
            newAppointment.setTimeStart((String) appointmentData.get("timeStart"));
            newAppointment.setTimeEnd((String) appointmentData.get("timeEnd"));
            newAppointment.setSpecialistId(new ObjectId((String)appointmentData.get("specialistId")));
            newAppointment.setServices("Khám dịch vụ");
            newAppointment.setDoctorId(new ObjectId((String)appointmentData.get("doctorId")));
            // Thêm các thuộc tính khác tùy theo yêu cầu
            return newAppointment;
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
    private static Report convertMapToReport(Map<String, Object> reportData) {
        try {
            // Phương thức giả định để chuyển đổi Map thành Report
            // Bạn cần thay thế bằng logic thực tế để chuyển đổi Map thành Report
            Report report = new Report();
            report.set_id((String) reportData.get("_id"));
            report.setAccount(new ObjectId((String) reportData.get("account")));
            report.setRecord(new ObjectId((String) reportData.get("record")));
            report.setAppointment((List<ObjectId>) reportData.get("appointment"));
            report.setCreatedAt(new Date((Long) reportData.get("createdAt")));
            report.setAmount((double) reportData.get("amount"));
            report.setPayment((boolean) reportData.get("payment"));
            report.setPaymentId((String) reportData.get("paymentId"));
            // Thêm các thuộc tính khác tùy theo yêu cầu
            return report;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
